package de.hotel.android.library.remoteaccess.adapter;

import android.test.InstrumentationTestCase;

import org.mockito.Mockito;

import de.hotel.android.library.domain.model.response.RegistrationResponse;
import de.hotel.android.library.remoteaccess.adapter.mocks.Paths;
import de.hotel.android.library.remoteaccess.adapter.mocks.RequestUnawareWebFormService;
import de.hotel.android.library.remoteaccess.querymapping.CustomerRegistrationRequestMapperImpl;
import de.hotel.android.library.remoteaccess.resultmapping.CustomerRegistrationResponseMapperImpl;
import de.hotel.android.library.remoteaccess.resultmapping.HdeJsonMapperImpl;

import static org.assertj.core.api.Assertions.assertThat;


public class HdeCustomerRegistrationAdapterTest extends InstrumentationTestCase {

    // This is needed because dexmaker has some problems without this, so mockito crashes
    // See http://stackoverflow.com/questions/12267572/mockito-dexmaker-on-android
    @Override
    protected void setUp() throws Exception {
        super.setUp();

        System.setProperty(
                "dexmaker.dexcache",
                getInstrumentation().getTargetContext().getCacheDir().getPath());
    }

    public void testThatRegistrationWithAlreadyExistingEmailIsParsedSuccessfully() {
        // Arrange
        RequestUnawareWebFormService webFormService = new RequestUnawareWebFormService(Paths.REGISTER_CUSTOMER_ALREADY_EXISTS, new HdeJsonMapperImpl());

        // Act
        RegistrationResponse registrationResponse = webFormService.performRequest(null, RegistrationResponse.class);

        // Assert
        assertThat(registrationResponse).isNotNull();
        assertThat(registrationResponse.getResult()).isNotNull();
        assertThat(registrationResponse.getResult().getErrorCodes()).isNotNull();
        assertThat(registrationResponse.getResult().getErrorCodes().get(0)).isEqualTo(RegistrationResponse.ERROR_CODE_CUSTOMER_ALREADY_EXISTS);
    }

    public void testThatLeisureRegistrationIsParsedSuccessfully() {
        // Arrange
        RequestUnawareWebFormService webFormService = new RequestUnawareWebFormService(Paths.REGISTER_CUSTOMER_PUBLIC, new HdeJsonMapperImpl());

        // Act
        RegistrationResponse registrationResponse = webFormService.performRequest(null, RegistrationResponse.class);

        // Assert
        assertThat(registrationResponse).isNotNull();
        assertThat(registrationResponse.getResult()).isNotNull();
        assertThat(registrationResponse.getResult().getErrorCodes()).isNull();
        assertThat(registrationResponse.getResult().getCustomerNumber()).isEqualTo(8133371);
        assertThat(registrationResponse.getResult().getCorporate()).isFalse();
        assertThat(registrationResponse.getResult().getNewsletter()).isFalse();
    }

    public void testThatCompanyRegistrationIsParsedSuccessfully() {
        // Arrange
        RequestUnawareWebFormService webFormService = new RequestUnawareWebFormService(Paths.REGISTER_CUSTOMER_COMPANY, new HdeJsonMapperImpl());

        // Act
        RegistrationResponse registrationResponse = webFormService.performRequest(null, RegistrationResponse.class);

        // Assert
        assertThat(registrationResponse).isNotNull();
        assertThat(registrationResponse.getResult()).isNotNull();
        assertThat(registrationResponse.getResult().getErrorCodes()).isNull();
        assertThat(registrationResponse.getResult().getCustomerNumber()).isEqualTo(8133372);
        assertThat(registrationResponse.getResult().getCorporate()).isTrue();
        assertThat(registrationResponse.getResult().getNewsletter()).isFalse();
    }
}
