package de.hotel.android.library.remoteaccess.adapter.mocks;

public final class Paths {
    private Paths() {
        // Intentionally left blank
    }

    public static final String PROPERTY_DESCRIPTION_RESPONSE = "res/raw/property_description_response.xml";
    public static final String PROPERTY_REVIEWS_RESPONSE = "res/raw/property_reviews_response.xml";
    public static final String PROPERTY_REVIEWS_RESPONSE_NO_REVIEWS = "res/raw/property_reviews_response_no_reviews.xml";
    public static final String PROPERTY_REVIEWS_RESPONSE_WITH_COMMENTS = "res/raw/property_reviews_response_with_comments.xml";
    public static final String PROPERTY_DESCRIPTION_RESPONSE_NO_LEISURE_AMENITIES = "res/raw/property_description_response_no_leisure_amenities.xml";
    public static final String PROPERTY_DESCRIPTION_PROPERTY_NOT_FOUND = "res/raw/property_description_response_property_not_found.xml";
    public static final String HOTEL_AVAIL_RESPONSE_NO_AVAILABILITY = "res/raw/hotel_avail_response_no_availability.xml";
    public static final String HOTEL_AVAIL_RESPONSE_WITHOUT_PAGING = "res/raw/hotel_avail_response_without_paging.xml";
    public static final String HOTEL_AVAIL_RESPONSE_PAGED_0 = "res/raw/hotel_avail_response_paging_0.xml";
    public static final String HOTEL_AVAIL_RESPONSE_PAGED_2 = "res/raw/hotel_avail_response_paging_2.xml";
    public static final String HOTEL_AVAIL_RESPONSE_CORPORATE = "res/raw/hotel_avail_response_corporate.xml";
    public static final String RATE_DETAIL_RESPONSE = "res/raw/rate_detail_response.xml";
    public static final String CHECK_RESERVATION_STATUS_RESPONSE = "res/raw/check_reservation_status_response.xml";
    public static final String CHECK_RESERVATION_STATUS_RESPONSE_MULTIPLE_RATES = "res/raw/check_reservation_status_response_multiple_rates.xml";
    public static final String CHECK_RESERVATION_STATUS_RESPONSE_NO_RESERVATIONS = "res/raw/check_reservation_status_response_no_reservations.xml";
    public static final String CANCEL_RESERVATION_RESPONSE = "res/raw/cancel_reservation_response.xml";
    public static final String CANCEL_RESERVATION_RESPONSE_ALREADY_CANCELLED = "res/raw/cancel_reservation_response_already_cancelled.xml";
    public static final String CANCEL_RESERVATION_RESPONSE_NOT_CANCELABLE_RESERVATION = "res/raw/cancel_reservation_response_not_cancelable_reservation.xml";

    // Customer Registration
    public static final String REGISTER_CUSTOMER_ALREADY_EXISTS = "res/raw/register_customer_already_exists.json";
    public static final String REGISTER_CUSTOMER_COMPANY = "res/raw/register_customer_company.json";
    public static final String REGISTER_CUSTOMER_PUBLIC = "res/raw/register_customer_public.json";
}
