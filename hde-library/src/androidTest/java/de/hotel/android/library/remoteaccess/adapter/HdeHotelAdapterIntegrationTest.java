package de.hotel.android.library.remoteaccess.adapter;

import android.test.suitebuilder.annotation.Suppress;

import de.hotel.android.library.HotelDe;
import de.hotel.android.library.domain.adapter.HotelAdapter;
import de.hotel.android.library.domain.model.HdeRatePlan;
import de.hotel.android.library.domain.model.HotelAvailResult;
import de.hotel.android.library.domain.model.Hotel;
import de.hotel.android.library.domain.model.criterion.FilterCriterion;
import de.hotel.android.library.domain.model.criterion.HotelAvailCriterion;
import de.hotel.android.library.domain.model.criterion.HotelSearchCriterion;
import de.hotel.android.library.domain.model.criterion.LocationCriterion;
import de.hotel.android.library.domain.model.criterion.RoomCriterion;
import de.hotel.android.library.domain.model.data.Language;
import de.hotel.android.library.domain.model.data.Locale;
import de.hotel.android.library.domain.model.enums.AmenityType;
import de.hotel.android.library.domain.model.enums.BreakfastType;
import de.hotel.android.library.domain.model.enums.RoomType;
import de.hotel.android.library.domain.model.enums.SortOrder;
import de.hotel.android.library.domain.model.query.HotelAvailQuery;
import de.hotel.android.library.remoteaccess.RemoteAccessTargetEnvironmentType;

import junit.framework.TestCase;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Runs against the real HSBW. Use for developing/debugging purposes.
 */
@Suppress
public class HdeHotelAdapterIntegrationTest extends TestCase {
    public static final int LOCATION_ID_PARIS = 65886;

    private HotelDe hde;

    public void setUp() {
        hde = new HotelDe();
    }

    public void testHotelSearchAgainstRealHSBW() throws ParseException {
        // Arrange
        HotelAdapter hdeHotelAdapterImpl = hde.createHdeHotelAdapterImpl(RemoteAccessTargetEnvironmentType.BETA);
        HotelAvailQuery availQuery = HdeHotelAdapterTest.createAvailQuery();

        // Act
        HotelAvailResult hotelAvailResult = hdeHotelAdapterImpl.searchAvailableHotels(availQuery);

        // Assert
        assertThat(hotelAvailResult.getHotelList()).isNotEmpty();
    }

    public void testHotelSearchWithFiltersAgainstRealHSBW() throws ParseException {
        // Arrange
        HotelAdapter hdeHotelAdapterImpl = hde.createHdeHotelAdapterImpl(RemoteAccessTargetEnvironmentType.BETA);
        HotelAvailQuery availQuery = createFilteredHotelQuery();

        // Act
        HotelAvailResult hotelAvailResult = hdeHotelAdapterImpl.searchAvailableHotels(availQuery);

        // Assert
        assertThat(hotelAvailResult.getHotelList()).isNotEmpty();

        for (Hotel hotel : hotelAvailResult.getHotelList()) {

            boolean atLeastOneRateWithBreakfast = false;

            for (HdeRatePlan ratePlan : hotel.getHotelOffer().getRatePlans()) {
                if (ratePlan.getPaymentInfo().isBreakFastIncluded()) {
                    atLeastOneRateWithBreakfast = true;
                    break;
                }
            }
            assertThat(atLeastOneRateWithBreakfast);
        }
    }

    public void testHotelSearchWithSortingAgainstRealHSBW() throws ParseException {
        // Arrange
        HotelAdapter hdeHotelAdapterImpl = hde.createHdeHotelAdapterImpl(RemoteAccessTargetEnvironmentType.BETA);
        HotelAvailQuery availQuery = createFilteredHotelQuery();
        availQuery.setSortOrder(SortOrder.STAR_RATING_DESCENDING);

        float maximumStarRating = 5.0f;
        float lastStarRating = maximumStarRating;

        // Act
        HotelAvailResult hotelAvailResult = hdeHotelAdapterImpl.searchAvailableHotels(availQuery);

        // Assert
        assertThat(hotelAvailResult.getHotelList()).isNotEmpty();

        for (Hotel hotel : hotelAvailResult.getHotelList()) {
            assertThat(hotel.getStarsRating()).isLessThanOrEqualTo(lastStarRating);
            lastStarRating = hotel.getStarsRating();
        }
    }

    private HotelAvailQuery createFilteredHotelQuery() throws ParseException {
        HotelAvailQuery hotelAvailQuery = new HotelAvailQuery();
        HotelAvailCriterion hotelAvailCriterion = new HotelAvailCriterion();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", java.util.Locale.US);
        Date dateFrom = simpleDateFormat.parse("2016-10-13");
        long from = dateFrom.getTime();

        Date dateTo = simpleDateFormat.parse("2016-10-14");
        long to = dateTo.getTime();

        hotelAvailCriterion.setFrom(from);
        hotelAvailCriterion.setTo(to);
        ArrayList<RoomCriterion> roomCriterions = new ArrayList<>();
        RoomCriterion roomCriterion = new RoomCriterion();
        roomCriterion.setAdultCount(1);
        roomCriterion.setRoomType(RoomType.DOUBLE);
        roomCriterion.setQuantity(1);
        roomCriterions.add(roomCriterion);
        hotelAvailCriterion.setRoomCriterionList(roomCriterions);
        hotelAvailQuery.setHotelAvailCriterion(hotelAvailCriterion);

        HotelSearchCriterion hotelSearchCriterion = new HotelSearchCriterion();
        LocationCriterion locationCriterion = new LocationCriterion();
        locationCriterion.setLocationId(LOCATION_ID_PARIS);
        hotelSearchCriterion.setLocationCriterion(locationCriterion);

        FilterCriterion filterCriterion = new FilterCriterion();
        filterCriterion.setPricePerNightMaximum(new BigDecimal(175.00));
        filterCriterion.setUserRatingMinimum(3);
        filterCriterion.setStarRatingMinimum(2);
        filterCriterion.getAmenities().add(AmenityType.WIFI);
        filterCriterion.getAmenities().add(AmenityType.PARKING_LOT);
        filterCriterion.setBreakfastType(BreakfastType.INCLUSIVE);
        hotelSearchCriterion.setFilterCriterion(filterCriterion);
        hotelAvailQuery.setHotelSearchCriterion(hotelSearchCriterion);

        Locale locale = new Locale();
        locale.setIso3Currency("EUR");
        Language language = new Language();
        language.setIso2Language("EN");
        locale.setLanguage(language);
        hotelAvailQuery.setLocale(locale);

        return hotelAvailQuery;
    }
}
