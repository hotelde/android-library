package de.hotel.android.library.remoteaccess.adapter.mocks;

import junit.framework.Assert;

import org.jibx.runtime.JiBXException;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import de.hotel.android.library.remoteaccess.HdeJiBXMarshaller;
import de.hotel.android.library.remoteaccess.HdeJiBXMarshallerImpl;
import de.hotel.android.library.remoteaccess.soap.SoapService;
import de.hotel.android.library.util.FileReader;

public class MockedSoapService implements SoapService {
    private static final String PARAM_MAX_RESPONSES = "MaxResponses=";
    private static final String PARAM_SEQUENCE_NUMBER = "SequenceNmbr=";

    private HdeJiBXMarshaller hdeJiBXMarshaller = new HdeJiBXMarshallerImpl();
    private InputStream response;

    public MockedSoapService(String file) {
        try {
            response = FileReader.loadXMLFile(file);
        } catch (FileNotFoundException e) {
            Assert.fail("Could not load XML response file");
        }
    }

    @Override
    public <T, S> S performRequest(T request, Class<S> type, String soapAction, boolean retryOnFailure) {
        try {
            String marshalledRequest = hdeJiBXMarshaller.marshalRequest(request).toString();

            if (marshalledRequest.contains(PARAM_MAX_RESPONSES) || marshalledRequest.contains(PARAM_SEQUENCE_NUMBER)) {
                Assert.fail("Paging attribute found in request without paging");
            }

            return hdeJiBXMarshaller.unmarshalResponse(response, type);
        } catch (JiBXException | UnsupportedEncodingException e) {
            Assert.fail(e.toString());
        }

        throw new IllegalArgumentException();
    }
}
