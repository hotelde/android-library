package de.hotel.android.library.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import junit.framework.TestCase;

import de.hotel.android.library.domain.model.data.Address;
import de.hotel.android.library.domain.model.data.Customer;
import de.hotel.android.library.domain.model.data.GeoPosition;

import static org.assertj.core.api.Assertions.assertThat;

public class CustomerParcelTest extends TestCase {

    private static final String FIRSTNAME = "First name";
    public static final String LASTNAME = "Last name";
    public static final String EMAIL = "customer@hotel.de";
    public static final String PHONE = "+497659566";
    public static final int CUSTOMER_NUMBER = 7865464;
    public static final int COMPANY_NUMBER = 1337;
    public static final String COMPANY_NAME = "1337 Company";
    public static final String CREDIT_CARD_NUMBER = "4111111111111111";
    public static final long LAST_LOGIN_TIME = 654654987654654l;
    public static final String CITY = "Customer city";
    public static final String COUNRY_ISOA3 = "DEU";
    public static final String POSTAL_CODE = "90411";
    public static final String STREET = "1337 Street";
    public static final double LATITUDE = 40.123123;
    public static final double LONGITUDE = 11.123123;

    public void testParcelShouldReturnAllValuesCorrectly() {

        // Arrange
        Customer customer = new Customer();
        customer.setFirstname(FIRSTNAME);
        customer.setLastname(LASTNAME);
        customer.setEmail(EMAIL);
        customer.setPhone(PHONE);
        customer.setCustomerNumber(CUSTOMER_NUMBER);
        customer.setCompanyNumber(COMPANY_NUMBER);
        customer.setCompanyName(COMPANY_NAME);
        customer.setCreditCardNumber(CREDIT_CARD_NUMBER);
        customer.setLastLoginTime(LAST_LOGIN_TIME);

        Address address = new Address();
        address.setCity(CITY);
        address.setCountryIsoA3(COUNRY_ISOA3);
        address.setPostalCode(POSTAL_CODE);
        address.setStreet(STREET);

        customer.setAddress(address);

        GeoPosition geoPosition = new GeoPosition();
        geoPosition.setLatitude(LATITUDE);
        geoPosition.setLongitude(LONGITUDE);
        address.setGeoPosition(geoPosition);

        // Act
        Parcel parcel = Parcel.obtain();
        parcel.writeParcelable(customer, Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
        parcel.setDataPosition(0);

        Customer customerFromParcel = parcel.readParcelable(Customer.class.getClassLoader());

        // Assert
        assertThat(customerFromParcel.getFirstname()).isEqualTo(FIRSTNAME);
        assertThat(customerFromParcel.getLastname()).isEqualTo(LASTNAME);
        assertThat(customerFromParcel.getEmail()).isEqualTo(EMAIL);
        assertThat(customerFromParcel.getPhone()).isEqualTo(PHONE);
        assertThat(customerFromParcel.getCustomerNumber()).isEqualTo(CUSTOMER_NUMBER);
        assertThat(customerFromParcel.getCompanyNumber()).isEqualTo(COMPANY_NUMBER);
        assertThat(customerFromParcel.getCompanyName()).isEqualTo(COMPANY_NAME);
        assertThat(customerFromParcel.getCreditCardNumber()).isEqualTo(CREDIT_CARD_NUMBER);
        assertThat(customerFromParcel.getLastLoginTime()).isEqualTo(LAST_LOGIN_TIME);
    }
}
