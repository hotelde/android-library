package de.hotel.android.library.remoteaccess.adapter;

import android.test.suitebuilder.annotation.Suppress;

import junit.framework.TestCase;

import java.text.ParseException;

import de.hotel.android.library.HotelDe;
import de.hotel.android.library.domain.model.data.Customer;
import de.hotel.android.library.domain.model.data.Language;
import de.hotel.android.library.remoteaccess.RemoteAccessTargetEnvironmentType;
import de.hotel.android.library.remoteaccess.v28.querymapping.CustomerDataQuery;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Runs against the real HSBW. Use for developing/debugging purposes.
 */
@Suppress
public class HdeCustomerManagementAdapterIntegrationTest extends TestCase {

    private HotelDe hde;

    public void setUp() {
        hde = new HotelDe();
    }

    public void testGetCustomerData() throws ParseException {
        // Arrange
        HdeCustomerManagementAdapterImpl customerManagementAdapter = hde.createCustomerManagementAdapter(RemoteAccessTargetEnvironmentType.BETA);

        CustomerDataQuery customerDataQuery = new CustomerDataQuery();
        customerDataQuery.setEmailAddress("ad@hotel.de");
        customerDataQuery.setPassword("123456");
        Language language = new Language();
        language.setIso2Language("EN");
        customerDataQuery.setLanguage(language);

        // Act
        Customer customerData = customerManagementAdapter.getCustomerData(customerDataQuery);

        // Assert
        assertThat(customerData).isNotNull();
        assertThat(customerData.getFirstname()).isEqualTo("Booker first name");
        assertThat(customerData.getLastname()).isEqualTo("Booker last name");
        assertThat(customerData.getEmail()).isEqualTo("ad@hotel.de");
        assertThat(customerData.getPhone()).isEqualTo("+499111234567");
    }
}
