package de.hotel.android.library.remoteaccess.adapter.mocks;

import junit.framework.Assert;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URLConnection;

import de.hotel.android.library.remoteaccess.soap.HdeSoapClient;

public class TwoTimesFailingSoapClient implements HdeSoapClient {
    private int actualCalls = 0;
    private int expectedCalls;
    private MockedUrlConnection mockedUrlConnection;
    private InputStream fileStream;

    public TwoTimesFailingSoapClient(String pathToXmlFile, int expectedCalls) {
        this.expectedCalls = expectedCalls;

        try {
            fileStream = loadXMLFile(pathToXmlFile);
            mockedUrlConnection = new MockedUrlConnection();
        } catch (FileNotFoundException e) {
            Assert.fail("Could not load XML response file");
        }
    }

    @Override
    public URLConnection performRequest(String soapRequestContent, String soapAction) throws Exception {
        actualCalls++;
        if (actualCalls < expectedCalls) {
            throw new Exception();
        }

        return mockedUrlConnection;
    }

    public int getActualCalls() {
        return actualCalls;
    }

    private InputStream loadXMLFile(String resource) throws FileNotFoundException {
        return this.getClass().getClassLoader().getResourceAsStream(resource);
    }

    private class MockedUrlConnection extends HttpURLConnection {

        protected MockedUrlConnection() {
            super(null);
        }

        @Override
        public InputStream getInputStream() throws IOException {
            return fileStream;
        }

        @Override
        public void disconnect() {
            try {
                fileStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public boolean usingProxy() {
            return false;
        }

        @Override
        public void connect() throws IOException {

        }
    }
}
