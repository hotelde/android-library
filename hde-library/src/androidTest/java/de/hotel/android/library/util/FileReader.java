package de.hotel.android.library.util;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class FileReader {
    public static InputStream loadXMLFile(String resource) throws FileNotFoundException {
        return FileReader.class.getClassLoader().getResourceAsStream(resource);
    }
}
