package de.hotel.android.library.remoteaccess.adapter;

import android.test.InstrumentationTestCase;

import de.hotel.android.library.domain.model.HotelPropertyReviews;
import de.hotel.android.library.domain.model.data.HdeValuationDetail;
import de.hotel.android.library.domain.model.enums.HdeGuestType;
import de.hotel.android.library.remoteaccess.adapter.mocks.Paths;
import de.hotel.android.library.remoteaccess.adapter.mocks.RequestUnawareSoapService;
import de.hotel.android.library.remoteaccess.v28.querymapping.HdeV28PropertyReviewsRequestMapper;
import de.hotel.android.library.remoteaccess.v28.resultmapping.HdeV28PropertyReviewsResultMapper;

import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class HdePropertyReviewsAdapterTest extends InstrumentationTestCase {

    // This is needed because dexmaker has some problems without this, so mockito crashes
    // See http://stackoverflow.com/questions/12267572/mockito-dexmaker-on-android
    @Override
    protected void setUp() throws Exception {
        super.setUp();

        System.setProperty(
                "dexmaker.dexcache",
                getInstrumentation().getTargetContext().getCacheDir().getPath());
    }

    // Data for ALL_GUESTS reviews
    private static final int ALL_GUESTS_NUMBER_OF_REVIEWS = 215;
    private static final Float ALL_GUESTS_OVERALL_EVALUATION = 8.7f;
    private static final Float ALL_GUESTS_CLEANLINESS = 9.1f;
    private static final Float ALL_GUESTS_FRIENDLINESS = 9.2f;
    private static final Float ALL_GUESTS_CATERING = 8.5f;
    private static final Float ALL_GUESTS_RATIO_OF_PRICE_AND_PERFORMANCE = 8.7f;
    private static final Float ALL_GUESTS_RECOOMENDATIONS = 9.4f;
    private static final Float ALL_GUESTS_ROOM_QUALITY = 8.3f;
    private static final Float ALL_GUESTS_ROOM_NOISE = 8.4f;

    public void testThatReviewsAreReturnedProperly() {
        // Arrange
        HdePropertyReviewsAdapterImpl reviewsAdapter = createReviewsAdapter(Paths.PROPERTY_REVIEWS_RESPONSE);

        // Act
        HotelPropertyReviews hotelPropertyReviews = reviewsAdapter.fetchPropertyReviews(null);

        // Assert
        assertValuationDetails(hotelPropertyReviews, HdeGuestType.ALL_GUESTS);
    }

    public void testThatNoReviewsAreHandledProperly() {
        // Arrange
        HdePropertyReviewsAdapterImpl reviewsAdapter = createReviewsAdapter(Paths.PROPERTY_REVIEWS_RESPONSE_NO_REVIEWS);

        // Act
        HotelPropertyReviews hotelPropertyReviews = reviewsAdapter.fetchPropertyReviews(null);

        // Assert
        assertThat(hotelPropertyReviews).isNotNull();
        assertThat(hotelPropertyReviews.getCategoryDetailsMap()).isEmpty();
    }

    public void testThatTextualReviewsAreHandledProperly() {
        // Arrange
        HdePropertyReviewsAdapterImpl reviewsAdapter = createReviewsAdapter(Paths.PROPERTY_REVIEWS_RESPONSE_WITH_COMMENTS);

        // Act
        HotelPropertyReviews hotelPropertyReviews = reviewsAdapter.fetchPropertyReviews(null);

        // Assert
        assertThat(hotelPropertyReviews).isNotNull();
        assertThat(hotelPropertyReviews.getCategoryDetailsMap()).hasSize(8);
        assertThat(hotelPropertyReviews.getCustomerReviews()).isNotNull();
        assertThat(hotelPropertyReviews.getCustomerReviews()).hasSize(5);
    }

    private void assertValuationDetails(HotelPropertyReviews hotelPropertyReviews, @HdeGuestType int allGuests) {
        HdeValuationDetail details = hotelPropertyReviews.getValuationDetail(allGuests);
        assertThat(details).isNotNull();

        assertThat(details.getNumberOfReviews()).isEqualTo(ALL_GUESTS_NUMBER_OF_REVIEWS);
        assertThat(details.getOverallEvaluation()).isEqualTo(ALL_GUESTS_OVERALL_EVALUATION);

        assertThat(details.getCleanliness()).isEqualTo(ALL_GUESTS_CLEANLINESS);
        assertThat(details.getFriendliness()).isEqualTo(ALL_GUESTS_FRIENDLINESS);
        assertThat(details.getCatering()).isEqualTo(ALL_GUESTS_CATERING);
        assertThat(details.getRatioOfPriceAndPerformance()).isEqualTo(ALL_GUESTS_RATIO_OF_PRICE_AND_PERFORMANCE);
        assertThat(details.getRecommendationToOtherGuests()).isEqualTo(ALL_GUESTS_RECOOMENDATIONS);
        assertThat(details.getRoomQuality()).isEqualTo(ALL_GUESTS_ROOM_QUALITY);
        assertThat(details.getRoomNoise()).isEqualTo(ALL_GUESTS_ROOM_NOISE);
    }

    private HdePropertyReviewsAdapterImpl createReviewsAdapter(String file) {
        return new HdePropertyReviewsAdapterImpl(
                new RequestUnawareSoapService(file),
                Mockito.mock(HdeV28PropertyReviewsRequestMapper.class),
                new HdeV28PropertyReviewsResultMapper());
    }
}
