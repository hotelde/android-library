package de.hotel.android.library.remoteaccess.adapter;

import android.test.suitebuilder.annotation.Suppress;

import junit.framework.TestCase;

import java.util.Locale;

import de.hotel.android.library.HotelDe;
import de.hotel.android.library.domain.model.enums.RegistrationResultCode;
import de.hotel.android.library.domain.model.query.CustomerRegistrationQuery;
import de.hotel.android.library.domain.model.response.RegistrationResponse;
import de.hotel.android.library.remoteaccess.RemoteAccessTargetEnvironmentType;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Runs against the real COLA service. Use for developing/debugging purposes.
 */
@Suppress
public class HdeCustomerRegistrationAdapterIntegrationTest extends TestCase {
    private HotelDe hde;

    public void setUp() {
        hde = new HotelDe();
    }

    public void testRegisterCustomerWithoutPhoneNumber() {
        // Arrange
        HdeCustomerRegistrationAdapter registrationAdapter = hde.createCustomerRegistrationAdapter(RemoteAccessTargetEnvironmentType.BETA);

        CustomerRegistrationQuery query = new CustomerRegistrationQuery();
        query.setFirstName("Hallo");
        query.setLastName("Welt");
        query.setEmail("hallowelt15@hotel.de");
        query.setLocale(createLocale());
        query.setPassword("123456");

        // Act
        @RegistrationResultCode int resultCode = registrationAdapter.registerCustomer(query);

        // Assert
        assertThat(resultCode).isEqualTo(RegistrationResultCode.CUSTOMER_ERROR_EMAIL_EXISTS);
    }

    public void testRegisterCompanyCustomer() {
        // Arrange
        HdeCustomerRegistrationAdapter registrationAdapter = hde.createCustomerRegistrationAdapter(RemoteAccessTargetEnvironmentType.BETA);

        CustomerRegistrationQuery query = new CustomerRegistrationQuery();
        query.setFirstName("Hallo");
        query.setLastName("Welt");
        query.setEmail("hallowelt16@hotel.de");
        query.setLocale(createLocale());
        query.setPassword("123456");
        query.setCompanyName("Hallo Welt AG");

        // Act
        @RegistrationResultCode int resultCode = registrationAdapter.registerCustomer(query);

        // Assert
        assertThat(resultCode).isEqualTo(RegistrationResultCode.CUSTOMER_ERROR_EMAIL_EXISTS);
    }

    public void testRegisterCustomerWithPhoneNumber() {
        // Arrange
        HdeCustomerRegistrationAdapter registrationAdapter = hde.createCustomerRegistrationAdapter(RemoteAccessTargetEnvironmentType.BETA);

        CustomerRegistrationQuery query = new CustomerRegistrationQuery();
        query.setFirstName("Hallo");
        query.setLastName("Welt");
        query.setEmail("hallowelt18@hotel.de");
        query.setPhoneNumber("+49 1234564");
        query.setLocale(createLocale());
        query.setPassword("123456");

        // Act
        @RegistrationResultCode int resultCode = registrationAdapter.registerCustomer(query);

        // Assert
        assertThat(resultCode).isEqualTo(RegistrationResultCode.CUSTOMER_ERROR_EMAIL_EXISTS);
    }

    private Locale createLocale() {
        return new Locale("en");
    }
}
