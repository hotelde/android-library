package de.hotel.android.library.remoteaccess.adapter;

import android.test.InstrumentationTestCase;

import de.hotel.android.library.domain.model.Hotel;
import de.hotel.android.library.domain.model.enums.CreditCardType;
import de.hotel.android.library.domain.model.enums.PictureCategory;
import de.hotel.android.library.remoteaccess.adapter.mocks.Paths;
import de.hotel.android.library.remoteaccess.adapter.mocks.RequestUnawareSoapService;
import de.hotel.android.library.remoteaccess.v28.querymapping.HdeV28PropertyDescriptionRequestMapper;
import de.hotel.android.library.remoteaccess.v28.resultmapping.HdeV28PropertyDescriptionResultMapper;

import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings({"magicnumber", "javadoctype"})
public class HdePropertyDescriptionAdapterTest extends InstrumentationTestCase {

    // This is needed because dexmaker has some problems without this, so mockito crashes
    // See http://stackoverflow.com/questions/12267572/mockito-dexmaker-on-android
    @Override
    protected void setUp() throws Exception {
        super.setUp();

        System.setProperty(
                "dexmaker.dexcache",
                getInstrumentation().getTargetContext().getCacheDir().getPath());
    }

    public void testThatCorrectPropertiesAreReturned() {
        // Arrange
        PropertyDescriptionAdapter propertyDescriptionAdapter = createPropertyDescriptionAdapter(Paths.PROPERTY_DESCRIPTION_RESPONSE);

        // Act
        Hotel hotel = propertyDescriptionAdapter.fetchHotelDescription(null);

        // Assert
        assertThat(hotel).isNotNull();
        assertThat(hotel.getHotelProperties()).isNotNull();
        assertThat(hotel.getHotelProperties().getPropertyDescription()).isNotNull();
        assertThat(hotel.getHotelProperties().getPropertyDescription().getPropertyNumber()).isEqualTo(296998);
        assertThat(hotel.getHotelProperties().getHotelDescription().getHtmlDescription()).contains("<p>Hotel built in 1903, Last complete renovation 2007, Last partial renovation 2011, Number of floors: 2, Total number of rooms: 35, Number of single rooms: 15, Number of double rooms: 20, of which have 2 separate beds: 20, of which are suitable as three-bed rooms: 4, of which are suitable as four-bed rooms: 3, Bed-and-breakfast hotel , City hotel , Station hotel , Motel , Apartment/Suite hotel , Holiday hotel , family-run hotel , child-friendly hotel , Cycling/biker hotel , Hiking hotel , No-smoking hotel , Gay and lesbian-friendly hotel</p> <p>&nbsp;</p> <p>&nbsp;</p> <p>&nbsp;</p> <p>SMOKELESS HOTEL<br />From buildings of the former Kavallerieregimentes a bright, friendly hotel plant with personal atmosphere in calm situation developed with much love for the detail.</p> <p>&nbsp;</p> <p>&nbsp;</p> <p>&nbsp;</p> <p>NON-SMOKING HOTEL. Please notify the hotel upon arrival outside the reception desk time. On the generous hotel area sufficiently free parking lots are to you at the disposal.</p>");
        assertThat(hotel.getHotelProperties().getHotelDescription().getHtmlDescription()).contains("Reception hours:");
        assertThat(hotel.getHotelProperties().getHotelDescription().getHtmlDescription()).contains("Every day:\tfrom 00:00 to 00:00");

        assertThat(hotel.getHotelProperties().getHotelDescription().getPlainTextDescription()).contains("The hotel's qualified staff can't wait to greet you and begin your time with us! Our whole team would like to enthusiastically welcome you to the PrimaVera parco. Apartment Hotels are easy to find, but they are not all created equal. Make the right choice and book a room at this quaint hotel to enjoy all of its features and benefits! Our hotel offers 29 double rooms and 30 single rooms. The hotel's garden terrace area is an ideal place to take a load off. Whether you would like to browse the news, google the location of your next outing, translate a request into the local language, play an online game or share your holiday with friends, the WLan internet access in the hotel gives you the opportunity to do so. For your comfort we supply changing mats for our guests still in diapers. For your safety the hotel entrance is protected by Video surveillance. The hotel parking spaces are also monitored by patrolled security. During your stay, you may also enjoy a cold drink from our drink machine. Staying for an extended period of time? No problem, we have a laundry service available for hotel clients. Avoid the stress of slow check outs. The hotel's express check-out service smoothens your departure. Do you need a pet friendly hotel? We allow pets, including huge dogs. Articles of value will be stored securely in the strong box at reception. We provide hotel clients with car parking spots including a secure parking area for motorbikes of all kinds. For your safety the hotel entrance is protected by Video surveillance. One of the hotel services is a microwave featured in every single room. Make use of it and prepare an easy dish especially when you are in a hurry. Use the WLan internet access in every single hotel room to play an online game or share your holiday with friends. An enjoyable stay is ensured thanks to the windows in every single hotel room that are sound proof. Open the windows in your suite's bathroom and enjoy the fresh air. Right from your room you can stay informed about global affairs with the newspaper offered to. You will find a minibar filled with a large selection of beverages, a kettle for brewing a cup of coffee in the morning as well as a hand-held blow-dryer in every single hotel room. Every single room at our hotel features a bathroom with a hand-held blow-dryer and towel warmers next to the shower. Horseback-riding is one of the leisure activities available in our hotel. It is really popular among our guests and is a great opportunity to share some fun moments with your family. One of the leisure activities available in our hotel is taking a walk on our walking paths. It is really popular among our guests and features a great opportunity to share some fun moments with your family. Squash is one of the leisure activities available in our hotel and is really popular among our guests. The leisure activities in our hotel consist of many different kinds of sports. Our hotel provides the necessary equipment and the games are really popular among our guests. Boule is one of the leisure activities available in our hotel and is really popular among our guests. Mini-golf is one of the leisure activities available in our hotel. It is really popular among our guests and is a great opportunity to share some fun moments with your family. Bikes are available for hire! A billiard table is offered to our guests. The hotel's skittles alley is a popular spot among all our guests, young and old alike. Fishing trips and boat tours are available during your stay. Chat about all and sundry while enjoying a sociable bottled or mixed drink at our lively bar. </p>The hotel has a wide range of vegetarian meals available to guests. Enjoy our hotel's nutritious goodies entailing loads of whole-grain products. Have our shuttle bus take you to one of the nearby trade expos.");
        assertThat(hotel.getHotelProperties().getHotelDescription().getPlainTextDescription()).contains("Reception hours:");
        assertThat(hotel.getHotelProperties().getHotelDescription().getPlainTextDescription()).contains("Every day:\tfrom 00:00 to 00:00");

        assertThat(hotel.getHotelProperties().getHotelAmenities()).hasSize(23);
        assertThat(hotel.getHotelProperties().getLeisureAmenities()).hasSize(11);
        assertThat(hotel.getHotelProperties().getGastronomyAmenities()).hasSize(3);
        assertThat(hotel.getHotelProperties().getRoomAmenities()).hasSize(21);
        assertThat(hotel.getHotelProperties().getComplimentaryServices()).hasSize(2);

        assertThat(hotel.getHotelProperties().getAcceptedCreditCards()).hasSize(2);
        assertThat(hotel.getHotelProperties().getAcceptedCreditCards().get(0)).isEqualTo(CreditCardType.VISA);
        assertThat(hotel.getHotelProperties().getAcceptedCreditCards().get(1)).isEqualTo(CreditCardType.MASTER_CARD);

        assertThat(hotel.getHotelProperties().getPictures()).hasSize(33);
        assertThat(hotel.getHotelProperties().getPictures().get(8).getUrl()).isEqualTo("https://www.hotel.de/media/hotel/pictures/296/296998/original/Other_635332779576133252.jpg");
        assertThat(hotel.getHotelProperties().getPictures().get(8).getCategory()).isEqualTo(PictureCategory.UNKNOWN);
        assertThat(hotel.getHotelProperties().getPictures().get(9).getUrl()).isEqualTo("https://www.hotel.de/media/hotel/pictures/296/296998/original/Room_635332779920746088.jpg");
        assertThat(hotel.getHotelProperties().getPictures().get(9).getCategory()).isEqualTo(PictureCategory.ROOM);
        assertThat(hotel.getHotelProperties().getPictures().get(10).getUrl()).isEqualTo("https://www.hotel.de/media/hotel/pictures/296/296998/original/Surounding_635332779594073712.jpg"); // This tests that the 10th picture is actually not the duplicate of the logo found in the XML
        assertThat(hotel.getHotelProperties().getPictures().get(10).getCategory()).isEqualTo(PictureCategory.SURROUNDING);
    }

    public void testThatEmptyAmenitiesDontCrash() {
        // Arrange
        PropertyDescriptionAdapter propertyDescriptionAdapter = createPropertyDescriptionAdapter(Paths.PROPERTY_DESCRIPTION_RESPONSE_NO_LEISURE_AMENITIES);

        // Act
        Hotel hotel = propertyDescriptionAdapter.fetchHotelDescription(null);

        // Assert
        assertThat(hotel).isNotNull();
        assertThat(hotel.getHotelProperties()).isNotNull();
        assertThat(hotel.getHotelProperties().getPropertyDescription()).isNotNull();
        assertThat(hotel.getHotelProperties().getPropertyDescription().getPropertyNumber()).isEqualTo(399056);

        assertThat(hotel.getHotelProperties().getHotelAmenities()).hasSize(3);
        assertThat(hotel.getHotelProperties().getLeisureAmenities()).hasSize(0);
        assertThat(hotel.getHotelProperties().getGastronomyAmenities()).hasSize(0);
        assertThat(hotel.getHotelProperties().getRoomAmenities()).hasSize(1);
        assertThat(hotel.getHotelProperties().getComplimentaryServices()).hasSize(1);
    }

    public void testThatPropertyNotFoundIsHandledCorrectly() {
        // Arrange
        PropertyDescriptionAdapter propertyDescriptionAdapter = createPropertyDescriptionAdapter(Paths.PROPERTY_DESCRIPTION_PROPERTY_NOT_FOUND);

        // Act
        Hotel hotel = propertyDescriptionAdapter.fetchHotelDescription(null);

        // Assert
        assertThat(hotel).isNull();
    }

    private static PropertyDescriptionAdapter createPropertyDescriptionAdapter(String file) {
        return new HdePropertyDescriptionAdapterImpl(
                new RequestUnawareSoapService(file),
                Mockito.mock(HdeV28PropertyDescriptionRequestMapper.class),
                new HdeV28PropertyDescriptionResultMapper());
    }
}
