package de.hotel.android.library.domain.model.query;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.model.data.Locale;

/**
 * TODO Change from Locale to java locale or just to an ISO A2 code
 */
public abstract class Query {

    private Locale locale;

    @NonNull
    public Locale getLocale() {
        return locale;
    }

    public void setLocale(@NonNull Locale locale) {
        this.locale = locale;
    }
}
