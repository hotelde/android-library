package de.hotel.android.library.remoteaccess.soap;

import android.util.Xml;

import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.io.StringWriter;

public class HdeV30SOAPRequestBuilderImpl implements SOAPRequestBuilder {
    public static final String TAG = HdeV30SOAPRequestBuilderImpl.class.getSimpleName();

    private static final String NAMESPACE_SOAP = "http://www.w3.org/2003/05/soap-envelope";
    private static final String NAMESPACE_OTA = "http://www.opentravel.org/OTA/2003/05";
    private static final String NAMESPACE_SECURITY = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
    private static final String UTF_8 = "UTF-8";
    private static final String PREFIX_SOAP = "soap";
    private static final String PREFIX_NS = "ns";
    private static final String PREFIX_O = "o";

    private final String endpointUsername;
    private final String endpointPassword;

    public HdeV30SOAPRequestBuilderImpl(String username, String password) {
        endpointUsername = username;
        endpointPassword = password;
    }

    @Override
    public String buildSOAPRequest(String content) throws IOException {
        XmlSerializer xmlSerializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        xmlSerializer.setOutput(writer);

        xmlSerializer.startDocument(UTF_8, false);
        xmlSerializer.setPrefix(PREFIX_SOAP, NAMESPACE_SOAP);
        xmlSerializer.setPrefix(PREFIX_NS, NAMESPACE_OTA);

        xmlSerializer.startTag(NAMESPACE_SOAP, "Envelope");
        xmlSerializer.startTag(NAMESPACE_SOAP, "Header");

        xmlSerializer.setPrefix(PREFIX_O, NAMESPACE_SECURITY);
        xmlSerializer.startTag(NAMESPACE_SECURITY, "Security");
        xmlSerializer.attribute(NAMESPACE_SECURITY, "mustUnderstand", "1");
        xmlSerializer.startTag(NAMESPACE_SECURITY, "UsernameToken");
        xmlSerializer.startTag(NAMESPACE_SECURITY, "Username");
        xmlSerializer.text(endpointUsername);
        xmlSerializer.endTag(NAMESPACE_SECURITY, "Username");
        xmlSerializer.startTag(NAMESPACE_SECURITY, "Password");
        xmlSerializer.text(endpointPassword);
        xmlSerializer.endTag(NAMESPACE_SECURITY, "Password");
        xmlSerializer.endTag(NAMESPACE_SECURITY, "UsernameToken");
        xmlSerializer.endTag(NAMESPACE_SECURITY, "Security");

        xmlSerializer.endTag(NAMESPACE_SOAP, "Header");

        xmlSerializer.startTag(NAMESPACE_SOAP, "Body");
        xmlSerializer.flush();
        writer.write(content);
        xmlSerializer.endTag(NAMESPACE_SOAP, "Body");

        xmlSerializer.endTag(NAMESPACE_SOAP, "Envelope");
        xmlSerializer.endDocument();

        return writer.toString();
    }
}
