package de.hotel.android.library.domain.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * One concrete RoomRate
 * which can be reserved or which has already been reserved.
 */
public class HdeRoomRate implements Parcelable {

    public static final Parcelable.Creator<HdeRoomRate> CREATOR = new Parcelable.Creator<HdeRoomRate>() {
        @Override
        public HdeRoomRate createFromParcel(Parcel parcel) {
            return new HdeRoomRate(parcel);
        }

        @Override
        public HdeRoomRate[] newArray(int i) {
            return new HdeRoomRate[i];
        }
    };
    private RoomDescription roomDescription;
    private List<HdeRate> hdeRates = new ArrayList<>();
    private String iso3CurrencyHotel;
    private String iso3CurrencyCustomer;
    private BigDecimal totalRoomPriceGrossHotel;
    private BigDecimal totalRoomPriceGrossCustomer;
    private BigDecimal totalRoomPriceNetHotel;
    private BigDecimal totalRoomPriceNetCustomer;

    public HdeRoomRate() {
    }

    public HdeRoomRate(Parcel parcel) {
        roomDescription = parcel.readParcelable(RoomDescription.class.getClassLoader());
        parcel.readTypedList(hdeRates, HdeRate.CREATOR);

        iso3CurrencyHotel = parcel.readString();
        iso3CurrencyCustomer = parcel.readString();

        totalRoomPriceGrossHotel = (BigDecimal) parcel.readSerializable();
        totalRoomPriceGrossCustomer = (BigDecimal) parcel.readSerializable();
        totalRoomPriceNetHotel = (BigDecimal) parcel.readSerializable();
        totalRoomPriceNetCustomer = (BigDecimal) parcel.readSerializable();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(roomDescription, i);
        parcel.writeTypedList(hdeRates);

        parcel.writeString(iso3CurrencyHotel);
        parcel.writeString(iso3CurrencyCustomer);

        parcel.writeSerializable(totalRoomPriceGrossHotel);
        parcel.writeSerializable(totalRoomPriceGrossCustomer);
        parcel.writeSerializable(totalRoomPriceNetHotel);
        parcel.writeSerializable(totalRoomPriceNetCustomer);
    }


    public RoomDescription getRoomDescription() {
        return roomDescription;
    }

    public void setRoomDescription(RoomDescription roomDescription) {
        this.roomDescription = roomDescription;
    }

    @Nullable
    public String getIso3CurrencyHotel() {
        return iso3CurrencyHotel;
    }

    public void setIso3CurrencyHotel(@Nullable String iso3CurrencyHotel) {
        this.iso3CurrencyHotel = iso3CurrencyHotel;
    }

    @Nullable
    public String getIso3CurrencyCustomer() {
        return iso3CurrencyCustomer;
    }

    public void setIso3CurrencyCustomer(@Nullable String iso3CurrencyCustomer) {
        this.iso3CurrencyCustomer = iso3CurrencyCustomer;
    }

    @Nullable
    public BigDecimal getTotalRoomPriceGrossHotel() {
        return totalRoomPriceGrossHotel;
    }

    public void setTotalRoomPriceGrossHotel(@Nullable BigDecimal totalRoomPriceGrossHotel) {
        this.totalRoomPriceGrossHotel = totalRoomPriceGrossHotel;
    }

    @Nullable
    public BigDecimal getTotalRoomPriceGrossCustomer() {
        return totalRoomPriceGrossCustomer;
    }

    public void setTotalRoomPriceGrossCustomer(@Nullable BigDecimal totalRoomPriceGrossCustomer) {
        this.totalRoomPriceGrossCustomer = totalRoomPriceGrossCustomer;
    }

    @Nullable
    public BigDecimal getTotalRoomPriceNetHotel() {
        return totalRoomPriceNetHotel;
    }

    public void setTotalRoomPriceNetHotel(@Nullable BigDecimal totalRoomPriceNetHotel) {
        this.totalRoomPriceNetHotel = totalRoomPriceNetHotel;
    }

    @Nullable
    public BigDecimal getTotalRoomPriceNetCustomer() {
        return totalRoomPriceNetCustomer;
    }

    public void setTotalRoomPriceNetCustomer(@Nullable BigDecimal totalRoomPriceNetCustomer) {
        this.totalRoomPriceNetCustomer = totalRoomPriceNetCustomer;
    }

    public List<HdeRate> getHdeRates() {
        return hdeRates;
    }

    public void setHdeRates(List<HdeRate> hdeRates) {
        this.hdeRates = hdeRates;
    }
}
