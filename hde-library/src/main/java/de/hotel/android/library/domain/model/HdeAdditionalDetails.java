package de.hotel.android.library.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import de.hotel.android.library.domain.model.enums.AdditionalDetailsType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class HdeAdditionalDetails implements Parcelable {

    public static final Creator<HdeAdditionalDetails> CREATOR = new Creator<HdeAdditionalDetails>() {
        @Override
        public HdeAdditionalDetails createFromParcel(Parcel in) {
            return new HdeAdditionalDetails(in);
        }

        @Override
        public HdeAdditionalDetails[] newArray(int size) {
            return new HdeAdditionalDetails[size];
        }
    };
    private List<String> descriptions = new ArrayList<>();
    private
    @AdditionalDetailsType
    int detailsType;
    private BigDecimal amount;
    private String currencyCode;

    public HdeAdditionalDetails() {
    }

    protected HdeAdditionalDetails(Parcel in) {
        descriptions = in.createStringArrayList();

        int type = in.readInt();
        //noinspection ResourceType
        detailsType = type;

        currencyCode = in.readString();
        amount = (BigDecimal) in.readSerializable();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(descriptions);
        dest.writeInt(detailsType);
        dest.writeString(currencyCode);
        dest.writeSerializable(amount);
    }

    public List<String> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<String> descriptions) {
        this.descriptions = descriptions;
    }

    public
    @AdditionalDetailsType
    int getDetailsType() {
        return detailsType;
    }

    public void setDetailsType(@AdditionalDetailsType int detailsType) {
        this.detailsType = detailsType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

}
