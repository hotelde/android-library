package de.hotel.android.library.remoteaccess.v30;

import de.hotel.android.library.domain.model.enums.CreditCardType;

/**
 * Taken from http://cuinl.tripod.com/Tips/o-1.htm
 */
@SuppressWarnings({"missingswitchdefault", "magicnumber"})
public class CreditCardTypeMapper {
    public @CreditCardType int determineTypeByNumber(String creditCardNumber) {
        creditCardNumber = creditCardNumber.replace(" ", "").replace("-", "");

        /**
               CARD TYPES             PREFIX            WIDTH
               American Express       34, 37            15
               Diners Club            300 to 305, 36    14
               Carte Blanche          38                14
               Discover               6011              16
               EnRoute                2014, 2149        15
               JCB                    3                 16
               JCB                    2131, 1800        15
               Master Card            51 to 55          16
               Visa                   4                 13, 16
         */

//      Check the first two digits first
        switch (creditCardNumber.substring(0, 2)) {
            case "34":
            case "37":
                return CreditCardType.AMERICAN_EXPRESS;
            case "36":
                return CreditCardType.DINERS_CLUB;
//            case "38"
//                return "Carte Blanche";
            case "51":
            case "52":
            case "53":
            case "54":
            case "55":
                return CreditCardType.MASTER_CARD;
        }

//      None of the above - so check the first four digits collectively
        switch (creditCardNumber.substring(0, 4)) {
//            case "2014":
//            case "2149":
//                return "EnRoute"
            case "2131":
            case "1800":
                return CreditCardType.JCB;
//            case "6011":
//                return CreditCardType = "Discover";
        }

//      None of the above - so check the first three digits collectively
        switch (creditCardNumber.substring(0, 3)) {
            case "300":
            case "301":
            case "302":
            case "303":
            case "304":
            case "305":
                return CreditCardType.DINERS_CLUB;
        }

//      None of the above - so simply check the first digit
        switch (creditCardNumber.substring(0, 1)) {
            case "3":
                return CreditCardType.JCB;
            case "4":
                return CreditCardType.VISA;
        }

        return CreditCardType.UNKNOWN;
    }
}