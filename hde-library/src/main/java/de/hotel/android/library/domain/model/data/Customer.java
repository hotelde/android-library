package de.hotel.android.library.domain.model.data;

import android.os.Parcel;
import android.os.Parcelable;

import de.hotel.android.library.util.Dates;

public class Customer implements Parcelable {
    public static final Parcelable.Creator<Customer> CREATOR = new Parcelable.Creator<Customer>() {
        @Override
        public Customer createFromParcel(Parcel parcel) {
            return new Customer(parcel);
        }

        @Override
        public Customer[] newArray(int i) {
            return new Customer[i];
        }
    };
    private String firstname;
    private String lastname;
    private String phone;
    private String email;
    private String creditCardNumber;
    private Address address;
    private String password;
    private int customerNumber;
    private Integer companyNumber;
    private String companyName;
    private long lastLoginTime;

    public Customer() {
    }

    public Customer(Parcel parcel) {
        firstname = parcel.readString();
        lastname = parcel.readString();
        phone = parcel.readString();
        email = parcel.readString();
        creditCardNumber = parcel.readString();

        address = parcel.readParcelable(Address.class.getClassLoader());

        customerNumber = parcel.readInt();
        try {
            // Do not use readInt because writing happens with the writeValue method
            companyNumber = (Integer) parcel.readValue(Integer.class.getClassLoader());
        } catch (NullPointerException e) {
            // Nullable company number remains null
        }
        companyName = parcel.readString();
        lastLoginTime = parcel.readLong();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(firstname);
        parcel.writeString(lastname);
        parcel.writeString(phone);
        parcel.writeString(email);
        parcel.writeString(creditCardNumber);

        // Do not parcel the password for security reasons

        parcel.writeParcelable(address, i);
        parcel.writeInt(customerNumber);
        // Do not use writeInt because that method cannot handle nullable Integers
        // Do not skip writing this value either because then the parcel cannot be read correctly
        parcel.writeValue(companyNumber);
        parcel.writeString(companyName);
        parcel.writeLong(lastLoginTime);
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(int customerNumber) {
        this.customerNumber = customerNumber;
    }

    public Integer getCompanyNumber() {
        return companyNumber;
    }

    public void setCompanyNumber(int companyNumber) {
        this.companyNumber = companyNumber;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public long getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(long lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("\nLast Login Time: ").append(Dates.dayDateYearHourMinute(getLastLoginTime()));
        builder.append("\nFirst Name: ").append(firstname);
        builder.append("\nLast Name: ").append(lastname);
        builder.append("\nAddress: ").append(address);
        builder.append("\nPhone No: ").append(phone);
        builder.append("\nCredit Card No: ").append(creditCardNumber);
        builder.append("\nEmail-Addresse: ").append(email);
        builder.append("\nCompany No: ").append(companyNumber);
        builder.append("\nCompany Name: ").append(companyName);
        builder.append("\nCustomer No: ").append(customerNumber);
        return builder.toString();
    }

    public String asRow() {
        StringBuilder builder = new StringBuilder();
        builder.append("Last Login  Time: ").append(getLastLoginTime());
        builder.append("; Date: ").append(Dates.dayDateYearHourMinute(getLastLoginTime()));
        builder.append("; First Name: ").append(firstname);
        builder.append("; Last Name: ").append(lastname);
        builder.append(", Email-Addresse: ").append(email);
        builder.append(", Customer No: ").append(customerNumber);
        builder.append(", Company No: ").append(companyNumber);
        builder.append(", Company Name: ").append(companyName);
        builder.append(", Credit Card No: ").append(creditCardNumber);
        builder.append(", Phone: ").append(phone);
        return builder.toString();
    }
}
