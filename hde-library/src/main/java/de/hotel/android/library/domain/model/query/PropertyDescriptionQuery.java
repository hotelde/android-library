package de.hotel.android.library.domain.model.query;

import de.hotel.android.library.domain.model.data.Language;

public class PropertyDescriptionQuery {
    private Language language;
    private String hotelId;
    private int companyNumber = 0;

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public int getCompanyNumber() {
        return companyNumber;
    }

    public void setCompanyNumber(int companyNumber) {
        this.companyNumber = companyNumber;
    }
}
