package de.hotel.android.library.remoteaccess.soap;

import java.io.IOException;

public interface SOAPRequestBuilder {
    String buildSOAPRequest(String content) throws IOException;
}
