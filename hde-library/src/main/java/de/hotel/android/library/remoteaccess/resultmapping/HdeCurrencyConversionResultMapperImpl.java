package de.hotel.android.library.remoteaccess.resultmapping;

import android.support.annotation.Nullable;

import com.hrsgroup.remoteaccess.BuildConfig;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.OTAHotelAvailRS;
import de.hotel.android.library.util.Lists;

import java.math.BigDecimal;

public class HdeCurrencyConversionResultMapperImpl implements HdeCurrencyConversionResultMapper {
    /**
     * Calculates the currency conversion factor "X" for following equation:
     * hotel currency amount * X = customer currency amount
     */
    @Override
    public BigDecimal getCustomerCurrencyFactor(@Nullable OTAHotelAvailRS.CurrencyConversions currencyConversions, String iso3CurrencyHotel, String iso3CurrencyCustomer) {
        if (currencyConversions == null || Lists.size(currencyConversions.getCurrencyConversionList()) <= 0) {
            return null;
        }

        // HSBW does not always return conversion values
        BigDecimal customerCurrencyFactor = null;
        for (OTAHotelAvailRS.CurrencyConversions.CurrencyConversion currencyConversion : currencyConversions.getCurrencyConversionList()) {
            if (iso3CurrencyCustomer.equals(currencyConversion.getRequestedCurrencyCode())) {
                if (BuildConfig.DEBUG && !(iso3CurrencyHotel.equals(currencyConversion.getSourceCurrencyCode()))) {
                    throw new AssertionError();
                }
                customerCurrencyFactor = currencyConversion.getRateConversion();
            }
        }
        return customerCurrencyFactor;
    }
}
