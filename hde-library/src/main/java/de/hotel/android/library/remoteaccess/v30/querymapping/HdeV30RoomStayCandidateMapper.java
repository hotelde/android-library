package de.hotel.android.library.remoteaccess.v30.querymapping;

import de.hotel.android.library.domain.model.criterion.RoomCriterion;
import de.hotel.android.library.domain.model.enums.RoomType;
import de.hotel.android.library.remoteaccess.v30.OtaConstants;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.GuestCountGroup;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.GuestCountType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.RoomGroup;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.RoomStayCandidateType;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Johannes Mueller
 */
public final class HdeV30RoomStayCandidateMapper {

    public static final int ONE = 1;
    public static final int TWO = 2;
    public static final int THREE = 3;
    public static final int FOUR = 4;

    private HdeV30RoomStayCandidateMapper() {
        // Don't instantiate me.
    }

    public static List<RoomStayCandidateType> roomStayCandidateTypes(List<RoomCriterion> roomCriterionList) {
        assert roomCriterionList != null;

        ArrayList<RoomStayCandidateType> roomStayCandidateTypes = new ArrayList<>();

        RoomStayCandidateType roomStayCandidate = new RoomStayCandidateType();

		/*
         * The domain model forces every room to be requested separately, see
		 * {@link HotelAvailCriterion}. HDE v3.0, however, allows only one room
		 * type to be requested at once. Therefore, only the first {@link
		 * RoomCriterion} is used.
		 */
        RoomCriterion roomCriterion = roomCriterionList.get(0);

        RoomGroup roomGroup = new RoomGroup();
        roomGroup.setBedTypeCodes(null);
        roomGroup.setPromotionVendorCodes(null);
        roomGroup.setQuantity(BigInteger.valueOf(roomCriterion.getQuantity()));
        roomStayCandidate.setRoomGroup(roomGroup);

        GuestCountType guestCounts = new GuestCountType();
        guestCounts.setIsPerRoom(true);
        GuestCountType.GuestCount guestCount = new GuestCountType.GuestCount();
        GuestCountGroup guestCountGroup = new GuestCountGroup();
        guestCountGroup.setAgeQualifyingCode(OtaConstants.OTA_AGE_QUALIFYING_CODE_ADULT);

        switch (roomCriterion.getRoomType()) {
            case RoomType.SINGLE:
                guestCountGroup.setCount(BigInteger.valueOf(ONE));
                break;
            case RoomType.DOUBLE:
                guestCountGroup.setCount(BigInteger.valueOf(TWO));
                break;
            case RoomType.THREE_BED:
                guestCountGroup.setCount(BigInteger.valueOf(THREE));
                break;
            case RoomType.FAMILY:
                guestCountGroup.setCount(BigInteger.valueOf(FOUR));
                break;
            default:
                guestCountGroup.setCount(BigInteger.valueOf(TWO));
        }

        guestCount.setGuestCountGroup(guestCountGroup);
        ArrayList<GuestCountType.GuestCount> guestCountsList = new ArrayList<>();
        guestCountsList.add(guestCount);
        guestCounts.setGuestCountList(guestCountsList);

        roomStayCandidate.setGuestCounts(guestCounts);

        roomStayCandidateTypes.add(roomStayCandidate);
        return roomStayCandidateTypes;
    }
}
