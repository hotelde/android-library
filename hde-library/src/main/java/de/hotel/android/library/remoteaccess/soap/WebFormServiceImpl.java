package de.hotel.android.library.remoteaccess.soap;

import android.support.annotation.NonNull;

import java.io.BufferedOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Map;

import de.hotel.android.library.remoteaccess.cola.WebFormRequestBuilder;
import de.hotel.android.library.remoteaccess.resultmapping.HdeJsonMapper;
import timber.log.Timber;

public class WebFormServiceImpl implements WebFormService {

    private static final String UTF_8 = "UTF-8";
    private static final String PARAMETER_DELIMITER = "&";
    private static final String KEY_VALUE_DELIMITER = "=";

    private final WebFormRequestBuilder webFormRequestBuilder;
    private final HdeJsonMapper jsonResponseMapper;

    public WebFormServiceImpl(@NonNull WebFormRequestBuilder webFormRequestBuilder, @NonNull HdeJsonMapper jsonResponseMapper) {
        this.webFormRequestBuilder = webFormRequestBuilder;
        this.jsonResponseMapper = jsonResponseMapper;
    }

    @Override
    public <S> S performRequest(Map<String, String> parameters, Class<S> type) {

        HttpURLConnection connection = null;
        try {
            String requestBody = createQueryStringForParameters(parameters);
            connection = (HttpURLConnection) webFormRequestBuilder.createWebFormUrlConnection(requestBody.getBytes(Charset.forName(UTF_8)).length);

            BufferedOutputStream outputStream = new BufferedOutputStream(connection.getOutputStream());
            outputStream.write(requestBody.getBytes(Charset.forName(UTF_8)));
            outputStream.flush();
            outputStream.close();

            return jsonResponseMapper.mapFromJson(connection.getInputStream(), type);
        } catch (Exception e) {
            Timber.e(e, "webFormRequestBuilder.createWebFormUrlConnection in registerCustomer failed");
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

        return null;
    }

    private static String createQueryStringForParameters(Map<String, String> parameters) {
        StringBuilder parametersAsQueryString = new StringBuilder();
        if (parameters != null) {
            boolean firstParameter = true;

            for (String parameterName : parameters.keySet()) {
                if (!firstParameter) {
                    parametersAsQueryString.append(PARAMETER_DELIMITER);
                }

                try {
                    parametersAsQueryString.append(parameterName)
                            .append(KEY_VALUE_DELIMITER)
                            .append(URLEncoder.encode(parameters.get(parameterName), UTF_8));
                } catch (UnsupportedEncodingException e) {
                    throw new RuntimeException(e);
                }

                firstParameter = false;
            }
        }

        return parametersAsQueryString.toString();
    }
}
