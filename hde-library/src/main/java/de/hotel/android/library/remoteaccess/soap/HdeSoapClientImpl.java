package de.hotel.android.library.remoteaccess.soap;

import android.support.annotation.NonNull;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.charset.Charset;

public class HdeSoapClientImpl implements HdeSoapClient {

    private static final String UTF_8 = "UTF-8";

    private final ConnectionBuilder connectionBuilderImpl;

    public HdeSoapClientImpl(@NonNull ConnectionBuilder connectionBuilderImpl) {
        this.connectionBuilderImpl = connectionBuilderImpl;
    }

    @Override
    public URLConnection performRequest(String soapRequestContent, String soapAction) throws IOException {
        URLConnection urlConnection = connectionBuilderImpl.createUrlConnection(soapRequestContent.getBytes(Charset.forName(UTF_8)).length, soapAction);

        return executeRequest(urlConnection, soapRequestContent);
    }

    private URLConnection executeRequest(URLConnection urlConnection, String requestBody) throws IOException {
        BufferedOutputStream outputStream = new BufferedOutputStream(urlConnection.getOutputStream());
        outputStream.write(requestBody.getBytes(Charset.forName(UTF_8)));
        outputStream.flush();
        outputStream.close();

        return urlConnection;
    }
}
