package de.hotel.android.library.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import de.hotel.android.library.domain.model.enums.PictureCategory;
import de.hotel.android.library.domain.model.enums.PictureType;

public class HotelPicture implements Parcelable {
    private String url;
    private @PictureType
    int pictureType;
    private @PictureCategory
    int pictureCategory;

    public static final Creator<HotelPicture> CREATOR = new Creator<HotelPicture>() {
        @Override
        public HotelPicture createFromParcel(Parcel in) {
            return new HotelPicture(in);
        }

        @Override
        public HotelPicture[] newArray(int size) {
            return new HotelPicture[size];
        }
    };

    public HotelPicture() {

    }

    @SuppressWarnings("ResourceType")
    protected HotelPicture(Parcel in) {
        url = in.readString();
        pictureType = in.readInt();
        pictureCategory = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeInt(pictureType);
        dest.writeInt(pictureCategory);
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getType() {
        return pictureType;
    }

    public void setType(int type) {
        this.pictureType = type;
    }

    public int getCategory() {
        return pictureCategory;
    }

    public void setCategory(int category) {
        this.pictureCategory = category;
    }
}
