package de.hotel.android.library.remoteaccess.v28.resultmapping;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.model.enums.CancelResultCode;
import de.hotel.remoteaccess.v28.model.CancelReservationResponse1;

public class HdeV28CancelReservationResultMapper {

    public @CancelResultCode int mapCancelReservationResponse(@NonNull CancelReservationResponse1 response) {

        // TODO Implement
        switch (response.getCancelAttemptStatus()) {
            case CANCEL_OK:
                return CancelResultCode.CANCEL_OK;
        }

        throw new RuntimeException("Unknown cancel result");
    }
}
