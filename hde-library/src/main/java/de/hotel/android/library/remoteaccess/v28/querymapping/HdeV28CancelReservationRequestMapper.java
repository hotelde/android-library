package de.hotel.android.library.remoteaccess.v28.querymapping;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.model.query.CancelReservationQuery;
import de.hotel.android.library.domain.model.query.CheckReservationStatusQuery;
import de.hotel.remoteaccess.v28.model.CancelReservation;
import de.hotel.remoteaccess.v28.model.CancelReservationRequest;
import de.hotel.remoteaccess.v28.model.CheckReservationStatusRequest;

public class HdeV28CancelReservationRequestMapper {

    private HdeV28BaseRequestMapper baseRequestMapper;

    public HdeV28CancelReservationRequestMapper(@NonNull HdeV28BaseRequestMapper baseRequestMapper) {
        this.baseRequestMapper = baseRequestMapper;
    }

    public CancelReservation createCancelReservationRequest(CancelReservationQuery query) {
        CancelReservation cancelReservation = new CancelReservation();
        CancelReservationRequest request = new CancelReservationRequest();

        baseRequestMapper.setStandardProperties(request, query.getLocale().getLanguage().getIso2Language());

        request.setCustomerPassword(query.getPassword());
        request.setCustomerEmail(query.getEmail());
        request.setHoteldeSendsCancellationEmail(true);
        request.setReservationID(query.getReservationId());

        cancelReservation.setObjRequest(request);

        return cancelReservation;
    }
}
