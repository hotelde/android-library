package de.hotel.android.library.domain.model;

public class BookingGuarantee {

    private boolean creditCardNeeded;
    private boolean prepaidNeeded;

    public boolean isCreditCardNeeded() {
        return creditCardNeeded;
    }

    public void setCreditCardNeeded(boolean creditCardNeeded) {
        this.creditCardNeeded = creditCardNeeded;
    }

    public boolean isPrepaidNeeded() {
        return prepaidNeeded;
    }

    public void setPrepaidNeeded(boolean prepaidNeeded) {
        this.prepaidNeeded = prepaidNeeded;
    }
}
