package de.hotel.android.library.remoteaccess.adapter;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import de.hotel.android.library.domain.model.data.Language;
import de.hotel.android.library.domain.model.data.Location;
import de.hotel.android.library.domain.model.query.GetLocationsQuery;
import de.hotel.android.library.remoteaccess.soap.SoapService;
import de.hotel.android.library.remoteaccess.v28.querymapping.HdeV28DetermineLocationNumberRequestMapper;
import de.hotel.android.library.remoteaccess.v28.querymapping.HdeV28GetLocationsRequestMapper;
import de.hotel.android.library.remoteaccess.v28.resultmapping.HdeV28LocationsResultMapper;
import de.hotel.android.library.util.Constants;
import de.hotel.remoteaccess.v28.model.DetermineLocationNumber;
import de.hotel.remoteaccess.v28.model.DetermineLocationNumberResponse1;
import de.hotel.remoteaccess.v28.model.GetLocations;
import de.hotel.remoteaccess.v28.model.GetLocationsResponse1;
import de.hotel.remoteaccess.v28.v28SoapEnvelope;

public class HdeLocationAdapter {

    private final SoapService soapService;
    private final HdeV28GetLocationsRequestMapper hdeV28GetLocationsRequestMapper;
    private final HdeV28LocationsResultMapper hdeV28LocationsResultMapper;
    private final HdeV28DetermineLocationNumberRequestMapper hdeV28DetermineLocationNumberRequestMapper;

    public HdeLocationAdapter(
            @NonNull SoapService soapService,
            @NonNull HdeV28GetLocationsRequestMapper hdeV28GetLocationsRequestMapper,
            @NonNull HdeV28DetermineLocationNumberRequestMapper hdeV28DetermineLocationNumberRequestMapper,
            @NonNull HdeV28LocationsResultMapper hdeV28LocationsResultMapper) {

        this.soapService = soapService;
        this.hdeV28GetLocationsRequestMapper = hdeV28GetLocationsRequestMapper;
        this.hdeV28LocationsResultMapper = hdeV28LocationsResultMapper;
        this.hdeV28DetermineLocationNumberRequestMapper = hdeV28DetermineLocationNumberRequestMapper;
    }

    public Location getLocationById(int locationId, Language language) {
        GetLocations getLocationsRequest = hdeV28GetLocationsRequestMapper.createGetLocationsRequest(locationId, language);

        v28SoapEnvelope soapEnvelope = soapService.performRequest(getLocationsRequest, v28SoapEnvelope.class, Constants.SCHEMA_GET_GET_LOCATIONS_REQUEST, true);
        GetLocationsResponse1 locationsResult = soapEnvelope.getBody().getGetLocationsResponse().getGetLocationsResult();

        if (locationsResult.getError() != null) {
            switch (locationsResult.getError().getErrorCode()) {
                case NO_ERROR:
                    break;
                default:
                    return null;
            }
        }

        List<Location> locations = hdeV28LocationsResultMapper.mapLocations(locationsResult, language);
        if (locations.isEmpty()) {
            return null;
        }

        return locations.get(0);
    }

    public List<Location> getLocations(@NonNull GetLocationsQuery request) {
        DetermineLocationNumber determineLocationNumberRequest = hdeV28DetermineLocationNumberRequestMapper.createDetermineLocationNumberRequest(request);

        v28SoapEnvelope soapEnvelope = soapService.performRequest(determineLocationNumberRequest, v28SoapEnvelope.class, Constants.SCHEMA_DETERMINE_LOCATION_NUMBER_REQUEST, true);
        DetermineLocationNumberResponse1 determineLocationNumberResult = soapEnvelope.getBody().getDetermineLocationNumberResponse().getDetermineLocationNumberResult();

        if (determineLocationNumberResult.getError() != null) {
            switch (determineLocationNumberResult.getError().getErrorCode()) {
                case NO_ERROR:
                    break;
                case DESTINATION_NOT_FOUND:
                case MISSING_DESTINATION_DATA:
                case INVALID_COUNTRY_IS_OA3:
                default:
                    return new ArrayList<>();
            }
        }

        return hdeV28LocationsResultMapper.mapLocations(determineLocationNumberResult, request.getLanguage());
    }
}
