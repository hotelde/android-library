package de.hotel.android.library.domain.model.data;

import android.support.annotation.NonNull;

/**
 * @author Johannes Mueller
 */
public class Locale {

    private Language language;
    private String iso3Currency;

    @NonNull
    public Language getLanguage() {
        return language;
    }

    public void setLanguage(@NonNull Language language) {
        this.language = language;
    }

    @NonNull
    public String getIso3Currency() {
        return iso3Currency;
    }

    public void setIso3Currency(@NonNull String iso3Currency) {
        this.iso3Currency = iso3Currency;
    }
}
