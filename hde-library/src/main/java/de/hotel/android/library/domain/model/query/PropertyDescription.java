package de.hotel.android.library.domain.model.query;

public class PropertyDescription {
    private int propertyNumber;

    public int getPropertyNumber() {
        return propertyNumber;
    }

    public void setPropertyNumber(int propertyNumber) {
        this.propertyNumber = propertyNumber;
    }
}
