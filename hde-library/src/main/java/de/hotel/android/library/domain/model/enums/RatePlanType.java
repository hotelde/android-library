package de.hotel.android.library.domain.model.enums;

import android.support.annotation.IntDef;

@IntDef({RatePlanType.NEGOTIATED, RatePlanType.PACKAGE, RatePlanType.OTHER, RatePlanType.RACK, RatePlanType.CONSORTIA, RatePlanType.GROUP})
public @interface RatePlanType {
    int NEGOTIATED = 10;
    int PACKAGE = 11;
    int OTHER = 12;
    int RACK = 13;
    int CONSORTIA = 20;
    int GROUP = 21;
}
