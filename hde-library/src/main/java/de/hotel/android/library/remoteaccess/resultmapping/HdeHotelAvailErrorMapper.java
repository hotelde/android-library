package de.hotel.android.library.remoteaccess.resultmapping;

import de.hotel.android.library.domain.model.query.HotelAvailQuery;
import de.hotel.android.library.domain.model.HotelAvailResult;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.ErrorType;

import java.util.List;

public interface HdeHotelAvailErrorMapper {
    HotelAvailResult handleHotelAvailErrors(HotelAvailQuery query, List<ErrorType> errors);
}
