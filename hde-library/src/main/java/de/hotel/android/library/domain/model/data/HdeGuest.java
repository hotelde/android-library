package de.hotel.android.library.domain.model.data;

public class HdeGuest {

    private String firstName = "";
    private String lastName = "";
    private String eMail= "";

    public HdeGuest(String firstName, String lastName, String eMail){
        this.firstName = firstName;
        this.lastName = lastName;
        this.eMail = eMail;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEMail() {
        return eMail;
    }

    public void setEMail(String EMail) {
        this.eMail = EMail;
    }

    public String toString(){
        StringBuilder builder =  new StringBuilder(firstName);
        builder.append(" ").append(lastName).append(" ").append(eMail);
        return builder.toString();
    }
}
