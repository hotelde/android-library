package de.hotel.android.library.domain.exception;

import android.support.annotation.IntDef;

/**
 * @author Johannes Mueller
 */
@IntDef({TechnicalExceptionCode.UNKNOWN, TechnicalExceptionCode.MARSHALLING_EXCEPTION, TechnicalExceptionCode.UNMARSHALLING_EXCEPTION, TechnicalExceptionCode.HTTP_EXCEPTION,
        TechnicalExceptionCode.SSL_CERTIFICATE_EXCEPTION})
public @interface TechnicalExceptionCode {

    int UNKNOWN = 0;
    int MARSHALLING_EXCEPTION = 1;
    int UNMARSHALLING_EXCEPTION = 2;
    int HTTP_EXCEPTION = 3;
    int SSL_CERTIFICATE_EXCEPTION = 4;
}