package de.hotel.android.library.remoteaccess.adapter;

import android.support.annotation.NonNull;

import java.io.IOException;

import de.hotel.android.library.domain.model.data.Customer;
import de.hotel.android.library.remoteaccess.soap.SoapService;
import de.hotel.android.library.remoteaccess.v28.querymapping.CustomerDataQuery;
import de.hotel.android.library.remoteaccess.v28.querymapping.HdeV28GetCustomerDataRequestMapper;
import de.hotel.android.library.remoteaccess.v28.resultmapping.HdeV28GetCustomerDataResultMapper;
import de.hotel.android.library.util.Constants;
import de.hotel.remoteaccess.v28.model.GetCustomerData;
import de.hotel.remoteaccess.v28.v28SoapEnvelope;

public class HdeCustomerManagementAdapterImpl {

    private final SoapService soapService;
    private final HdeV28GetCustomerDataRequestMapper customerDataRequestMapper;
    private final HdeV28GetCustomerDataResultMapper customerDataResultMapper;

    public HdeCustomerManagementAdapterImpl(
            @NonNull SoapService soapService,
            @NonNull HdeV28GetCustomerDataRequestMapper customerDataRequestMapper,
            @NonNull HdeV28GetCustomerDataResultMapper customerDataResultMapper) {

        this.soapService = soapService;
        this.customerDataRequestMapper = customerDataRequestMapper;
        this.customerDataResultMapper = customerDataResultMapper;
    }

    public Customer getCustomerData(CustomerDataQuery query) {
        GetCustomerData getCustomerDataRequest = customerDataRequestMapper.createGetCustomerDataRequest(query);

        v28SoapEnvelope v28SoapEnvelope = soapService.performRequest(getCustomerDataRequest, de.hotel.remoteaccess.v28.v28SoapEnvelope.class, Constants.SCHEMA_GET_CUSTOMER_DATA_REQUEST, false);

        return customerDataResultMapper.mapGetCustomerDataResponse(v28SoapEnvelope.getBody().getCustomerDataResponse());
    }
}
