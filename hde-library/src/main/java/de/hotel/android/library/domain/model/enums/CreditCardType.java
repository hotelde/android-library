package de.hotel.android.library.domain.model.enums;

import android.support.annotation.IntDef;

@IntDef({CreditCardType.VISA, CreditCardType.MASTER_CARD, CreditCardType.AMERICAN_EXPRESS, CreditCardType.JCB, CreditCardType.DINERS_CLUB, CreditCardType.UNKNOWN})
public @interface CreditCardType {
    int VISA = 0;
    int MASTER_CARD = 1;
    int AMERICAN_EXPRESS = 2;
    int JCB = 3;
    int DINERS_CLUB = 4;
    int UNKNOWN = 5;
}
