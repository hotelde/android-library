package de.hotel.android.library.remoteaccess.adapter;

import android.support.annotation.NonNull;

import java.util.Collections;
import java.util.List;

import de.hotel.android.library.domain.model.Reservation;
import de.hotel.android.library.domain.model.enums.CancelResultCode;
import de.hotel.android.library.domain.model.query.CancelReservationQuery;
import de.hotel.android.library.domain.model.query.CheckReservationStatusQuery;
import de.hotel.android.library.remoteaccess.soap.SoapService;
import de.hotel.android.library.remoteaccess.v28.querymapping.HdeV28CancelReservationRequestMapper;
import de.hotel.android.library.remoteaccess.v28.querymapping.HdeV28CheckReservationStatusRequestMapper;
import de.hotel.android.library.remoteaccess.v28.resultmapping.HdeV28CancelReservationResultMapper;
import de.hotel.android.library.remoteaccess.v28.resultmapping.HdeV28CheckReservationStatusResultMapper;
import de.hotel.android.library.util.Constants;
import de.hotel.remoteaccess.v28.model.CancelReservation;
import de.hotel.remoteaccess.v28.model.CancelReservationResponse1;
import de.hotel.remoteaccess.v28.model.CheckReservationStatus;
import de.hotel.remoteaccess.v28.model.CheckReservationStatusResponse1;
import de.hotel.remoteaccess.v28.v28SoapEnvelope;

public class HdeReservationManagementAdapterImpl {

    private SoapService soapService;
    private HdeV28CheckReservationStatusRequestMapper hdeV28CheckReservationStatusRequestMapper;
    private HdeV28CheckReservationStatusResultMapper hdeV28CheckReservationStatusResultMapper;
    private final HdeV28CancelReservationRequestMapper hdeV28CancelReservationRequestMapper;
    private final HdeV28CancelReservationResultMapper hdeV28CancelReservationResultMapper;

    public HdeReservationManagementAdapterImpl(
            @NonNull SoapService soapService,
            @NonNull HdeV28CheckReservationStatusRequestMapper hdeV28CheckReservationStatusRequestMapper,
            @NonNull HdeV28CheckReservationStatusResultMapper hdeV28CheckReservationStatusResultMapper,
            @NonNull HdeV28CancelReservationRequestMapper hdeV28CancelReservationRequestMapper,
            @NonNull HdeV28CancelReservationResultMapper hdeV28CancelReservationResultMapper) {

        this.soapService = soapService;
        this.hdeV28CheckReservationStatusRequestMapper = hdeV28CheckReservationStatusRequestMapper;
        this.hdeV28CheckReservationStatusResultMapper = hdeV28CheckReservationStatusResultMapper;
        this.hdeV28CancelReservationRequestMapper = hdeV28CancelReservationRequestMapper;
        this.hdeV28CancelReservationResultMapper = hdeV28CancelReservationResultMapper;
    }

    public List<Reservation> getReservationList(@NonNull CheckReservationStatusQuery query) {
        CheckReservationStatus request = hdeV28CheckReservationStatusRequestMapper.createReservationStatusRequest(query);

        v28SoapEnvelope soapEnvelope = soapService.performRequest(request, v28SoapEnvelope.class, Constants.SCHEMA_CHECK_RESERVATION_STATUS_REQUEST, false);
        CheckReservationStatusResponse1 checkReservationStatusResult = soapEnvelope.getBody().getReservationStatusResponse().getCheckReservationStatusResult();

        if (checkReservationStatusResult.getError() != null) {
            switch (checkReservationStatusResult.getError().getErrorCode()) {
                case NO_ERROR:
                    break;
                case NO_REVIEWS_FOUND:
                default:
                    return Collections.emptyList();
            }
        }

        return hdeV28CheckReservationStatusResultMapper.mapCheckReservationStatusResponse(checkReservationStatusResult);
    }

    public @CancelResultCode int cancelReservation(@NonNull CancelReservationQuery query) {
        CancelReservation request = hdeV28CancelReservationRequestMapper.createCancelReservationRequest(query);

        v28SoapEnvelope soapEnvelope = soapService.performRequest(request, v28SoapEnvelope.class, Constants.SCHEMA_CANCEL_RESERVATION_REQUEST, false);
        CancelReservationResponse1 cancelReservationResult = soapEnvelope.getBody().getCancelReservationResponse().getCancelReservationResult();

        // TODO Map all necessary errors
        if (cancelReservationResult.getError() != null) {
            switch (cancelReservationResult.getError().getErrorCode()) {
                case NO_ERROR:
                    break;
                case CANCEL_RESERVATION_RATE_NON_CANCELABLE:
                    return CancelResultCode.RATE_NOT_CANCELABLE;
                case CANCEL_RESERVATION_ALREADY_CANCELLED:
                    return CancelResultCode.ALREADY_CANCELLED;
                default:
                    return CancelResultCode.GENERIC_ERROR;
            }
        }

        // TODO Implement
        return hdeV28CancelReservationResultMapper.mapCancelReservationResponse(cancelReservationResult);
    }
}
