package de.hotel.android.library.remoteaccess.http;

import android.support.annotation.NonNull;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class HdeLocationAutoCompleteHttpClientImpl implements HdeLocationAutoCompleteHttpClient {
    private final String endpointUrl;

    @SuppressWarnings("SameParameterValue")
    public HdeLocationAutoCompleteHttpClientImpl(@NonNull String endpointUrl) {
        this.endpointUrl = endpointUrl;
    }

    @Override
    public InputStream performRequest(String urlParameter) throws IOException {
        URL url = new URL(endpointUrl + "?" + urlParameter);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setDoInput(true);

        return urlConnection.getInputStream();
    }


}
