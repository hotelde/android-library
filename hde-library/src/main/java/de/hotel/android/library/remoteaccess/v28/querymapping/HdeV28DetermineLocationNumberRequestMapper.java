package de.hotel.android.library.remoteaccess.v28.querymapping;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.model.query.GetLocationsQuery;
import de.hotel.remoteaccess.v28.model.DetermineLocationNumber;
import de.hotel.remoteaccess.v28.model.DetermineLocationNumberRequest;

public class HdeV28DetermineLocationNumberRequestMapper {

    private final HdeV28BaseRequestMapper baseRequestMapper;

    public HdeV28DetermineLocationNumberRequestMapper(@NonNull HdeV28BaseRequestMapper baseRequestMapper) {
        this.baseRequestMapper = baseRequestMapper;
    }

    public DetermineLocationNumber createDetermineLocationNumberRequest(GetLocationsQuery request) {
        DetermineLocationNumber determineLocationNumber = new DetermineLocationNumber();
        DetermineLocationNumberRequest determineLocationNumberRequest = new DetermineLocationNumberRequest();
        determineLocationNumberRequest.setDestination(request.getDestination());
        determineLocationNumberRequest.setAirportCode(request.getAirportCode());
        determineLocationNumberRequest.setCountryISOa3(request.getCountryIsoA3());
        determineLocationNumber.setObjRequest(determineLocationNumberRequest);

        baseRequestMapper.setStandardProperties(determineLocationNumberRequest, request.getLanguage().getIso2Language());

        return determineLocationNumber;
    }

}
