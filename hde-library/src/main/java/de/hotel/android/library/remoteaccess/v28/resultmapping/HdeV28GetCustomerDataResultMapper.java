package de.hotel.android.library.remoteaccess.v28.resultmapping;

import android.support.annotation.NonNull;

import java.io.IOException;

import de.hotel.android.library.domain.model.data.Customer;
import de.hotel.android.library.util.ObjectDumper;
import de.hotel.remoteaccess.v28.model.ErrorCodeType;
import de.hotel.remoteaccess.v28.model.GetCustomerDataResponse;

public class HdeV28GetCustomerDataResultMapper {
    public Customer mapGetCustomerDataResponse(@NonNull GetCustomerDataResponse response) {
        if (response == null
                || response.getGetCustomerDataResult().getCustomerData() == null) {
            throw new RuntimeException("GetCustomerDataResponse is null");
        }

        if (response.getGetCustomerDataResult().getError().getErrorCode() != ErrorCodeType.NO_ERROR) {
            throw new RuntimeException(ObjectDumper.dump(response.getGetCustomerDataResult().getError().getErrorCode()));
        }

        Customer customer = new Customer();

        de.hotel.remoteaccess.v28.model.Customer customerData = response.getGetCustomerDataResult().getCustomerData();
        customer.setEmail(customerData.getCustomerEmailAddress());
        customer.setFirstname(customerData.getCustomerFirstName());
        customer.setLastname(customerData.getCustomerLastName());
        customer.setPhone(customerData.getCustomerPhoneNumber());
        customer.setCustomerNumber(customerData.getHoteldeCustomerNumber());
        if (customerData.getCompanyNumber() != 0) {
            customer.setCompanyNumber(customerData.getCompanyNumber());
        }
        customer.setCompanyName(customerData.getCustomerCompany());

        return customer;
    }
}
