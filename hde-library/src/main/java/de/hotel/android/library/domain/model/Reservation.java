package de.hotel.android.library.domain.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hotel.android.library.domain.model.data.Address;
import de.hotel.android.library.domain.model.data.HdeGuest;
import de.hotel.android.library.domain.model.enums.CancellationType;
import de.hotel.android.library.domain.model.enums.ReservationStatus;

public class Reservation {
    private String hotelName;
    private List<ReservationRate> rates = new ArrayList<>();
    private Date arrival;
    private Date departure;
    private BigDecimal totalPrice;
    private String currency;
    private Address hotelAddress;
    private String hotelPhoneNumber;
    private float starsRating;

    private @ReservationStatus int reservationStatus;
    private String reservationNr;
    private String reservationId;

    private @CancellationType int cancellationType;
    private String cancellationCode;
    private boolean mayTechnicallyBeCancelled;
    private String cancellationPolicy;

    private List<HdeGuest> guests = new ArrayList<>();

    public Date getArrival() {
        return arrival;
    }

    public void setArrival(Date arrival) {
        this.arrival = arrival;
    }

    public String getReservationNr() {
        return reservationNr;
    }

    public void setReservationNr(String reservationNr) {
        this.reservationNr = reservationNr;
    }

    public String getReservationId() {
        return reservationId;
    }

    public void setReservationId(String reservationId) {
        this.reservationId = reservationId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Date getDeparture() {
        return departure;
    }

    public void setDeparture(Date departure) {
        this.departure = departure;
    }

    public Address getHotelAddress() {
        return hotelAddress;
    }

    public void setHotelAddress(Address hotelAddress) {
        this.hotelAddress = hotelAddress;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getHotelPhoneNumber() {
        return hotelPhoneNumber;
    }

    public void setHotelPhoneNumber(String hotelPhoneNumber) {
        this.hotelPhoneNumber = hotelPhoneNumber;
    }

    public String getCancellationCode() {
        return cancellationCode;
    }

    public void setCancellationCode(String cancellationCode) {
        this.cancellationCode = cancellationCode;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public @ReservationStatus int getReservationStatus() {
        return reservationStatus;
    }

    public void setReservationStatus(@ReservationStatus int reservationStatus) {
        this.reservationStatus = reservationStatus;
    }

    public int getCancellationType() {
        return cancellationType;
    }

    public void setCancellationType(int cancellationType) {
        this.cancellationType = cancellationType;
    }

    public boolean getMayTechnicallyBeCancelled() {
        return mayTechnicallyBeCancelled;
    }

    public void setMayTechnicallyBeCancelled(boolean mayTechnicallyBeCancelled) {
        this.mayTechnicallyBeCancelled = mayTechnicallyBeCancelled;
    }

    public String getCancellationPolicy() {
        return cancellationPolicy;
    }

    public void setCancellationPolicy(String cancellationPolicy) {
        this.cancellationPolicy = cancellationPolicy;
    }

    public float getStarsRating() {
        return starsRating;
    }

    public void setStarsRating(float starsRating) {
        this.starsRating = starsRating;
    }

    public List<HdeGuest> getGuests() {
        return guests;
    }

    public void setGuests(List<HdeGuest> guests) {
        this.guests = guests;
    }

    public List<ReservationRate> getRates() {
        return rates;
    }

    public void setRates(List<ReservationRate> rates) {
        this.rates = rates;
    }
}
