package de.hotel.android.library;

import de.hotel.android.library.domain.adapter.HotelAdapter;
import de.hotel.android.library.domain.service.CustomerManagementFacade;
import de.hotel.android.library.domain.service.HotelFacade;
import de.hotel.android.library.domain.service.LocationAutoCompleteFacade;
import de.hotel.android.library.domain.service.ReservationFacade;
import de.hotel.android.library.domain.service.impl.CustomerManagementFacadeImpl;
import de.hotel.android.library.domain.service.impl.HotelFacadeImpl;
import de.hotel.android.library.domain.service.impl.LocationAutoCompleteFacadeImpl;
import de.hotel.android.library.domain.service.impl.ReservationFacadeImpl;
import de.hotel.android.library.remoteaccess.HdeJiBXMarshallerImpl;
import de.hotel.android.library.remoteaccess.RemoteAccessTargetEnvironmentType;
import de.hotel.android.library.remoteaccess.adapter.HdeCustomerManagementAdapterImpl;
import de.hotel.android.library.remoteaccess.adapter.HdeCustomerRegistrationAdapter;
import de.hotel.android.library.remoteaccess.adapter.HdeCustomerRegistrationAdapterImpl;
import de.hotel.android.library.remoteaccess.adapter.HdeLocationAdapter;
import de.hotel.android.library.remoteaccess.adapter.HdeHotelAdapterImpl;
import de.hotel.android.library.remoteaccess.adapter.HdeLocationAutoCompleteAdapter;
import de.hotel.android.library.remoteaccess.adapter.HdePropertyDescriptionAdapterImpl;
import de.hotel.android.library.remoteaccess.adapter.HdePropertyReviewsAdapterImpl;
import de.hotel.android.library.remoteaccess.adapter.HdeReservationAdapterImpl;
import de.hotel.android.library.remoteaccess.adapter.HdeReservationManagementAdapterImpl;
import de.hotel.android.library.remoteaccess.autocomplete.HdeLocationAutoCompleteMapperImpl;
import de.hotel.android.library.remoteaccess.cola.WebFormRequestBuilderImpl;
import de.hotel.android.library.remoteaccess.http.HdeLocationAutoCompleteHttpClientImpl;
import de.hotel.android.library.remoteaccess.querymapping.CustomerRegistrationRequestMapperImpl;
import de.hotel.android.library.remoteaccess.resultmapping.CustomerRegistrationResponseMapperImpl;
import de.hotel.android.library.remoteaccess.resultmapping.HdeCurrencyConversionResultMapperImpl;
import de.hotel.android.library.remoteaccess.resultmapping.HdeHotelAvailErrorMapperImpl;
import de.hotel.android.library.remoteaccess.resultmapping.HdeHotelAvailResultMapperImpl;
import de.hotel.android.library.remoteaccess.resultmapping.HdeHotelOffersResultMapperImpl;
import de.hotel.android.library.remoteaccess.resultmapping.HdeJsonMapperImpl;
import de.hotel.android.library.remoteaccess.resultmapping.reservation.HdeHotelResErrorMapperImpl;
import de.hotel.android.library.remoteaccess.resultmapping.reservation.HdeHotelResResultMapperImpl;
import de.hotel.android.library.remoteaccess.soap.AuthorizationConnectionBuilder;
import de.hotel.android.library.remoteaccess.soap.ConnectionBuilder;
import de.hotel.android.library.remoteaccess.soap.ConnectionBuilderImpl;
import de.hotel.android.library.remoteaccess.soap.ConnectionBuilderV28;
import de.hotel.android.library.remoteaccess.soap.ConnectionBuilderV30;
import de.hotel.android.library.remoteaccess.soap.HdeSoapClientImpl;
import de.hotel.android.library.remoteaccess.soap.HdeV28SOAPRequestBuilderImpl;
import de.hotel.android.library.remoteaccess.soap.HdeV30SOAPRequestBuilderImpl;
import de.hotel.android.library.remoteaccess.soap.SOAPRequestBuilder;
import de.hotel.android.library.remoteaccess.soap.SoapServiceImpl;
import de.hotel.android.library.remoteaccess.soap.WebFormServiceImpl;
import de.hotel.android.library.remoteaccess.v28.querymapping.HdeV28BaseRequestMapper;
import de.hotel.android.library.remoteaccess.v28.querymapping.HdeV28CancelReservationRequestMapper;
import de.hotel.android.library.remoteaccess.v28.querymapping.HdeV28CheckReservationStatusRequestMapper;
import de.hotel.android.library.remoteaccess.v28.querymapping.HdeV28DetermineLocationNumberRequestMapper;
import de.hotel.android.library.remoteaccess.v28.querymapping.HdeV28GetCustomerDataRequestMapper;
import de.hotel.android.library.remoteaccess.v28.querymapping.HdeV28GetLocationsRequestMapper;
import de.hotel.android.library.remoteaccess.v28.querymapping.HdeV28PropertyDescriptionRequestMapper;
import de.hotel.android.library.remoteaccess.v28.querymapping.HdeV28PropertyReviewsRequestMapper;
import de.hotel.android.library.remoteaccess.v28.resultmapping.HdeV28CancelReservationResultMapper;
import de.hotel.android.library.remoteaccess.v28.resultmapping.HdeV28CheckReservationStatusResultMapper;
import de.hotel.android.library.remoteaccess.v28.resultmapping.HdeV28GetCustomerDataResultMapper;
import de.hotel.android.library.remoteaccess.v28.resultmapping.HdeV28LocationsResultMapper;
import de.hotel.android.library.remoteaccess.v28.resultmapping.HdeV28PropertyDescriptionResultMapper;
import de.hotel.android.library.remoteaccess.v28.resultmapping.HdeV28PropertyReviewsResultMapper;
import de.hotel.android.library.remoteaccess.v30.CreditCardTypeMapper;
import de.hotel.android.library.remoteaccess.v30.HdeV30CreditCardMapperImpl;
import de.hotel.android.library.remoteaccess.v30.querymapping.HdeV30OtaHotelAvailRqMapperImpl;
import de.hotel.android.library.remoteaccess.v30.querymapping.HdeV30OtaHotelResRqDefaultValuesMapperImpl;
import de.hotel.android.library.remoteaccess.v30.querymapping.HdeV30OtaHotelResRqMapper;
import de.hotel.android.library.remoteaccess.v30.querymapping.HdeV30OtaHotelResRqMapperImpl;
import de.hotel.android.library.remoteaccess.v30.querymapping.HdeV30OtaMapperImpl;
import de.hotel.android.library.util.Constants;

/**
 * Main API entry.
 */
public class HotelDe {

    private String consumerUsername;
    private String consumerPassword;
    private String hsbwProductionUsername;
    private String hsbwProductionPassword;

    private static HdeV28BaseRequestMapper hdeV28BaseRequestMapper;
    private static String customEndPoint;

    /**
     * Constructor with dummy credentials.
     * <p/>
     * Mainly useful for local testing and mock service endpoint usage.
     */
    public HotelDe() {
        consumerUsername = "";
        consumerPassword = "";
        hsbwProductionUsername = "";
        hsbwProductionPassword = "";
    }

    /**
     * Constructor with basic authentication credentials.
     * <p/>
     * For ordinary access to HOTEL DE webservice, you need a registered consumer username and
     * password pair. Please contact HOTEL DE support in case you need it.
     *
     * @param consumerUsername       HOTEL DE webservice consumer username
     * @param consumerPassword       HOTEL DE webservice consumer password
     * @param hsbwProductionUsername HOTEL DE HSBW webservice username
     * @param hsbwProductionPassword HOTEL DE HSBW webservice password
     */
    public HotelDe(String consumerUsername, String consumerPassword,
                   String hsbwProductionUsername, String hsbwProductionPassword) {
        this.consumerUsername = consumerUsername;
        this.consumerPassword = consumerPassword;
        this.hsbwProductionUsername = hsbwProductionUsername;
        this.hsbwProductionPassword = hsbwProductionPassword;
    }

    public static void setCustomEndPoint(String customEndPoint) {
        HotelDe.customEndPoint = customEndPoint;
    }

    public HotelFacade createHotelFacade(@RemoteAccessTargetEnvironmentType int environment) {
        return new HotelFacadeImpl(
                createHdeHotelAdapterImpl(environment),
                createPropertyDescriptionAdapter(environment),
                createPropertyReviewsAdapter(environment));
    }

    public ReservationFacade createReservationFacade(@RemoteAccessTargetEnvironmentType int environment) {
        return new ReservationFacadeImpl(
                createReservationAdapter(environment));
    }

    public HotelAdapter createHdeHotelAdapterImpl(@RemoteAccessTargetEnvironmentType int environmentType) {
        return new HdeHotelAdapterImpl(
                createHdeV30SoapService(environmentType, Constants.SCHEMA_HTNG_CHECK_AVAILABILITY, consumerUsername, consumerPassword, hsbwProductionUsername, hsbwProductionPassword),
                new HdeV30OtaHotelAvailRqMapperImpl(createHdeV30OtaMapper(environmentType, consumerUsername)),
                new HdeHotelAvailResultMapperImpl(new HdeHotelOffersResultMapperImpl(new HdeCurrencyConversionResultMapperImpl(), createHdeV30CreditCardMapper())),
                new HdeHotelAvailErrorMapperImpl());
    }

    public HdeReservationAdapterImpl createReservationAdapter(@RemoteAccessTargetEnvironmentType int environmentType) {
        return new HdeReservationAdapterImpl(environmentType,
                createHdeV30SoapService(environmentType, Constants.SCHEMA_HTNG_RESERVATION_REQUEST, consumerUsername, consumerPassword, hsbwProductionUsername, hsbwProductionPassword),
                createHdeV30OtaHotelResRqMapper(environmentType, consumerUsername),
                new HdeHotelResErrorMapperImpl(),
                new HdeHotelResResultMapperImpl(new HdeHotelOffersResultMapperImpl(new HdeCurrencyConversionResultMapperImpl(), createHdeV30CreditCardMapper()), createHdeV30CreditCardMapper()));
    }

    public HdePropertyDescriptionAdapterImpl createPropertyDescriptionAdapter(@RemoteAccessTargetEnvironmentType int environmentType) {
        return new HdePropertyDescriptionAdapterImpl(
                createV28SoapService(environmentType, Constants.HSBW_2_8_HOTEL_SEARCH_SERVICE),
                new HdeV28PropertyDescriptionRequestMapper(Integer.valueOf(consumerUsername), consumerPassword),
                new HdeV28PropertyDescriptionResultMapper());
    }

    public HdeLocationAdapter createGetLocationsAdapter(@RemoteAccessTargetEnvironmentType int environmentType) {
        HdeV28BaseRequestMapper baseRequestMapper = new HdeV28BaseRequestMapper(Integer.valueOf(consumerUsername), consumerPassword);

        return new HdeLocationAdapter(
                createV28SoapService(environmentType, Constants.HSBW_2_8_HOTEL_SEARCH_SERVICE),
                new HdeV28GetLocationsRequestMapper(baseRequestMapper),
                new HdeV28DetermineLocationNumberRequestMapper(baseRequestMapper),
                new HdeV28LocationsResultMapper());
    }

    private SoapServiceImpl createV28SoapService(@RemoteAccessTargetEnvironmentType int environmentType, String serviceName) {
        return new SoapServiceImpl(
                new HdeJiBXMarshallerImpl(),
                createV28SoapRequestBuilder(),
                createHdeV28SoapClient(environmentType, hsbwProductionUsername, hsbwProductionPassword, serviceName));
    }

    public HdePropertyReviewsAdapterImpl createPropertyReviewsAdapter(@RemoteAccessTargetEnvironmentType int environmentType) {
        return new HdePropertyReviewsAdapterImpl(
                createV28SoapService(environmentType, Constants.HSBW_2_8_HOTEL_SEARCH_SERVICE),
                new HdeV28PropertyReviewsRequestMapper(createBaseRequestMapper()),
                new HdeV28PropertyReviewsResultMapper());
    }

    public CustomerManagementFacade createCustomerManagementFacade(@RemoteAccessTargetEnvironmentType int environment) {
        return new CustomerManagementFacadeImpl(createCustomerManagementAdapter(environment), createReservationManagementAdapter(environment), createCustomerRegistrationAdapter(environment));
    }

    public HdeCustomerRegistrationAdapter createCustomerRegistrationAdapter(@RemoteAccessTargetEnvironmentType int environment) {
        return new HdeCustomerRegistrationAdapterImpl(
                new CustomerRegistrationRequestMapperImpl(),
                new CustomerRegistrationResponseMapperImpl(),
                new WebFormServiceImpl(
                        new WebFormRequestBuilderImpl(createColaConnectionBuilder(environment, Constants.COLA_SIMPLE_REGISTRATION)),
                        new HdeJsonMapperImpl()));
    }

    private ConnectionBuilder createColaConnectionBuilder(@RemoteAccessTargetEnvironmentType int environment, String path) {
        switch (environment) {
            case RemoteAccessTargetEnvironmentType.BETA:
                return createConnectionBuilder(Constants.COLA_BETA_ENDPOINT + path);
            case RemoteAccessTargetEnvironmentType.CUSTOM:
                return createConnectionBuilder(customEndPoint + path);
            default:
            case RemoteAccessTargetEnvironmentType.PRODUCTION:
                return createConnectionBuilder(Constants.COLA_PROD_ENDPOINT + path);
        }
    }

    public HdeCustomerManagementAdapterImpl createCustomerManagementAdapter(@RemoteAccessTargetEnvironmentType int environment) {
        return new HdeCustomerManagementAdapterImpl(
                createV28SoapService(environment, Constants.HSBW_2_8_CUSTOMER_MANAGEMENT_SERVICE),
                new HdeV28GetCustomerDataRequestMapper(createBaseRequestMapper()),
                new HdeV28GetCustomerDataResultMapper());
    }

    public HdeReservationManagementAdapterImpl createReservationManagementAdapter(@RemoteAccessTargetEnvironmentType int environment) {
        return new HdeReservationManagementAdapterImpl(
                createV28SoapService(environment, Constants.HSBW_2_8_BOOK_SERVICE),
                new HdeV28CheckReservationStatusRequestMapper(createBaseRequestMapper()),
                new HdeV28CheckReservationStatusResultMapper(),
                new HdeV28CancelReservationRequestMapper(createBaseRequestMapper()),
                new HdeV28CancelReservationResultMapper());
    }

    private HdeV28BaseRequestMapper createBaseRequestMapper() {
        if (hdeV28BaseRequestMapper == null) {
            hdeV28BaseRequestMapper = new HdeV28BaseRequestMapper(Integer.valueOf(consumerUsername), consumerPassword);
        }

        return hdeV28BaseRequestMapper;
    }

    public LocationAutoCompleteFacade createLocationAutoCompleteFacade() {
        return new LocationAutoCompleteFacadeImpl(createLocationAutoCompleteAdapter());
    }

    private static SoapServiceImpl createHdeV30SoapService(@RemoteAccessTargetEnvironmentType int environmentType, String action, String consumerUsername, String consumerPassword, String hsbwProductionUsername, String hsbwProductionPassword) {
        return new SoapServiceImpl(
                new HdeJiBXMarshallerImpl(),
                createV30SoapRequestBuilder(consumerUsername, consumerPassword),
                createHdeV30SoapClient(environmentType,
                        hsbwProductionUsername,
                        hsbwProductionPassword));
    }

    private static SOAPRequestBuilder createV30SoapRequestBuilder(String consumerUsername, String consumerPassword) {
        return new HdeV30SOAPRequestBuilderImpl(consumerUsername, consumerPassword);
    }

    private static SOAPRequestBuilder createV28SoapRequestBuilder() {
        return new HdeV28SOAPRequestBuilderImpl();
    }

    private static HdeV30OtaMapperImpl createHdeV30OtaMapper(@RemoteAccessTargetEnvironmentType int environmentType, String consumerUsername) {
        return new HdeV30OtaMapperImpl(consumerUsername, environmentType);
    }

    private static HdeSoapClientImpl createHdeV28SoapClient(@RemoteAccessTargetEnvironmentType int environmentType, String hsbwProductionUsername, String hsbwProductionPassword, String serviceName) {

        ConnectionBuilder baseConnectionBuilder;
        switch (environmentType) {
            case RemoteAccessTargetEnvironmentType.BETA:
                baseConnectionBuilder = createConnectionBuilder(Constants.HSBW_BETA_ENDPOINT + serviceName);
                break;
            case RemoteAccessTargetEnvironmentType.CUSTOM:
                baseConnectionBuilder = createConnectionBuilder(customEndPoint + serviceName);
                break;
            default:
            case RemoteAccessTargetEnvironmentType.PRODUCTION:
                baseConnectionBuilder = createAuthorizationConnectionBuilder(
                        Constants.HSBW_PROD_ENDPOINT + serviceName,
                        hsbwProductionUsername,
                        hsbwProductionPassword);
                break;
        }

        return new HdeSoapClientImpl(new ConnectionBuilderV28(baseConnectionBuilder));
    }

    private static HdeSoapClientImpl createHdeV30SoapClient(@RemoteAccessTargetEnvironmentType int environmentType, String hsbwProductionUsername, String hsbwProductionPassword) {

        ConnectionBuilder baseConnectionBuilder;
        switch (environmentType) {
            case RemoteAccessTargetEnvironmentType.BETA:
                baseConnectionBuilder = createConnectionBuilder(Constants.HSBW_BETA_ENDPOINT + Constants.HSBW_3_0_SEAMLESS_SHOP_AND_BOOK_SERVICE);
                break;
            case RemoteAccessTargetEnvironmentType.CUSTOM:
                baseConnectionBuilder = createConnectionBuilder(customEndPoint + Constants.HSBW_3_0_SEAMLESS_SHOP_AND_BOOK_SERVICE);
                break;
            default:
            case RemoteAccessTargetEnvironmentType.PRODUCTION:
                baseConnectionBuilder = createAuthorizationConnectionBuilder(
                        Constants.HSBW_PROD_ENDPOINT + Constants.HSBW_3_0_SEAMLESS_SHOP_AND_BOOK_SERVICE,
                        hsbwProductionUsername,
                        hsbwProductionPassword);
                break;
        }

        return new HdeSoapClientImpl(new ConnectionBuilderV30(baseConnectionBuilder));
    }

    private static ConnectionBuilder createConnectionBuilder(String endPoint) {
        return new ConnectionBuilderImpl(endPoint);
    }

    private static ConnectionBuilder createAuthorizationConnectionBuilder(String endPoint, String hsbwProductionUsername, String hsbwProductionPassword) {
        return new AuthorizationConnectionBuilder(
                hsbwProductionUsername,
                hsbwProductionPassword,
                new ConnectionBuilderImpl(endPoint));
    }

    private static HdeV30CreditCardMapperImpl createHdeV30CreditCardMapper() {
        return new HdeV30CreditCardMapperImpl(new CreditCardTypeMapper());
    }

    private static HdeLocationAutoCompleteAdapter createLocationAutoCompleteAdapter() {
        return createLocationAutoCompleteAdapter(RemoteAccessTargetEnvironmentType.PRODUCTION);
    }

    private static HdeLocationAutoCompleteAdapter createLocationAutoCompleteAdapter(@RemoteAccessTargetEnvironmentType int environmentType) {
        switch (environmentType) {
            case RemoteAccessTargetEnvironmentType.BETA:
                // There is no beta LAC available, use production...
            case RemoteAccessTargetEnvironmentType.PRODUCTION:
                return new HdeLocationAutoCompleteAdapter(
                        new HdeLocationAutoCompleteHttpClientImpl(Constants.LAC_PROD_ENDPOINT),
                        new HdeLocationAutoCompleteMapperImpl());
            default:
                return null; // Will never happen, but makes the compiler happy
        }
    }

    private static HdeV30OtaHotelResRqMapper createHdeV30OtaHotelResRqMapper(@RemoteAccessTargetEnvironmentType int environmentType, String consumerUsername) {
        return new HdeV30OtaHotelResRqDefaultValuesMapperImpl(
                new HdeV30OtaHotelResRqMapperImpl(
                        createHdeV30OtaMapper(environmentType, consumerUsername),
                        createHdeV30CreditCardMapper()));
    }
}
