package de.hotel.android.library.domain.service.impl;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.adapter.ReservationAdapter;
import de.hotel.android.library.domain.model.query.HotelReservationQuery;
import de.hotel.android.library.domain.model.HotelReservationResponse;
import de.hotel.android.library.domain.service.ReservationFacade;

/**
 * {@inheritDoc}
 */
public class ReservationFacadeImpl implements ReservationFacade {
    private static final String TAG = ReservationFacadeImpl.class.getSimpleName();

    private final ReservationAdapter reservationAdapter;

    public ReservationFacadeImpl(ReservationAdapter reservationAdapter) {
        this.reservationAdapter = reservationAdapter;
    }

    @Override
    public HotelReservationResponse bookHotel(@NonNull HotelReservationQuery hotelReservationRequest) {
        return reservationAdapter.bookHotel(hotelReservationRequest);
    }
}
