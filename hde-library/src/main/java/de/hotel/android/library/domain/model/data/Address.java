package de.hotel.android.library.domain.model.data;

import android.os.Parcel;
import android.os.Parcelable;

public class Address implements Parcelable {
    public static final Parcelable.Creator<Address> CREATOR = new Parcelable.Creator<Address>() {
        @Override
        public Address createFromParcel(Parcel parcel) {
            return new Address(parcel);
        }

        @Override
        public Address[] newArray(int i) {
            return new Address[i];
        }
    };
    private String street;
    private String city;
    private String postalCode;
    private String countryIsoA3;
    private GeoPosition geoPosition;

    public Address() {

    }

    public Address(Parcel parcel) {
        street = parcel.readString();
        city = parcel.readString();
        postalCode = parcel.readString();
        countryIsoA3 = parcel.readString();

        geoPosition = parcel.readParcelable(GeoPosition.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(street);
        parcel.writeString(city);
        parcel.writeString(postalCode);
        parcel.writeString(countryIsoA3);

        parcel.writeParcelable(geoPosition, i);
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountryIsoA3() {
        return countryIsoA3;
    }

    public void setCountryIsoA3(String countryIsoA3) {
        this.countryIsoA3 = countryIsoA3;
    }

    public GeoPosition getGeoPosition() {
        return geoPosition;
    }

    public void setGeoPosition(GeoPosition geoPosition) {
        this.geoPosition = geoPosition;
    }


}
