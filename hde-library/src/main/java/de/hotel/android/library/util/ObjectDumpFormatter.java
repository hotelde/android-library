package de.hotel.android.library.util;

/**
 * Interface for Object dumping.
 */
public interface ObjectDumpFormatter {
    String objectDumpFormat(Object object);
}
