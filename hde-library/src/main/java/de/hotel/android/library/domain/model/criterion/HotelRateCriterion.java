package de.hotel.android.library.domain.model.criterion;

public class HotelRateCriterion {

    private String hotelId;

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

}


