package de.hotel.android.library.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

public class HdeCancelPenalty implements Parcelable {
    private String language;
    private String text;

    public static final Creator<HdeCancelPenalty> CREATOR = new Creator<HdeCancelPenalty>() {
        @Override
        public HdeCancelPenalty createFromParcel(Parcel in) {
            return new HdeCancelPenalty(in);
        }

        @Override
        public HdeCancelPenalty[] newArray(int size) {
            return new HdeCancelPenalty[size];
        }
    };

    public HdeCancelPenalty(){

    }

    protected HdeCancelPenalty(Parcel in) {
        language = in.readString();
        text = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(language);
        dest.writeString(text);
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}