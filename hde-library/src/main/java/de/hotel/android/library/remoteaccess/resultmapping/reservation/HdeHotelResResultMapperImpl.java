package de.hotel.android.library.remoteaccess.resultmapping.reservation;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.model.data.CreditCard;
import de.hotel.android.library.domain.model.data.Customer;
import de.hotel.android.library.domain.model.Hotel;
import de.hotel.android.library.domain.model.query.HotelReservationQuery;
import de.hotel.android.library.domain.model.HotelReservationResponse;
import de.hotel.android.library.remoteaccess.resultmapping.HdeHotelOffersResultMapper;
import de.hotel.android.library.remoteaccess.v30.HdeV30CreditCardMapper;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.CustomerType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.HotelResResponseType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.HotelReservationIDsType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.HotelReservationsType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.PaymentCardType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.RequiredPaymentsType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.ResGlobalInfoType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.ResGuestType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.RoomStayType;

import java.util.ArrayList;
import java.util.List;

public class HdeHotelResResultMapperImpl implements HdeHotelResResultMapper {

    private static final String RESERVATION_ID_TYPE_RESERVATION_ID = "14";
    private static final String RESERVATION_ID_TYPE_RESERVATION_NUMBER = "10";
    private static final String RESERVATION_ID_TYPE_CANCELLATION_CODE = "15";
    private final HdeHotelOffersResultMapper hdeHotelOffersResultMapper;
    private final HdeV30CreditCardMapper hdeV30CreditCardMapper;

    public HdeHotelResResultMapperImpl(@NonNull HdeHotelOffersResultMapper hdeHotelOffersResultMapper, @NonNull HdeV30CreditCardMapper hdeV30CreditCardMapper) {
        this.hdeHotelOffersResultMapper = hdeHotelOffersResultMapper;
        this.hdeV30CreditCardMapper = hdeV30CreditCardMapper;
    }

    public HotelReservationResponse getHotelReservationResponse(@NonNull HotelReservationQuery hotelReservationQuery, @NonNull HotelResResponseType hotelReservationResponse) {
        HotelReservationResponse response = new HotelReservationResponse();
        HotelReservationsType.HotelReservation hotelReservation = hotelReservationResponse.getHotelReservations().getHotelReservationList().get(0);// Only one booking per request is currently supported

        response.setFromDateMs(hotelReservationQuery.getFrom());
        response.setToDateMs(hotelReservationQuery.getTo());

        response.setUniqueId(createUniqueId(hotelReservation));

        setReservationIds(response, hotelReservation.getResGlobalInfo());
        response.setBookingCustomer(createBookingCustomer(hotelReservation.getResGlobalInfo()));

        List<Hotel> hotels = hdeHotelOffersResultMapper.createHotels(hotelReservation.getRoomStays().getRoomStayList(), null, "EUR");
        response.setHotels(hotels);

        List<Customer> travelers = createTravelers(hotelReservation.getResGuests().getResGuestList());
        response.setTravelers(travelers);

        CreditCard creditCard = createCreditCardFromGuarantee(hotelReservation.getRoomStays().getRoomStayList().get(0));
        response.setCreditCard(creditCard);

        return response;
    }

    private String createUniqueId(HotelReservationsType.HotelReservation hotelReservation) {
        return hotelReservation.getUniqueIDList().get(0).getUniqueIDGroup().getID();
    }

    @SuppressWarnings({"magicnumber"})
    private CreditCard createCreditCardFromGuarantee(RoomStayType roomStayType) {
        if (roomStayType.getDepositPayments() == null
                || roomStayType.getDepositPayments().getGuaranteePaymentList() == null
                || roomStayType.getDepositPayments().getGuaranteePaymentList().isEmpty()) {
            return null;
        }

        CreditCard creditCard = new CreditCard();

        RequiredPaymentsType.GuaranteePayment guaranteePayment = roomStayType.getDepositPayments().getGuaranteePaymentList().get(0);
        PaymentCardType paymentCard = guaranteePayment.getAcceptedPayments().getAcceptedPaymentList().get(0).getSequence().getPaymentCard();

        // TODO Currently the format the HSBW returns is not like it is defined in the XSD --> cardNumber is null
//        creditCard.setNumber(paymentCard.getCardNumber().getPlainText());

        creditCard.setName(paymentCard.getCardHolderName());
        creditCard.setMonth(Integer.valueOf(paymentCard.getPaymentCardDateGroup().getExpireDate().substring(0, 2)));
        creditCard.setYear(Integer.valueOf(paymentCard.getPaymentCardDateGroup().getExpireDate().substring(2, 4)) + 2000);
        creditCard.setCreditCardType(hdeV30CreditCardMapper.mapFromCreditCardCode(paymentCard.getCardCode()));

        return creditCard;
    }

    private void setReservationIds(HotelReservationResponse response, ResGlobalInfoType resGlobalInfo) {
        List<HotelReservationIDsType.HotelReservationID> hotelReservationIDList = resGlobalInfo.getHotelReservationIDs().getHotelReservationIDList();
        for (HotelReservationIDsType.HotelReservationID reservationID : hotelReservationIDList) {
            if (reservationID.getResIDType().equals(RESERVATION_ID_TYPE_RESERVATION_ID)) {
                response.setReservationId(reservationID.getResIDValue());
                continue;
            }

            if (reservationID.getResIDType().equals(RESERVATION_ID_TYPE_RESERVATION_NUMBER)) {
                response.setReservationNumber(reservationID.getResIDValue());
                continue;
            }

            if (reservationID.getResIDType().equals(RESERVATION_ID_TYPE_CANCELLATION_CODE)) {
                response.setCancellationCode(reservationID.getResIDValue());
                continue;
            }
        }
    }

    private Customer createBookingCustomer(ResGlobalInfoType resGlobalInfoType) {
        Customer customer = new Customer();
        CustomerType customerType = resGlobalInfoType.getProfiles().getProfileInfoList().get(0).getProfile().getCustomer();
        customer.setFirstname(customerType.getPersonNameList().get(0).getGivenNameList().get(0));
        customer.setLastname(customerType.getPersonNameList().get(0).getSurname());
        customer.setEmail(customerType.getEmailList().get(0).getString());
        return customer;
    }

    private List<Customer> createTravelers(List<ResGuestType> resGuests) {
        ArrayList<Customer> travelers = new ArrayList<>();
        for (ResGuestType resGuestType : resGuests) {
            Customer customer = new Customer();
            customer.setFirstname(resGuestType.getProfiles().getProfileInfoList().get(0).getProfile().getCustomer().getPersonNameList().get(0).getGivenNameList().get(0));
            customer.setLastname(resGuestType.getProfiles().getProfileInfoList().get(0).getProfile().getCustomer().getPersonNameList().get(0).getSurname());
            travelers.add(customer);
        }

        return travelers;
    }
}
