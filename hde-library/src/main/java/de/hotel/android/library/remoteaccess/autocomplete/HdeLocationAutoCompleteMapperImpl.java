package de.hotel.android.library.remoteaccess.autocomplete;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import de.hotel.android.library.domain.model.data.Location;
import de.hotel.android.library.domain.model.enums.LocationType;
import de.hotel.android.library.domain.model.query.LocationAutoCompleteQuery;

public class HdeLocationAutoCompleteMapperImpl implements HdeLocationAutoCompleteMapper {
    private static final String RESPONSE_DATA_DELIMITER = "<!";
    private static final String PHRASE_AND_RESULTS_DELIMITER = "§";
    private static final String RESULTS_DELIMITER = "~";
    private static final String DESTINATION_AND_SEARCH_CODE_DELIMITER = "\\$";
    private static final String LOCATION_AND_CHAIN_DELIMITER = "LC";
    private static final String REGION_AND_CHAIN_DELIMITER = "RC";
    private static final String LOCATION_DELIMITER = "L";
    private static final String HOTEL_DELIMITER = "H";
    private static final String UTF_8 = "UTF-8";

    @Override
    public String mapLocationAutoCompleteRequest(LocationAutoCompleteQuery request) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("m=1"); // Hardcoded because no other option is
        // currently needed
        stringBuilder.append("&pf=");
        try {
            stringBuilder.append(URLEncoder.encode(request.getSearchPhrase(), UTF_8));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        stringBuilder.append("&lng=");
        stringBuilder.append(request.getIsoA2LanguageCode());

        return stringBuilder.toString();
    }

    @Override
    public List<Location> mapLocationAutoCompleteResponse(InputStream inputStream) throws IOException {
        StringBuilder stringBuilder = readStreamToStringBuilder(inputStream);
        return parseResponse(stringBuilder.toString());
    }

    private StringBuilder readStreamToStringBuilder(InputStream inputStream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
        }
        return stringBuilder;
    }

    private List<Location> parseResponse(String response) {
        ArrayList<Location> locations = new ArrayList<>();
        if (response.trim().isEmpty()) {
            return locations;
        }

        String[] responseData = response.split(RESPONSE_DATA_DELIMITER);
        String[] searchPhraseAndResults = responseData[0].split(PHRASE_AND_RESULTS_DELIMITER);
        String[] results = searchPhraseAndResults[1].split(RESULTS_DELIMITER);

        for (String result : results) {
            Location location = new Location();
            String[] destinationAndSearchCode = result.split(DESTINATION_AND_SEARCH_CODE_DELIMITER);
            String[] descriptionRegionAndCountry = splitDestination(destinationAndSearchCode[0]);

            parseDestination(location, descriptionRegionAndCountry);
            parseSearchCode(location, destinationAndSearchCode[1]);

            locations.add(location);
        }

        return locations;
    }

    private String[] splitDestination(String content) {
        List<String> splittedContent = new ArrayList<>();

        StringBuilder sb = new StringBuilder();

        boolean splitAtNextComma = true;
        for (int i = 0; i < content.length(); i++) {
            if (content.charAt(i) == '(') {
                splitAtNextComma = false;
            } else if (content.charAt(i) == ')') {
                splitAtNextComma = true;
            }

            if (content.charAt(i) == ',' && splitAtNextComma) {
                splittedContent.add(sb.toString());
                sb.setLength(0); // Re-use the StringBuilder to save instantiations
                i++; // Skip the next whitespace
            } else {
                sb.append(content.charAt(i));
            }
        }

        splittedContent.add(sb.toString());
        return splittedContent.toArray(new String[0]);
    }

    @SuppressWarnings({"magicnumber"}) // These numbers are defined by the auto complete service
    private void parseDestination(Location location, String[] destination) {
        switch (destination.length) {
            case 4:
                location.setLocationName(destination[0]);
                location.setRegionName(destination[1] + ", " + destination[2]);
                location.setIsoA3Country(destination[3]);
                break;
            case 3:
                location.setLocationName(destination[0]);
                location.setRegionName(destination[1]);
                location.setIsoA3Country(destination[2]);
                break;
            case 2:
            default:
                location.setLocationName(destination[0]);
                location.setIsoA3Country(destination[1]);
                break;
        }
    }

    private void parseSearchCode(Location location, String searchCode) {
        String locationId = null;
        if (searchCode.startsWith(LOCATION_AND_CHAIN_DELIMITER) || searchCode.startsWith(REGION_AND_CHAIN_DELIMITER)) {
            String[] locationAndChainCode = searchCode.split("\\|");
            locationId = locationAndChainCode[0].substring(2, locationAndChainCode[0].length());
            location.setLocationType(LocationType.CITY);
        } else if (searchCode.startsWith(LOCATION_DELIMITER)) {
            locationId = searchCode.substring(1, searchCode.length());
            location.setLocationType(LocationType.CITY);
        } else if (searchCode.startsWith(HOTEL_DELIMITER)) {
            locationId = searchCode.substring(1, searchCode.length());
            location.setLocationType(LocationType.HOTEL);
        }

        location.setLocationId(Integer.valueOf(locationId));
    }
}
