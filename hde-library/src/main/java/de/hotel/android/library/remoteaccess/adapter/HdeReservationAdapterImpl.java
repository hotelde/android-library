package de.hotel.android.library.remoteaccess.adapter;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.adapter.ReservationAdapter;
import de.hotel.android.library.domain.model.query.HotelReservationQuery;
import de.hotel.android.library.domain.model.HotelReservationResponse;
import de.hotel.android.library.remoteaccess.RemoteAccessTargetEnvironmentType;
import de.hotel.android.library.remoteaccess.resultmapping.reservation.HdeHotelResErrorMapper;
import de.hotel.android.library.remoteaccess.resultmapping.reservation.HdeHotelResResultMapper;
import de.hotel.android.library.remoteaccess.soap.SoapService;
import com.hrsgroup.remoteaccess.hde.v30.v30SoapEnvelope;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.HotelResResponseType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.OTAHotelResRQ;
import de.hotel.android.library.remoteaccess.v30.querymapping.HdeV30OtaHotelResRqMapper;
import de.hotel.android.library.util.Constants;

/**
 * {@inheritDoc}
 */
public class HdeReservationAdapterImpl implements ReservationAdapter {
    private final int remoteAccessTargetEnvironmentType;
    private final SoapService hdeV30SoapService;
    private final HdeV30OtaHotelResRqMapper hdeV30OtaHotelResRqMapper;
    private final HdeHotelResErrorMapper hdeHotelResErrorMapper;
    private final HdeHotelResResultMapper hdeHotelResResultMapper;

    public HdeReservationAdapterImpl(
            @RemoteAccessTargetEnvironmentType int remoteAccessTargetEnvironmentType,
            @NonNull SoapService hdeV30SoapService,
            @NonNull HdeV30OtaHotelResRqMapper hdeV30OtaHotelResRqMapper,
            @NonNull HdeHotelResErrorMapper hdeHotelResErrorMapper,
            @NonNull HdeHotelResResultMapper hdeHotelResResultMapper) {
        this.remoteAccessTargetEnvironmentType = remoteAccessTargetEnvironmentType;
        this.hdeV30SoapService = hdeV30SoapService;
        this.hdeV30OtaHotelResRqMapper = hdeV30OtaHotelResRqMapper;
        this.hdeHotelResErrorMapper = hdeHotelResErrorMapper;
        this.hdeHotelResResultMapper = hdeHotelResResultMapper;
    }

    @Override
    public HotelReservationResponse bookHotel(@NonNull HotelReservationQuery hotelReservationQuery) {
        OTAHotelResRQ otaHotelResRequest = hdeV30OtaHotelResRqMapper.createOTAHotelResRequest(hotelReservationQuery, remoteAccessTargetEnvironmentType);

        v30SoapEnvelope otaAvailRSEnvelope = hdeV30SoapService.performRequest(otaHotelResRequest, v30SoapEnvelope.class, Constants.SCHEMA_HTNG_RESERVATION_REQUEST, false);
        HotelResResponseType otaHotelResRS = otaAvailRSEnvelope.getBody().getOtaHotelResRS();

        if (otaHotelResRS.getErrors() != null && !otaHotelResRS.getErrors().getErrorList().isEmpty()) {
            return hdeHotelResErrorMapper.handleHotelResErrors(hotelReservationQuery, otaHotelResRS.getErrors().getErrorList());
        }

        return hdeHotelResResultMapper.getHotelReservationResponse(hotelReservationQuery, otaHotelResRS);
    }
}
