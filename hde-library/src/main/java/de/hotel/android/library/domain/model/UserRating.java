package de.hotel.android.library.domain.model;

public class UserRating {
    public static final float MINIMUM_FOR_GOOD = 7.0f;
    public static final float MINIMUM_FOR_VERY_GOOD = 8.0f;
    public static final float MINIMUM_FOR_EXCELLENT = 9.0f;
}
