package de.hotel.android.library.util;

import java.util.Collection;

/**
 * Utility class for {@link java.util.List}.
 *
 * @author Johannes Mueller
 */
public final class Lists {

    private Lists() {
        // Don't instantiate me.
    }

    public static int size(Collection<?> collection) {
        return collection != null ? collection.size() : 0;
    }

    public static boolean isNullOrEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }
}
