package de.hotel.android.library.domain.model;

import java.math.BigDecimal;

import de.hotel.android.library.domain.model.review.ReviewCategoryPerson;

public class HotelCustomerReview {

    // TODO Map when necessary
//    private TravelReason valuationDetailCategoryReason;
//    private TravelAge valuationDetailCategoryAge;
    private @ReviewCategoryPerson int reviewCategoryPerson;
    private String comment;
    private String commentPositive;
    private String commentNegative;
    private BigDecimal recommendValue;
    private String travelDate;

    public int getReviewCategoryPerson() {
        return reviewCategoryPerson;
    }

    public void setReviewCategoryPerson(int reviewCategoryPerson) {
        this.reviewCategoryPerson = reviewCategoryPerson;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCommentPositive() {
        return commentPositive;
    }

    public void setCommentPositive(String commentPositive) {
        this.commentPositive = commentPositive;
    }

    public String getCommentNegative() {
        return commentNegative;
    }

    public void setCommentNegative(String commentNegative) {
        this.commentNegative = commentNegative;
    }

    public BigDecimal getRecommendValue() {
        return recommendValue;
    }

    public void setRecommendValue(BigDecimal recommendValue) {
        this.recommendValue = recommendValue;
    }

    public String getTravelDate() {
        return travelDate;
    }

    public void setTravelDate(String travelDate) {
        this.travelDate = travelDate;
    }
}
