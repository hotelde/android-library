package de.hotel.android.library.remoteaccess.resultmapping;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.hrsgroup.remoteaccess.hde.v30.model.ota.OTAHotelAvailRS;
import de.hotel.android.library.domain.model.query.HotelAvailQuery;
import de.hotel.android.library.domain.model.HotelAvailResult;
import de.hotel.android.library.domain.model.Hotel;

import java.util.ArrayList;
import java.util.List;

public class HdeHotelAvailResultMapperImpl implements HdeHotelAvailResultMapper {

    private final HdeHotelOffersResultMapperImpl hdeHotelOffersResultMapper;

    public HdeHotelAvailResultMapperImpl(@NonNull HdeHotelOffersResultMapperImpl hdeHotelOffersResultMapper) {
        this.hdeHotelOffersResultMapper = hdeHotelOffersResultMapper;
    }

    @Override
    public HotelAvailResult hotelAvailResult(HotelAvailQuery hotelAvailQuery, OTAHotelAvailRS availResponse) {
        HotelAvailResult hotelAvailResult = new HotelAvailResult();

        // Take dates of stay from the query.
        hotelAvailResult.setFromDate(hotelAvailQuery.getHotelAvailCriterion().getFrom());
        hotelAvailResult.setToDate(hotelAvailQuery.getHotelAvailCriterion().getTo());

        OTAHotelAvailRS.RoomStays roomStays = availResponse.getRoomStays();
        String moreIndicator = roomStays.getMoreIndicator();
        hotelAvailResult.setTotalAvailableHotels((!TextUtils.isEmpty(moreIndicator) ? Integer.valueOf(moreIndicator) : roomStays.getRoomStayList().size()));

        hotelAvailResult.setHotelList(new ArrayList<Hotel>());
        List<Hotel> hotels = hdeHotelOffersResultMapper.createHotels(availResponse.getRoomStays().getRoomStayList(), availResponse.getCurrencyConversions(), hotelAvailQuery.getLocale().getIso3Currency());
        hotelAvailResult.getHotelList().addAll(hotels);

        hotelAvailResult.setTimestamp(availResponse.getOTAPayloadStdAttributes().getTimeStamp());

        return hotelAvailResult;
    }
}
