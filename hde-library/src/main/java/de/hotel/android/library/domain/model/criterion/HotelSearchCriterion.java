package de.hotel.android.library.domain.model.criterion;

import android.support.annotation.Nullable;

/**
 * Criterion for hotel searches (regardless of availability).
 *
 * @author Johannes Mueller
 */
public class HotelSearchCriterion {

    private LocationCriterion locationCriterion;
    private FilterCriterion filterCriterion = new FilterCriterion();

    public @Nullable LocationCriterion getLocationCriterion() {
        return locationCriterion;
    }

    public void setLocationCriterion(@Nullable LocationCriterion locationCriterion) {
        this.locationCriterion = locationCriterion;
    }

    public @Nullable FilterCriterion getFilterCriterion() {
        return filterCriterion;
    }

    public void setFilterCriterion(FilterCriterion filterCriterion) {
        this.filterCriterion = filterCriterion;
    }
}
