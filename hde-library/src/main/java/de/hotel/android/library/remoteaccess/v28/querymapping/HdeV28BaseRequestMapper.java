package de.hotel.android.library.remoteaccess.v28.querymapping;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.UUID;

import de.hotel.remoteaccess.v28.model.BaseRequest;
import de.hotel.remoteaccess.v28.model.WebServiceConsumerInformation;

public class HdeV28BaseRequestMapper {

    private final int id;
    private final String password;

    public HdeV28BaseRequestMapper(int id, @NonNull String password) {
        this.id = id;
        this.password = password;
    }
    public void setStandardProperties(@NonNull BaseRequest propertyReviewsRequest, @NonNull String isoA2Language) {
        WebServiceConsumerInformation webServiceConsumerInformation = new WebServiceConsumerInformation();
        webServiceConsumerInformation.setID(id);
        webServiceConsumerInformation.setPassword(password);
        propertyReviewsRequest.setWebServiceConsumerInformation(webServiceConsumerInformation);

        propertyReviewsRequest.setSecureContext(true);
        propertyReviewsRequest.setToken(UUID.randomUUID().toString());
        propertyReviewsRequest.setLanguage(isoA2Language);
    }
}
