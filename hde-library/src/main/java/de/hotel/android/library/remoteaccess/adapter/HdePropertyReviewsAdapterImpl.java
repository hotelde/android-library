package de.hotel.android.library.remoteaccess.adapter;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.model.HotelPropertyReviews;
import de.hotel.android.library.domain.model.query.PropertyReviewsQuery;
import de.hotel.android.library.remoteaccess.soap.SoapService;
import de.hotel.android.library.remoteaccess.v28.querymapping.HdeV28PropertyReviewsRequestMapper;
import de.hotel.android.library.remoteaccess.v28.resultmapping.HdeV28PropertyReviewsResultMapper;

import de.hotel.android.library.util.Constants;
import de.hotel.remoteaccess.v28.model.GetPropertyReviews;
import de.hotel.remoteaccess.v28.model.PropertyReviewsResponse;
import de.hotel.remoteaccess.v28.v28SoapEnvelope;

public class HdePropertyReviewsAdapterImpl implements PropertyReviewsAdapter {

    private final SoapService soapService;
    private final HdeV28PropertyReviewsRequestMapper hdeV28PropertyReviewsRequestMapper;
    private final HdeV28PropertyReviewsResultMapper hdeV28PropertyReviewsResultMapper;

    public HdePropertyReviewsAdapterImpl(
            @NonNull SoapService soapService,
            @NonNull HdeV28PropertyReviewsRequestMapper hdeV28PropertyReviewsRequestMapper,
            @NonNull HdeV28PropertyReviewsResultMapper hdeV28PropertyReviewsResultMapper) {

        this.soapService = soapService;
        this.hdeV28PropertyReviewsRequestMapper = hdeV28PropertyReviewsRequestMapper;
        this.hdeV28PropertyReviewsResultMapper = hdeV28PropertyReviewsResultMapper;
    }

    @Override
    public HotelPropertyReviews fetchPropertyReviews(@NonNull PropertyReviewsQuery query) {
        GetPropertyReviews propertyReviewsRequest = hdeV28PropertyReviewsRequestMapper.createPropertyReviewRequest(query);

        v28SoapEnvelope soapEnvelope = soapService.performRequest(propertyReviewsRequest, v28SoapEnvelope.class, Constants.SCHEMA_GET_PROPERTY_REVIEWS_REQUEST, true);
        PropertyReviewsResponse propertyReviewsResponse = soapEnvelope.getBody().getPropertyReviewsResponse().getGetPropertyReviewsResult();

        if (propertyReviewsResponse.getError() != null) {
            switch (propertyReviewsResponse.getError().getErrorCode()) {
                case NO_ERROR:
                    break;
                case NO_REVIEWS_FOUND:
                default:
                    return new HotelPropertyReviews();
            }
        }

        return hdeV28PropertyReviewsResultMapper.mapPropertyReviewsResponse(propertyReviewsResponse);
    }
}
