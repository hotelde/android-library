package de.hotel.android.library.util;

import android.content.Context;
import android.text.format.DateUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.MILLISECOND;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.SECOND;

/**
 * Utility class for {@link java.util.Date}.
 */
public final class Dates {

    public static final long ONE_DAY_IN_MILLISECONDS = 86400000L;
    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    public static final SimpleDateFormat UTC_COMBINED_DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);

    private Dates() {
        // Don't instantiate me.
    }

    public static String yyyyMmDdFor(long timeMillis) {
        final SimpleDateFormat yyyyMmDd = new SimpleDateFormat(YYYY_MM_DD, Locale.US);
        return yyyyMmDd.format(new Date(timeMillis));
    }

    public static int days(long fromMillis, long toMillis) {
        return (int) ((toMillis - fromMillis) / ONE_DAY_IN_MILLISECONDS);
    }

    public static Long mapDateToMilliseconds(String date) throws ParseException {
        return (new SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(date)).getTime();
    }

    public static long getTodayAtZeroHour() {
        Calendar today = Calendar.getInstance();
        setMidnight(today);
        return today.getTimeInMillis();
    }

    /**
     * Clears out the hours/minutes/seconds/millis of a Calendar.
     */
    public static void setMidnight(Calendar cal) {
        cal.set(HOUR_OF_DAY, 0);
        cal.set(MINUTE, 0);
        cal.set(SECOND, 0);
        cal.set(MILLISECOND, 0);
    }

    public static String dayDate(Context context, long timeMillis) {
        // For an explanation of the flags see:
        // http://developer.android.com/reference/android/text/format/DateUtils.html#formatDateRange%28android.content.Context,%20java.util.Formatter,%20long,%20long,%20int,%20java.lang.String%29
        return DateUtils.formatDateTime(context, timeMillis, DateUtils.FORMAT_ABBREV_MONTH | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NO_YEAR);
    }

    public static String dayDateYear(Context context, long timeMillis) {
        // For an explanation of the flags see:
        // http://developer.android.com/reference/android/text/format/DateUtils.html#formatDateRange%28android.content.Context,%20java.util.Formatter,%20long,%20long,%20int,%20java.lang.String%29
        return DateUtils.formatDateTime(context, timeMillis, DateUtils.FORMAT_ABBREV_MONTH | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_YEAR);
    }

    public static String dayDateYearHourMinute(long timeMillis) {
        final SimpleDateFormat sdf = new SimpleDateFormat("EE dd.MM.yyyy hh:mm", Locale.getDefault());
        return sdf.format(new Date(timeMillis));
    }

    public static String timeInUTC(Date date) {
        DateFormat dateFormat = DateFormat.getTimeInstance(DateFormat.SHORT);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(date);
    }
}
