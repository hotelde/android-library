package de.hotel.android.library.remoteaccess.v30.querymapping;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.model.criterion.FilterCriterion;
import de.hotel.android.library.domain.model.criterion.HotelAvailCriterion;
import de.hotel.android.library.domain.model.enums.AmenityType;
import de.hotel.android.library.domain.model.enums.BreakfastType;
import de.hotel.android.library.domain.model.enums.SortOrder;
import de.hotel.android.library.domain.model.query.HotelAvailQuery;
import de.hotel.android.library.domain.model.criterion.HotelBookCriterion;
import de.hotel.android.library.domain.model.criterion.HotelRateCriterion;
import de.hotel.android.library.domain.model.criterion.HotelSearchCriterion;
import de.hotel.android.library.domain.model.criterion.LocationCriterion;
import de.hotel.android.library.domain.model.criterion.RoomCriterion;
import de.hotel.android.library.domain.model.enums.RoomType;
import de.hotel.android.library.remoteaccess.v30.OtaConstants;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.AvailRequestSegmentsType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.DateTimeSpanType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.DistanceAttributesGroup;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.HotelReferenceGroup;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.HotelSearchCriteriaType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.HotelSearchCriterionType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.ItemSearchCriterionType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.LocationGroup;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.MealsIncludedGroup;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.OTAHotelAvailRQ;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.OTAPayloadStdAttributes;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.PositionGroup;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.RatePlanCandidatesType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.RatePlanGroup;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.RateRangeGroup;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.RoomAmenityPrefType;
import de.hotel.android.library.util.Lists;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Mapper class to map {@link HotelAvailQuery} to {@link OTAHotelAvailRQ}.
 *
 * @author Johannes Mueller
 */
public class HdeV30OtaHotelAvailRqMapperImpl implements HdeV30OtaHotelAvailRqMapper {
    private static final String TAG = HdeV30OtaHotelAvailRqMapperImpl.class.getSimpleName();

    private static final int MAXIMUM_WAIT_TIME_SECONDS = 60;
    private static final String CURRENCY_EUR = "EUR";
    private static final int FRACTIONAL_DIGITS = 2;
    private static final int VICINITY_GEO_POSITION_DEFAULT = 10;
    private static final String MEAL_PLAN_CODE_BREAKFAST = "MPT_19_Breakfast";

    private final HdeV30OtaMapper hdeV30OtaMapper;

    public HdeV30OtaHotelAvailRqMapperImpl(@NonNull HdeV30OtaMapper hdeV30OtaMapper) {
        this.hdeV30OtaMapper = hdeV30OtaMapper;
    }

    @Override
    public OTAHotelAvailRQ otaHotelAvailRq(HotelAvailQuery hotelAvailQuery) {
        OTAHotelAvailRQ hotelAvailRQ = new OTAHotelAvailRQ();
        hotelAvailRQ.setRequestedCurrency(hotelAvailQuery.getLocale().getIso3Currency().toUpperCase(Locale.US));
        hotelAvailRQ.setRateRangeOnly(true);

        OTAPayloadStdAttributes otaPayloadStdAttributes = hdeV30OtaMapper.createOtaPayloadStdAttributes(hotelAvailQuery.getLocale().getLanguage());
        hotelAvailRQ.setOTAPayloadStdAttributes(otaPayloadStdAttributes);

        setPaging(hotelAvailQuery, hotelAvailRQ, otaPayloadStdAttributes);

        hotelAvailRQ.setPOS(hdeV30OtaMapper.createPosType());

        HotelSearchCriteriaType hotelSearchCriteria = new HotelSearchCriteriaType();
        hotelSearchCriteria.setAvailableOnlyIndicator(true);

        ArrayList<HotelSearchCriteriaType.Criterion> criterionList = new ArrayList<>();
        HotelSearchCriteriaType.Criterion criterion = hotelSearchCriteriaTypeCriterion(hotelAvailQuery);
        criterionList.add(criterion);

        hotelSearchCriteria.setCriterionList(criterionList);

        AvailRequestSegmentsType availRequestSegments = new AvailRequestSegmentsType();
        AvailRequestSegmentsType.AvailRequestSegment availRequestSegment = new AvailRequestSegmentsType.AvailRequestSegment();

        availRequestSegment.setHotelSearchCriteria(hotelSearchCriteria);
        ArrayList<AvailRequestSegmentsType.AvailRequestSegment> availRequestSegmentsList = new ArrayList<>();
        availRequestSegmentsList.add(availRequestSegment);
        availRequestSegments.setAvailRequestSegmentList(availRequestSegmentsList);

        hotelAvailRQ.setAvailRequestSegmentsAvailRequestSegmentsType(availRequestSegments);
        hotelAvailRQ.setAvailRequestSegmentsMaximumWaitTime(new BigDecimal(MAXIMUM_WAIT_TIME_SECONDS));

        setSortOrder(hotelAvailQuery, hotelAvailRQ);

        return hotelAvailRQ;
    }

    private void setSortOrder(HotelAvailQuery hotelAvailQuery, OTAHotelAvailRQ hotelAvailRQ) {

        switch (hotelAvailQuery.getSortOrder()) {
            case SortOrder.PRICE_ASCENDING:
                hotelAvailRQ.setSortOrder(OTAHotelAvailRQ.SortOrder.RA);
                return;
            case SortOrder.DISTANCE_DESTINATION:
                hotelAvailRQ.setSortOrder(OTAHotelAvailRQ.SortOrder.DA);
                return;
            case SortOrder.STAR_RATING_DESCENDING:
                hotelAvailRQ.setSortOrder(OTAHotelAvailRQ.SortOrder.SD);
                return;
            case SortOrder.USER_RATING_DESCENDING:
                hotelAvailRQ.setSortOrder(OTAHotelAvailRQ.SortOrder.ED);
                return;
            case SortOrder.PRICE_DESCENDING:
                hotelAvailRQ.setSortOrder(OTAHotelAvailRQ.SortOrder.RD);
                return;
            default:
            case SortOrder.RECOMMENDED:
                // Intentionally left blank. The server sorts the hotels in its recommended order when no SortOrder is sent
                return;
        }
    }

    private void setFilter(HotelSearchCriteriaType.Criterion criterion, FilterCriterion filterCriterion) {
        if (filterCriterion == null) {
            return;
        }

        if (filterCriterion.getPricePerNightMaximum() != null) {
            ArrayList<HotelSearchCriterionType.RateRange> rateRanges = new ArrayList<>();
            HotelSearchCriterionType.RateRange rateRange = new HotelSearchCriterionType.RateRange();
            RateRangeGroup rateRangeGroup = new RateRangeGroup();
            rateRangeGroup.setCurrencyCode(CURRENCY_EUR); // TODO Set actual currency once the app supports different currencies
            rateRangeGroup.setMaxRate(filterCriterion.getPricePerNightMaximum().setScale(FRACTIONAL_DIGITS, BigDecimal.ROUND_HALF_UP));
            rateRange.setRateRangeGroup(rateRangeGroup);
            rateRanges.add(rateRange);
            criterion.setRateRangeList(rateRanges);
        }

        if (filterCriterion.getStarRatingMinimum() != null) {
            ArrayList<HotelSearchCriterionType.Award> awards = new ArrayList<>();
            HotelSearchCriterionType.Award award = new HotelSearchCriterionType.Award();
            award.setRating(filterCriterion.getStarRatingMinimum().toString());
            awards.add(award);
            criterion.setAwardList(awards);
        }

        if (filterCriterion.getUserRatingMinimum() != null) {
            HotelSearchCriterionType.UserGeneratedContent userGeneratedContent = new HotelSearchCriterionType.UserGeneratedContent();
            userGeneratedContent.setRating(BigInteger.valueOf(filterCriterion.getUserRatingMinimum()));
            criterion.setUserGeneratedContent(userGeneratedContent);
        }

        if (!Lists.isNullOrEmpty(filterCriterion.getAmenities())) {
            ArrayList<HotelSearchCriterionType.HotelAmenity> hotelAmenities = new ArrayList<>();
            ArrayList<RoomAmenityPrefType> roomAmenities = new ArrayList<>();

            for (@AmenityType int amenity : filterCriterion.getAmenities()) {
                switch (amenity) {
                    case AmenityType.INDOOR_PARKING:
                    case AmenityType.PARKING_LOT:
                        HotelSearchCriterionType.HotelAmenity hotelAmenity = new HotelSearchCriterionType.HotelAmenity();
                        hotelAmenity.setCode("184");
                        hotelAmenities.add(hotelAmenity);
                        break;
                    case AmenityType.WIFI:
                        RoomAmenityPrefType roomAmenity = new RoomAmenityPrefType();
                        roomAmenity.setRoomAmenity("123");
                        roomAmenity.setString("");
                        roomAmenities.add(roomAmenity);
                        break;
                }
            }

            criterion.setHotelAmenityList(hotelAmenities);
            criterion.setRoomAmenityList(roomAmenities);
        }

        if (filterCriterion.getBreakfastType() == BreakfastType.INCLUSIVE) {
            if (criterion.getRatePlanCandidates() == null) {
                createRatePlanCandidate(criterion);
            }

            MealsIncludedGroup mealsIncluded = new MealsIncludedGroup();
            ArrayList<String> mealPlanCodes = new ArrayList<>();
            mealPlanCodes.add(MEAL_PLAN_CODE_BREAKFAST);
            mealsIncluded.setMealPlanCodes(mealPlanCodes);

            criterion.getRatePlanCandidates().getRatePlanCandidateList().get(0).setMealsIncluded(mealsIncluded);
        }
    }

    private void createRatePlanCandidate(HotelSearchCriteriaType.Criterion criterion) {
        HotelSearchCriterionType.RatePlanCandidates ratePlanCandidates = new HotelSearchCriterionType.RatePlanCandidates();
        ArrayList<RatePlanCandidatesType.RatePlanCandidate> ratePlanCandidateList = new ArrayList<>();
        RatePlanCandidatesType.RatePlanCandidate ratePlanCandidate = new RatePlanCandidatesType.RatePlanCandidate();

        ratePlanCandidateList.add(ratePlanCandidate);
        ratePlanCandidates.setRatePlanCandidateList(ratePlanCandidateList);
        criterion.setRatePlanCandidates(ratePlanCandidates);
    }

    private void setPaging(HotelAvailQuery hotelAvailQuery, OTAHotelAvailRQ hotelAvailRQ, OTAPayloadStdAttributes otaPayloadStdAttributes) {
        if (hotelAvailQuery.getPageSize() == null || hotelAvailQuery.getPageNumber() == null) {
            return;
        }

        hotelAvailRQ.setMaxResponses(Integer.toString(hotelAvailQuery.getPageSize()));
        otaPayloadStdAttributes.setSequenceNmbr(BigInteger.valueOf(hotelAvailQuery.getPageNumber()));
    }


    private HotelSearchCriteriaType.Criterion hotelSearchCriteriaTypeCriterion(HotelAvailQuery hotelAvailQuery) {
        HotelSearchCriteriaType.Criterion criterion = new HotelSearchCriteriaType.Criterion();
        criterion.setExactMatch(true);

        HotelAvailCriterion hotelAvailCriterion = hotelAvailQuery.getHotelAvailCriterion();
        List<RoomCriterion> roomCriterions = hotelAvailCriterion.getRoomCriterionList();
        HotelSearchCriteriaType.Criterion.AlternateAvailability alternativeAvail = HotelSearchCriteriaType.Criterion.AlternateAvailability.ALWAYS;

        //TODO: More room criterions are planned for future implementations
        if (roomCriterions.size() == 1) {
            alternativeAvail = mapAlternativeAvailability(roomCriterions.get(0).getRoomType(), roomCriterions.get(0).getQuantity());
        }
        criterion.setAlternateAvailability(alternativeAvail);

        if (hotelAvailQuery.getCompanyNumber() != null) {
            if (criterion.getRatePlanCandidates() == null) {
                createRatePlanCandidate(criterion);
            }

            RatePlanGroup ratePlanGroup = new RatePlanGroup();
            ratePlanGroup.setRatePlanID(Integer.toString(hotelAvailQuery.getCompanyNumber()));
            criterion.getRatePlanCandidates().getRatePlanCandidateList().get(0).setRatePlanGroup(ratePlanGroup);
        }

        HotelRateCriterion hotelRateCriterion = hotelAvailQuery.getHotelRateCriterion();
        HotelBookCriterion hotelBookCriterion = hotelAvailQuery.getHotelBookCriterion();
        HotelSearchCriterion hotelSearchCriterion = hotelAvailQuery.getHotelSearchCriterion();

        if (hotelSearchCriterion != null) {
            configureHotelSearchCriterion(criterion, hotelSearchCriterion);
            addStandardRoomCandidates(criterion, hotelAvailCriterion);
            return criterion;
        } else if (hotelRateCriterion != null) {
            configureHoteRateCriterion(criterion, hotelRateCriterion);
            addStandardRoomCandidates(criterion, hotelAvailCriterion);
            return criterion;
        } else if (hotelBookCriterion != null) {
            configureHoteBookCriterion(criterion, hotelBookCriterion);
            addBookingRoomCandidates(criterion, hotelAvailCriterion, hotelBookCriterion.getBookingCode());
            return criterion;
        }

        throw new RuntimeException(TAG + " - hotelSearchCriteriaTypeCriterion():  no Criterion defined!");
    }

    private HotelSearchCriteriaType.Criterion.AlternateAvailability mapAlternativeAvailability(int roomType, int roomCount) {

        switch (roomType) {
            case RoomType.SINGLE:
                return HotelSearchCriteriaType.Criterion.AlternateAvailability.ALWAYS;

            default:
            case RoomType.DOUBLE:
            case RoomType.THREE_BED:
            case RoomType.FOUR_BED:
            case RoomType.FAMILY:
                return (roomCount == 1) ? HotelSearchCriteriaType.Criterion.AlternateAvailability.ALWAYS
                        : HotelSearchCriteriaType.Criterion.AlternateAvailability.NEVER;
        }
    }

    private void configureHotelSearchCriterion(HotelSearchCriteriaType.Criterion criterion, HotelSearchCriterion hotelSearchCriterion) {

        LocationCriterion locationCriterion = hotelSearchCriterion.getLocationCriterion();
        if (locationCriterion == null) {
            throw new NullPointerException(TAG + " - hotelSearchCriteriaTypeCriterion():  locationCriterion is null");
        }

        setFilter(criterion, hotelSearchCriterion.getFilterCriterion());

        if (locationCriterion.getLocationId() != null && locationCriterion.getLocationId() != 0) {
            ItemSearchCriterionType.CodeRef codeRef = new ItemSearchCriterionType.CodeRef();
            LocationGroup locationGroup = new LocationGroup();
            locationGroup.setCodeContext(HdeV30OtaMapper.HSBW_CONTEXT);
            locationGroup.setLocationCode(locationCriterion.getLocationId().toString());
            codeRef.setString(""); // Needed for JiBX
            codeRef.setVicinityCode(""); // Needed for JiBX
            codeRef.setLocationGroup(locationGroup);
            criterion.setCodeRef(codeRef);

            if (locationCriterion.getVicinityInKm() != 0) {
                DistanceAttributesGroup distance = new DistanceAttributesGroup();
                distance.setDistanceMax(Integer.toString(locationCriterion.getVicinityInKm()));
                distance.setUnitOfMeasureCode(OtaConstants.OTA_UNIT_OF_MEASURE_CODE_KM);
                criterion.setRadius(distance);
            }

        } else if (locationCriterion.getGeoPosition() != null) {
            PositionGroup position = new PositionGroup();
            position.setLatitude(Double.toString(locationCriterion.getGeoPosition().getLatitude()));
            position.setLongitude(Double.toString(locationCriterion.getGeoPosition().getLongitude()));
            criterion.setPosition(position);

            int vicinityInKm = VICINITY_GEO_POSITION_DEFAULT;
            if (locationCriterion.getVicinityInKm() != 0) {
                vicinityInKm = locationCriterion.getVicinityInKm();
            }

            DistanceAttributesGroup distance = new DistanceAttributesGroup();
            distance.setDistanceMax(Integer.toString(vicinityInKm));
            distance.setUnitOfMeasureCode(OtaConstants.OTA_UNIT_OF_MEASURE_CODE_KM);
            criterion.setRadius(distance);
        }
    }

    private void configureHoteRateCriterion(HotelSearchCriteriaType.Criterion criterion, HotelRateCriterion hrc) {
        String hotelCode = hrc.getHotelId();
        addHotelRefCriterion(criterion, hotelCode);
    }

    private void configureHoteBookCriterion(HotelSearchCriteriaType.Criterion criterion, HotelBookCriterion hbc) {
        String hotelCode = hbc.getHotelId();
        addHotelRefCriterion(criterion, hotelCode);
    }

    private void addHotelRefCriterion(HotelSearchCriteriaType.Criterion criterion, String hotelCode) {

        ItemSearchCriterionType.HotelRef hotelRef = new ItemSearchCriterionType.HotelRef();
        HotelReferenceGroup hotelReferenceGroup = new HotelReferenceGroup();
        hotelReferenceGroup.setHotelCode(hotelCode);
        hotelRef.setHotelReferenceGroup(hotelReferenceGroup);

        ArrayList<ItemSearchCriterionType.HotelRef> hotelRefList = new ArrayList<>();
        hotelRefList.add(hotelRef);
        criterion.setHotelRefList(hotelRefList);
    }

    private void addStandardRoomCandidates(HotelSearchCriteriaType.Criterion criterion, HotelAvailCriterion hac) {

        DateTimeSpanType stayDateRange = hdeV30OtaMapper.createDateTimeSpanType(hac.getFrom(), hac.getTo());
        criterion.setStayDateRange(stayDateRange);
        HotelSearchCriterionType.RoomStayCandidates roomStayCandidates = new HotelSearchCriterionType.RoomStayCandidates();

        List<RoomCriterion> criterionList = hac.getRoomCriterionList();
        roomStayCandidates.setRoomStayCandidateList(HdeV30RoomStayCandidateMapper.roomStayCandidateTypes(criterionList));
        criterion.setRoomStayCandidates(roomStayCandidates);
    }

    private void addBookingRoomCandidates(HotelSearchCriteriaType.Criterion criterion, HotelAvailCriterion hac, String bookingCode) {

        DateTimeSpanType stayDateRange = hdeV30OtaMapper.createDateTimeSpanType(hac.getFrom(), hac.getTo());
        criterion.setStayDateRange(stayDateRange);
        HotelSearchCriterionType.RoomStayCandidates roomStayCandidates = new HotelSearchCriterionType.RoomStayCandidates();

        List<RoomCriterion> criterionList = hac.getRoomCriterionList();
        roomStayCandidates.setRoomStayCandidateList(HdeV30RoomStayCandidateMapper.roomStayCandidateTypes(criterionList));
        roomStayCandidates.getRoomStayCandidateList().get(0).setBookingCode(bookingCode);

        criterion.setRoomStayCandidates(roomStayCandidates);
    }

}
