package de.hotel.android.library.domain.model.data;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

/**
 * @author Johannes Mueller
 */
public class Language implements Cloneable {

    private String iso2Language;
    private String variantIso2Country;

    @NonNull
    public String getIso2Language() {
        return iso2Language;
    }

    public void setIso2Language(@NonNull String iso2Language) {
        this.iso2Language = iso2Language;
    }

    @Nullable
    public String getVariantIso2Country() {
        return variantIso2Country;
    }

    public void setVariantIso2Country(@Nullable String variantIso2Country) {
        this.variantIso2Country = variantIso2Country;
    }

    @SuppressWarnings("EmptyMethod")
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (!(other instanceof Language)) {
            return false;
        }

        Language otherLanguage = (Language) other;

        if (iso2Language != null && !TextUtils.equals(iso2Language, otherLanguage.iso2Language)) {
            return false;
        }

        if (!TextUtils.equals(variantIso2Country, otherLanguage.variantIso2Country)) {
            return false;
        }

        return true;
    }
}
