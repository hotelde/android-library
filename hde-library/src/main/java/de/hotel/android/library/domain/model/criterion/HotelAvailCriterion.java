package de.hotel.android.library.domain.model.criterion;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Criterion for hotel searches considering availability.
 * <p/>
 * Important: For every requested room, an own {@link HotelAvailCriterion} has
 * to be used, even if multiple rooms have the same room configuration.
 *
 * @author Johannes Mueller
 */
public class HotelAvailCriterion {

    private Long from;
    private Long to;
    private List<RoomCriterion> roomCriterionList = new ArrayList<>();

    @NonNull
    public Long getFrom() {
        return from;
    }

    public void setFrom(@NonNull Long from) {
        this.from = from;
    }

    @NonNull
    public Long getTo() {
        return to;
    }

    public void setTo(@NonNull Long to) {
        this.to = to;
    }

    @NonNull
    public List<RoomCriterion> getRoomCriterionList() {
        return roomCriterionList;
    }

    public void setRoomCriterionList(@NonNull List<RoomCriterion> roomCriterionList) {
        this.roomCriterionList = roomCriterionList;
    }

}
