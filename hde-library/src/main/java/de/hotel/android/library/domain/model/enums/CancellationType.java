package de.hotel.android.library.domain.model.enums;

import android.support.annotation.IntDef;

@IntDef({
        CancellationType.WITHOUT_CHARGE_UNTIL_DATE,
        CancellationType.WITH_CHARGE,
        CancellationType.NON_CANCELLABLE_OR_WITH_CHARGE,
        CancellationType.NON_CANCELLABLE,
        CancellationType.UNKNOWN,})
public @interface CancellationType {
    int WITHOUT_CHARGE_UNTIL_DATE = 1;
    int WITH_CHARGE = 2;
    int NON_CANCELLABLE_OR_WITH_CHARGE = 3;
    int NON_CANCELLABLE = 4;
    int UNKNOWN = 5;
}
