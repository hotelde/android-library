package de.hotel.android.library.domain.model;

import de.hotel.android.library.domain.model.data.Amenity;
import de.hotel.android.library.domain.model.enums.CreditCardType;
import de.hotel.android.library.domain.model.query.PropertyDescription;

import java.util.ArrayList;
import java.util.List;

public class HotelProperties {
    private PropertyDescription propertyDescription;
    private HotelDescription hotelDescription;
    private List<Amenity> hotelAmenities = new ArrayList<>();
    private List<Amenity> leisureAmenities = new ArrayList<>();
    private List<Amenity> gastronomyAmenities = new ArrayList<>();
    private List<Amenity> roomAmenities = new ArrayList<>();
    private List<Amenity> complimentaryServices = new ArrayList<>();
    private @CreditCardType
    List<Integer> acceptedCreditCards = new ArrayList<>();
    // This should be an ArrayList (not List) because there's an API for ArrayLists as Intent extras,
    // but not for Lists
    private ArrayList<HotelPicture> pictures = new ArrayList<>();

    public PropertyDescription getPropertyDescription() {
        return propertyDescription;
    }

    public void setPropertyDescription(PropertyDescription propertyDescription) {
        this.propertyDescription = propertyDescription;
    }

    public HotelDescription getHotelDescription() {
        return hotelDescription;
    }

    public void setHotelDescription(HotelDescription hotelDescription) {
        this.hotelDescription = hotelDescription;
    }

    public List<Amenity> getHotelAmenities() {
        return hotelAmenities;
    }

    public void setHotelAmenities(List<Amenity> hotelAmenities) {
        this.hotelAmenities = hotelAmenities;
    }

    public List<Amenity> getLeisureAmenities() {
        return leisureAmenities;
    }

    public void setLeisureAmenities(List<Amenity> leisureAmenities) {
        this.leisureAmenities = leisureAmenities;
    }

    public List<Amenity> getGastronomyAmenities() {
        return gastronomyAmenities;
    }

    public void setGastronomyAmenities(List<Amenity> gastronomyAmenities) {
        this.gastronomyAmenities = gastronomyAmenities;
    }

    public List<Amenity> getRoomAmenities() {
        return roomAmenities;
    }

    public void setRoomAmenities(List<Amenity> roomAmenities) {
        this.roomAmenities = roomAmenities;
    }

    public List<Amenity> getComplimentaryServices() {
        return complimentaryServices;
    }

    public void setComplimentaryServices(List<Amenity> complimentaryServices) {
        this.complimentaryServices = complimentaryServices;
    }

    public @CreditCardType List<Integer> getAcceptedCreditCards() {
        return acceptedCreditCards;
    }

    public void setAcceptedCreditCards(@CreditCardType List<Integer> acceptedCreditCards) {
        this.acceptedCreditCards = acceptedCreditCards;
    }

    public ArrayList<HotelPicture> getPictures() {
        return pictures;
    }

    public void setPictures(ArrayList<HotelPicture> pictures) {
        this.pictures = pictures;
    }
}
