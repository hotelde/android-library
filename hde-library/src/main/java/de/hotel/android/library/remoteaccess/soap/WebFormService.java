package de.hotel.android.library.remoteaccess.soap;

import java.util.Map;

public interface WebFormService {
    <S> S performRequest(Map<String, String> parameters, Class<S> type);
}
