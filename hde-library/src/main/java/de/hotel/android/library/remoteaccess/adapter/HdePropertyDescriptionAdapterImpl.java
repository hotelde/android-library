package de.hotel.android.library.remoteaccess.adapter;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.model.Hotel;
import de.hotel.android.library.domain.model.HotelProperties;
import de.hotel.android.library.domain.model.query.PropertyDescriptionQuery;
import de.hotel.android.library.remoteaccess.soap.SoapService;
import de.hotel.android.library.remoteaccess.v28.querymapping.HdeV28PropertyDescriptionRequestMapper;
import de.hotel.android.library.remoteaccess.v28.resultmapping.HdeV28PropertyDescriptionResultMapper;

import de.hotel.android.library.util.Constants;
import de.hotel.remoteaccess.v28.model.GetPropertyDescription;
import de.hotel.remoteaccess.v28.model.PropertyDescriptionExtendedResponse;
import de.hotel.remoteaccess.v28.v28SoapEnvelope;

public class HdePropertyDescriptionAdapterImpl implements PropertyDescriptionAdapter {

    private final SoapService soapService;
    private final HdeV28PropertyDescriptionRequestMapper hdeV28PropertyDescriptionRequestMapper;
    private final HdeV28PropertyDescriptionResultMapper hdeV28PropertyDescriptionResultMapper;

    public HdePropertyDescriptionAdapterImpl(
            @NonNull SoapService soapService,
            @NonNull HdeV28PropertyDescriptionRequestMapper hdeV28PropertyDescriptionRequestMapper,
            @NonNull HdeV28PropertyDescriptionResultMapper hdeV28PropertyDescriptionResultMapper) {

        this.soapService = soapService;
        this.hdeV28PropertyDescriptionRequestMapper = hdeV28PropertyDescriptionRequestMapper;
        this.hdeV28PropertyDescriptionResultMapper = hdeV28PropertyDescriptionResultMapper;
    }

    @Override
    public Hotel fetchHotelDescription(@NonNull PropertyDescriptionQuery query) {
        GetPropertyDescription propertyDescriptionRequest = hdeV28PropertyDescriptionRequestMapper.createPropertyDescriptionRequest(query);

        v28SoapEnvelope soapEnvelope = soapService.performRequest(propertyDescriptionRequest, v28SoapEnvelope.class, Constants.SCHEMA_GET_PROPERTY_DESCRIPTION_REQUEST, true);

        PropertyDescriptionExtendedResponse propertyDescriptionResponse = soapEnvelope.getBody().getPropertyDescriptionResponse().getGetPropertyDescriptionResult();
        if (propertyDescriptionResponse.getError() != null) {
            switch (propertyDescriptionResponse.getError().getErrorCode()) {
                case NO_ERROR:
                    break;
                case PROPERTY_DESCRIPTION_NOT_FOUND:
                default:
                    return null;
            }
        }

        return hdeV28PropertyDescriptionResultMapper.mapPropertyDescriptionResponseToHotel(propertyDescriptionResponse);
    }
}
