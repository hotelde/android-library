package de.hotel.android.library.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import de.hotel.android.library.domain.model.data.CreditCard;
import de.hotel.android.library.domain.model.data.Customer;

import java.util.ArrayList;
import java.util.List;

/**
 * Response for a hotel reservation.
 *
 * @author Johannes Mueller
 */
public class HotelReservationResponse implements Parcelable {
    public static final Parcelable.Creator<HotelReservationResponse> CREATOR = new Parcelable.Creator<HotelReservationResponse>() {
        @Override
        public HotelReservationResponse createFromParcel(Parcel parcel) {
            return new HotelReservationResponse(parcel);
        }

        @Override
        public HotelReservationResponse[] newArray(int i) {
            return new HotelReservationResponse[i];
        }
    };
    private long fromDateMs;
    private long toDateMs;
    private String uniqueId;
    private String reservationId;
    private String reservationNumber;
    private String cancellationCode;
    private Customer bookingCustomer;
    private List<Customer> travelers = new ArrayList<>();
    private CreditCard creditCard;
    private List<Hotel> hotels = new ArrayList<>();

    public HotelReservationResponse() {

    }

    public HotelReservationResponse(Parcel parcel) {
        this.fromDateMs = parcel.readLong();
        this.toDateMs = parcel.readLong();

        this.uniqueId = parcel.readString();
        this.reservationId = parcel.readString();
        this.reservationNumber = parcel.readString();
        this.cancellationCode = parcel.readString();

        this.bookingCustomer = parcel.readParcelable(Customer.class.getClassLoader());
        parcel.readTypedList(travelers, Customer.CREATOR);
        this.creditCard = parcel.readParcelable(CreditCard.class.getClassLoader());
        parcel.readTypedList(hotels, Hotel.CREATOR);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(fromDateMs);
        parcel.writeLong(toDateMs);

        parcel.writeString(uniqueId);
        parcel.writeString(reservationId);
        parcel.writeString(reservationNumber);
        parcel.writeString(cancellationCode);

        parcel.writeParcelable(bookingCustomer, i);
        parcel.writeTypedList(travelers);
        parcel.writeParcelable(creditCard, i);
        parcel.writeTypedList(hotels);

    }

    public String getReservationId() {
        return reservationId;
    }

    public void setReservationId(String reservationId) {
        this.reservationId = reservationId;
    }

    public String getReservationNumber() {
        return reservationNumber;
    }

    public void setReservationNumber(String reservationNumber) {
        this.reservationNumber = reservationNumber;
    }

    public String getCancellationCode() {
        return cancellationCode;
    }

    public void setCancellationCode(String cancellationCode) {
        this.cancellationCode = cancellationCode;
    }

    public List<Hotel> getHotels() {
        return hotels;
    }

    public void setHotels(List<Hotel> hotels) {
        this.hotels = hotels;
    }

    public Customer getBookingCustomer() {
        return bookingCustomer;
    }

    public void setBookingCustomer(Customer bookingCustomer) {
        this.bookingCustomer = bookingCustomer;
    }

    public List<Customer> getTravelers() {
        return travelers;
    }

    public void setTravelers(List<Customer> travelers) {
        this.travelers = travelers;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Long getFromDateMs() {
        return fromDateMs;
    }

    public void setFromDateMs(Long fromDateMs) {
        this.fromDateMs = fromDateMs;
    }

    public Long getToDateMs() {
        return toDateMs;
    }

    public void setToDateMs(Long toDateMs) {
        this.toDateMs = toDateMs;
    }

}
