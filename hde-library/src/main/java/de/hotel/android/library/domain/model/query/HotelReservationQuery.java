package de.hotel.android.library.domain.model.query;

import de.hotel.android.library.domain.model.data.CreditCard;
import de.hotel.android.library.domain.model.data.Customer;

import java.util.ArrayList;
import java.util.List;

public class HotelReservationQuery extends Query {
    private String invBlockCode;
    private String hotelCode;
    private String ratePlanCode;
    private String roomTypeCode;
    private int roomCount;
    private Long from;
    private Long to;

    private CreditCard creditCard;

    private Customer bookingPerson;
    private List<Customer> travelingPersons = new ArrayList<>();

    public String getInvBlockCode() {
        return invBlockCode;
    }

    public void setInvBlockCode(String invBlockCode) {
        this.invBlockCode = invBlockCode;
    }

    public String getHotelCode() {
        return hotelCode;
    }

    public void setHotelCode(String hotelCode) {
        this.hotelCode = hotelCode;
    }

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public Long getTo() {
        return to;
    }

    public void setTo(Long to) {
        this.to = to;
    }

    public Customer getBookingPerson() {
        return bookingPerson;
    }

    public void setBookingPerson(Customer bookingPerson) {
        this.bookingPerson = bookingPerson;
    }

    public List<Customer> getTravelingPersons() {
        return travelingPersons;
    }

    public void setTravelingPersons(List<Customer> travelingPersons) {
        this.travelingPersons = travelingPersons;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public String getRatePlanCode() {
        return ratePlanCode;
    }

    public void setRatePlanCode(String ratePlanCode) {
        this.ratePlanCode = ratePlanCode;
    }

    public String getRoomTypeCode() {
        return roomTypeCode;
    }

    public void setRoomTypeCode(String roomTypeCode) {
        this.roomTypeCode = roomTypeCode;
    }

    public int getRoomCount() {
        return roomCount;
    }

    public void setRoomCount(int roomCount) {
        this.roomCount = roomCount;
    }
}
