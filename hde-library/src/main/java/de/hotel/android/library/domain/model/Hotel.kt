package de.hotel.android.library.domain.model

import android.net.Uri
import android.os.Parcel
import android.os.Parcelable

import de.hotel.android.library.domain.model.data.Address
import de.hotel.android.library.domain.model.data.Distance

import java.util.ArrayList

class Hotel() : Parcelable {

    var hotelId: String? = null
    var name: String? = null
    var receptionTimes: String? = null
    var address: Address? = null
    var starsRating = 0f
    var userRating: Float? = null
    var picture: Uri? = null
    var distances: List<Distance> = ArrayList()
    var hotelProperties: HotelProperties? = null
    var hotelPropertyReviews: HotelPropertyReviews? = null
    var hotelOffer: HotelOffer? = null

    constructor(parcel: Parcel) : this() {
        hotelId = parcel.readString()
        name = parcel.readString()
        address = parcel.readParcelable<Address>(Address::class.java.classLoader)
        starsRating = parcel.readFloat()
        userRating = parcel.readValue(Float::class.java.classLoader) as? Float
        picture = parcel.readParcelable<Uri>(Uri::class.java.classLoader)
        parcel.readTypedList(distances, Distance.CREATOR)

        hotelOffer = parcel.readParcelable<HotelOffer>(HotelOffer::class.java.classLoader)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, i: Int) {
        parcel.writeString(hotelId)
        parcel.writeString(name)
        parcel.writeParcelable(address, i)

        parcel.writeFloat(starsRating)
        // Do not use writeFloat because that method cannot handle nullable Floats
        // Do not skip writing this value either because then the parcel cannot be read correctly
        parcel.writeValue(userRating)

        parcel.writeParcelable(picture, i)
        parcel.writeTypedList(distances)
        parcel.writeParcelable(hotelOffer, i)
    }

    companion object {

        @JvmField val CREATOR: Parcelable.Creator<Hotel> = object : Parcelable.Creator<Hotel> {
            override fun createFromParcel(parcel: Parcel): Hotel {
                return Hotel(parcel)
            }

            override fun newArray(i: Int): Array<Hotel?> {
                return arrayOfNulls(i)
            }
        }
    }
}
