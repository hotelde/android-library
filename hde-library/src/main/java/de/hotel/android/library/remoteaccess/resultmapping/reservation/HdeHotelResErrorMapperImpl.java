package de.hotel.android.library.remoteaccess.resultmapping.reservation;

import android.support.annotation.NonNull;

import com.hrsgroup.remoteaccess.hde.v30.model.ota.ErrorType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.ErrorWarningAttributeGroup;

import java.util.List;

import de.hotel.android.library.domain.model.HotelReservationResponse;
import de.hotel.android.library.domain.model.query.HotelReservationQuery;
import timber.log.Timber;

public class HdeHotelResErrorMapperImpl implements HdeHotelResErrorMapper {
    private final static String errorTemplate = "Reservation error, Type: %s, Code: %s, RecordID: %s, Text: %s";

    public HotelReservationResponse handleHotelResErrors(@NonNull HotelReservationQuery hotelReservationQuery, @NonNull List<ErrorType> errors) {
        for (int i = 0; i < errors.size(); i++) {
            ErrorType error = errors.get(i);
            ErrorWarningAttributeGroup errorWarningAttributeGroup = error.getErrorWarningAttributeGroup();

            Timber.d(errorTemplate,
                    error.getType(),
                    errorWarningAttributeGroup.getCode(),
                    errorWarningAttributeGroup.getRecordID(),
                    error.getString());
        }

        // TODO Implement fancy error handling
        return null;
    }
}
