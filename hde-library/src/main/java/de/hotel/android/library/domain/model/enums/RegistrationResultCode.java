package de.hotel.android.library.domain.model.enums;

import android.support.annotation.IntDef;

@IntDef({RegistrationResultCode.UNKNOWN, RegistrationResultCode.SUCCESS, RegistrationResultCode.CUSTOMER_ERROR_EMAIL_EXISTS})
public @interface RegistrationResultCode {
    int UNKNOWN = 0;
    int SUCCESS = 1;
    int CUSTOMER_ERROR_EMAIL_EXISTS = 2;
}
