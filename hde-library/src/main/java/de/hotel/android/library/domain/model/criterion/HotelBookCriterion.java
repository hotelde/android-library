package de.hotel.android.library.domain.model.criterion;

public class HotelBookCriterion extends HotelRateCriterion {

    private String bookingCode;

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }
}


