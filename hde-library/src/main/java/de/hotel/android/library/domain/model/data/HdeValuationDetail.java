package de.hotel.android.library.domain.model.data;

import de.hotel.android.library.domain.model.enums.HdeGuestType;

/**
 * ValuationDetail contains data about the rating of a special GuestType
 * <p/>
 * the rating values ranges from zero up to ten
 */
public class HdeValuationDetail {

    private @HdeGuestType int valuationCategory;

    private int numberOfReviews;
    private Float overallEvaluation;

    private Float roomQuality;
    private Float roomNoise;
    private Float cleanliness;
    private Float RatioOfPriceAndPerformance;
    private Float catering;
    private Float friendliness;

    private Float recommendationToOtherGuests;

    public int getValuationCategory() {
        return valuationCategory;
    }

    public void setValuationCategory(int valuationCategory) {
        this.valuationCategory = valuationCategory;
    }

    public int getNumberOfReviews() {
        return numberOfReviews;
    }

    public void setNumberOfReviews(int numberOfReviews) {
        this.numberOfReviews = numberOfReviews;
    }

    public Float getOverallEvaluation() {
        return overallEvaluation;
    }

    public void setOverallEvaluation(Float overallEvaluation) {
        this.overallEvaluation = overallEvaluation;
    }

    public Float getRoomQuality() {
        return roomQuality;
    }

    public void setRoomQuality(Float roomQuality) {
        this.roomQuality = roomQuality;
    }

    public Float getRoomNoise() {
        return roomNoise;
    }

    public void setRoomNoise(Float roomNoise) {
        this.roomNoise = roomNoise;
    }

    public Float getCleanliness() {
        return cleanliness;
    }

    public void setCleanliness(Float cleanliness) {
        this.cleanliness = cleanliness;
    }

    public Float getRatioOfPriceAndPerformance() {
        return RatioOfPriceAndPerformance;
    }

    public void setRatioOfPriceAndPerformance(Float ratioOfPriceAndPerformance) {
        RatioOfPriceAndPerformance = ratioOfPriceAndPerformance;
    }

    public Float getCatering() {
        return catering;
    }

    public void setCatering(Float catering) {
        this.catering = catering;
    }

    public Float getFriendliness() {
        return friendliness;
    }

    public void setFriendliness(Float friendliness) {
        this.friendliness = friendliness;
    }

    public Float getRecommendationToOtherGuests() {
        return recommendationToOtherGuests;
    }

    public void setRecommendationToOtherGuests(Float recommendationToOtherGuests) {
        this.recommendationToOtherGuests = recommendationToOtherGuests;
    }
}

