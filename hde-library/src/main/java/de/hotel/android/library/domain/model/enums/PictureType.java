package de.hotel.android.library.domain.model.enums;

import android.support.annotation.IntDef;

@IntDef({PictureType.DEFAULT, PictureType.LOGO, PictureType.MAP, PictureType.THUMBNAIL, PictureType.CORPORATE_LOGO})
public @interface PictureType {
    int DEFAULT = 0;
    int LOGO = 1;
    int MAP = 2;
    int THUMBNAIL = 3;
    int CORPORATE_LOGO = 4;
}
