package de.hotel.android.library.remoteaccess.v30.querymapping;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.model.query.HotelReservationQuery;
import de.hotel.android.library.remoteaccess.RemoteAccessTargetEnvironmentType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.OTAHotelResRQ;

public interface HdeV30OtaHotelResRqMapper {
    OTAHotelResRQ createOTAHotelResRequest(
            @NonNull HotelReservationQuery hotelReservationQuery,
            @RemoteAccessTargetEnvironmentType int remoteAccessTargetEnvironmentType);
}
