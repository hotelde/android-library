package de.hotel.android.library.domain.service;

import android.support.annotation.NonNull;

import java.io.IOException;

import de.hotel.android.library.domain.model.data.Customer;
import de.hotel.android.library.domain.model.enums.RegistrationResultCode;
import de.hotel.android.library.domain.model.query.CustomerRegistrationQuery;
import de.hotel.android.library.remoteaccess.v28.querymapping.CustomerDataQuery;

public interface CustomerManagementFacade {
    Customer getCustomerData(CustomerDataQuery query) throws IOException;
    @RegistrationResultCode int registerCustomer(@NonNull CustomerRegistrationQuery query);
}