package de.hotel.android.library.remoteaccess.adapter;


import android.support.annotation.NonNull;

import de.hotel.android.library.domain.model.enums.RegistrationResultCode;
import de.hotel.android.library.domain.model.query.CustomerRegistrationQuery;
import de.hotel.android.library.domain.model.response.RegistrationResponse;
import de.hotel.android.library.remoteaccess.querymapping.CustomerRegistrationRequestMapperImpl;
import de.hotel.android.library.remoteaccess.resultmapping.CustomerRegistrationResponseMapperImpl;
import de.hotel.android.library.remoteaccess.soap.WebFormService;

public class HdeCustomerRegistrationAdapterImpl implements HdeCustomerRegistrationAdapter {

    private final CustomerRegistrationRequestMapperImpl requestMapper;
    private final CustomerRegistrationResponseMapperImpl responseMapper;
    private final WebFormService webFormService;

    public HdeCustomerRegistrationAdapterImpl(@NonNull CustomerRegistrationRequestMapperImpl requestMapper, @NonNull CustomerRegistrationResponseMapperImpl responseMapper, @NonNull WebFormService webFormService) {
        this.requestMapper = requestMapper;
        this.responseMapper = responseMapper;
        this.webFormService = webFormService;
    }

    @Override
    public @RegistrationResultCode int registerCustomer(@NonNull CustomerRegistrationQuery query) {

        RegistrationResponse response = webFormService.performRequest(requestMapper.mapQueryParameters(query), RegistrationResponse.class);
        return responseMapper.mapCustomerRegistrationResponse(response);
    }
}