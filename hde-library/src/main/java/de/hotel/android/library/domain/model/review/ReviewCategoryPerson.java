package de.hotel.android.library.domain.model.review;

import android.support.annotation.IntDef;

@IntDef({ReviewCategoryPerson.UNKNOWN, ReviewCategoryPerson.GUESTS_TRAVELLING_ALONE, ReviewCategoryPerson.GUESTS_TRAVELLING_AS_A_COUPLE, ReviewCategoryPerson.GUESTS_TRAVELLING_IN_GROUPS, ReviewCategoryPerson.GUESTS_TRAVELLING_WITH_CHILDREN, ReviewCategoryPerson.UNDEFINED_GUEST_TYPES})
public @interface ReviewCategoryPerson {
    int UNKNOWN = 0;
    int GUESTS_TRAVELLING_ALONE = 1;
    int GUESTS_TRAVELLING_AS_A_COUPLE = 2;
    int GUESTS_TRAVELLING_IN_GROUPS = 3;
    int GUESTS_TRAVELLING_WITH_CHILDREN = 4;
    int UNDEFINED_GUEST_TYPES = 5;
}
