package de.hotel.android.library.remoteaccess.v30.querymapping;

import de.hotel.android.library.domain.model.query.HotelAvailQuery;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.OTAHotelAvailRQ;

public interface HdeV30OtaHotelAvailRqMapper {
    OTAHotelAvailRQ otaHotelAvailRq(HotelAvailQuery hotelAvailQuery);
}
