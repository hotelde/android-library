package de.hotel.android.library.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import de.hotel.android.library.domain.model.enums.AdditionalDetailsType;
import de.hotel.android.library.domain.model.enums.CreditCardType;
import de.hotel.android.library.domain.model.enums.HdeGuaranteeType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * PaymentInfo collects data needed for Booking and booking
 * information
 */
public class HdePaymentInfo implements Parcelable {

    public static final Creator<HdePaymentInfo> CREATOR = new Creator<HdePaymentInfo>() {
        @Override
        public HdePaymentInfo createFromParcel(Parcel in) {
            return new HdePaymentInfo(in);
        }

        @Override
        public HdePaymentInfo[] newArray(int size) {
            return new HdePaymentInfo[size];
        }
    };

    private @CreditCardType List<Integer> creditCardTypes = new ArrayList<>();
    private List<HdeAdditionalDetails> additionalDetailList = new ArrayList<>();
    private List<HdeCancelPenalty> cancelPenaltyList = new ArrayList<>();
    private String rateDescription;
    private @HdeGuaranteeType int guaranteeType;
    private Date holdTime;
    private boolean prepaidIndicator;
    private BigDecimal depositAmount;
    private String depositCurrencyCode;
    private boolean breakFastIncluded;
    private boolean lunchIncluded;
    private boolean dinnerIncluded;
    private boolean cancelable;
    private Date cancellationDate;

    public HdePaymentInfo() {
        holdTime = new Date(System.currentTimeMillis());
    }

    protected HdePaymentInfo(Parcel in) {

        creditCardTypes = new ArrayList<>();
        in.readList(creditCardTypes, Integer.class.getClassLoader());

        additionalDetailList = in.createTypedArrayList(HdeAdditionalDetails.CREATOR);
        cancelPenaltyList = in.createTypedArrayList(HdeCancelPenalty.CREATOR);

        rateDescription = in.readString();

        //noinspection ResourceType
        this.guaranteeType = in.readInt();

        Long timeStamp = (Long) in.readValue(Long.class.getClassLoader());
        holdTime = (timeStamp != null) ?new Date(timeStamp) :null;

        prepaidIndicator = in.readInt() != 0;

        depositAmount = (BigDecimal) in.readSerializable();
        depositCurrencyCode = in.readString();

        breakFastIncluded = in.readInt() != 0;
        lunchIncluded = in.readInt() != 0;
        dinnerIncluded = in.readInt() != 0;

        cancelable = in.readInt() != 0;

        Long cancellationTime = (Long) in.readValue(Long.class.getClassLoader());
        cancellationDate = (cancellationTime != null) ?new Date(cancellationTime) : null;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeList(creditCardTypes);

        dest.writeTypedList(additionalDetailList);
        dest.writeTypedList(cancelPenaltyList);

        dest.writeString(rateDescription);

        dest.writeInt(guaranteeType);

        Long timeStamp = (holdTime != null) ?holdTime.getTime() : null;
        dest.writeValue(timeStamp);

        dest.writeInt(prepaidIndicator ? 1 : 0);

        dest.writeSerializable(depositAmount);
        dest.writeString(depositCurrencyCode);

        dest.writeInt(breakFastIncluded ? 1 : 0);
        dest.writeInt(lunchIncluded ? 1 : 0);
        dest.writeInt(dinnerIncluded ? 1 : 0);

        dest.writeInt(cancelable ? 1 : 0);

        Long cancellationTime = (cancellationDate != null) ?cancellationDate.getTime() : null;
        dest.writeValue(cancellationTime);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public List<HdeAdditionalDetails> getAdditionalDetailList() {
        return additionalDetailList;
    }

    public void setAdditionalDetailList(List<HdeAdditionalDetails> additionalDetailList) {
        this.additionalDetailList = additionalDetailList;

    }

    public List<HdeCancelPenalty> getCancelPenaltyList() {
        return cancelPenaltyList;
    }

    public void setCancelPenaltyList(List<HdeCancelPenalty> cancelPenaltyList) {
        this.cancelPenaltyList = cancelPenaltyList;
    }

    public String getRateDescription() {
        return rateDescription;
    }

    public void setRateDescription(String rateDescription) {
        this.rateDescription = rateDescription;
    }

    public boolean isPrepaidIndicator() {
        return prepaidIndicator;
    }

    public void setPrepaidIndicator(boolean prepaidIndicator) {
        this.prepaidIndicator = prepaidIndicator;
    }

    public Date getHoldTime() {
        return holdTime;
    }

    public void setHoldTime(Date holdTime) {
        this.holdTime = holdTime;
    }

    public boolean isBreakFastIncluded() {
        return breakFastIncluded;
    }

    public void setBreakFastIncluded(boolean breakFastIncluded) {
        this.breakFastIncluded = breakFastIncluded;
    }

    public boolean isLunchIncluded() {
        return lunchIncluded;
    }

    public void setLunchIncluded(boolean lunchIncluded) {
        this.lunchIncluded = lunchIncluded;
    }

    public boolean isDinnerIncluded() {
        return dinnerIncluded;
    }

    public void setDinnerIncluded(boolean dinnerIncluded) {
        this.dinnerIncluded = dinnerIncluded;
    }

    @CreditCardType
    public List<Integer> getCreditCardTypes() {
        return creditCardTypes;
    }

    @CreditCardType
    public void setCreditCardTypes(List<Integer> creditCardTypes) {
        this.creditCardTypes = creditCardTypes;
    }

    public @HdeGuaranteeType int getGuaranteeType() {
        return guaranteeType;
    }


    public void setGuaranteeType(@HdeGuaranteeType int guaranteeType) {
        this.guaranteeType = guaranteeType;
    }

    public BigDecimal getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(BigDecimal depositAmount) {
        this.depositAmount = depositAmount;
    }

    public String getDepositCurrencyCode() {
        return depositCurrencyCode;
    }

    public void setDepositCurrencyCode(String depositCurrencyCode) {
        this.depositCurrencyCode = depositCurrencyCode;
    }

    public boolean isCancelable() {
        return cancelable;
    }

    public void setCancelable(boolean cancelable) {
        this.cancelable = cancelable;
    }

    public Date getCancellationDeadline() { return cancellationDate; }

    public void setCancellationDeadline(Date date) {
        this.cancellationDate = date;
    }

    public BigDecimal getBreakfastAmount() {
        HdeAdditionalDetails breakfastDetails = getBreakfastDetails();
        return (breakfastDetails != null) ? breakfastDetails.getAmount() : null;
    }

    public String getBreakfastCurrencyCode() {
        HdeAdditionalDetails breakfastDetails = getBreakfastDetails();
        return (breakfastDetails != null) ? breakfastDetails.getCurrencyCode() : null;
    }

    private HdeAdditionalDetails getBreakfastDetails() {
        for (HdeAdditionalDetails additionalDetails : additionalDetailList) {
            @AdditionalDetailsType int detailsType = additionalDetails.getDetailsType();
            if (detailsType == AdditionalDetailsType.BREAKFAST) {
                return additionalDetails;
            }
        }
        return null;
    }
}
