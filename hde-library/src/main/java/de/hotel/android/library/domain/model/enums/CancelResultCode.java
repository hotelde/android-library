package de.hotel.android.library.domain.model.enums;

import android.support.annotation.IntDef;

@IntDef({CancelResultCode.GENERIC_ERROR, CancelResultCode.CANCEL_OK, CancelResultCode.RATE_NOT_CANCELABLE, CancelResultCode.ALREADY_CANCELLED})
public @interface CancelResultCode {
    int GENERIC_ERROR = 0;
    int CANCEL_OK = 1;
    int RATE_NOT_CANCELABLE = 2;
    int ALREADY_CANCELLED = 3;
    // TODO Implement all necessary codes
}
