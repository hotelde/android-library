package de.hotel.android.library.remoteaccess.adapter;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.adapter.HotelAdapter;
import de.hotel.android.library.domain.model.query.HotelAvailQuery;
import de.hotel.android.library.domain.model.HotelAvailResult;
import de.hotel.android.library.remoteaccess.resultmapping.HdeHotelAvailErrorMapper;
import de.hotel.android.library.remoteaccess.resultmapping.HdeHotelAvailResultMapper;
import de.hotel.android.library.remoteaccess.soap.SoapService;
import com.hrsgroup.remoteaccess.hde.v30.v30SoapEnvelope;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.OTAHotelAvailRQ;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.OTAHotelAvailRS;

import de.hotel.android.library.remoteaccess.v30.querymapping.HdeV30OtaHotelAvailRqMapper;
import de.hotel.android.library.util.Constants;

public class HdeHotelAdapterImpl implements HotelAdapter {

    private final SoapService hdeV30SoapService;
    private final HdeV30OtaHotelAvailRqMapper hdeV30HotelAvailQueryMapper;
    private final HdeHotelAvailResultMapper hdeHotelAvailResultMapper;
    private final HdeHotelAvailErrorMapper hdeHotelAvailErrorMapper;

    public HdeHotelAdapterImpl(@NonNull SoapService hdeV30SoapService,
                               @NonNull HdeV30OtaHotelAvailRqMapper hdeV30HotelAvailQueryMapper,
                               @NonNull HdeHotelAvailResultMapper hdeHotelAvailResultMapper,
                               @NonNull HdeHotelAvailErrorMapper hdeHotelAvailErrorMapper) {
        this.hdeV30SoapService = hdeV30SoapService;
        this.hdeV30HotelAvailQueryMapper = hdeV30HotelAvailQueryMapper;
        this.hdeHotelAvailResultMapper = hdeHotelAvailResultMapper;
        this.hdeHotelAvailErrorMapper = hdeHotelAvailErrorMapper;
    }

    @Override
    public HotelAvailResult searchAvailableHotels(@NonNull HotelAvailQuery hotelAvailQuery) {
        OTAHotelAvailRQ otaHotelAvailRQ = hdeV30HotelAvailQueryMapper.otaHotelAvailRq(hotelAvailQuery);

        v30SoapEnvelope otaAvailRSEnvelope = hdeV30SoapService.performRequest(otaHotelAvailRQ, v30SoapEnvelope.class, Constants.SCHEMA_HTNG_CHECK_AVAILABILITY, true);
        OTAHotelAvailRS otaHotelAvailRS = otaAvailRSEnvelope.getBody().getOtaHotelAvailRS();

        if (otaHotelAvailRS.getErrors() != null && !otaHotelAvailRS.getErrors().getErrorList().isEmpty()) {
            return hdeHotelAvailErrorMapper.handleHotelAvailErrors(hotelAvailQuery, otaHotelAvailRS.getErrors().getErrorList());
        }

        return hdeHotelAvailResultMapper.hotelAvailResult(hotelAvailQuery, otaHotelAvailRS);
    }
}
