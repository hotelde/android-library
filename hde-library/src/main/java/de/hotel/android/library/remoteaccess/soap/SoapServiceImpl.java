package de.hotel.android.library.remoteaccess.soap;

import android.support.annotation.NonNull;


import java.net.HttpURLConnection;

import de.hotel.android.library.domain.exception.TechnicalException;
import de.hotel.android.library.domain.exception.TechnicalExceptionCode;
import de.hotel.android.library.remoteaccess.HdeJiBXMarshaller;
import de.hotel.android.library.util.ObjectDumper;
import timber.log.Timber;

public class SoapServiceImpl implements SoapService {
    private static final String TAG = SoapServiceImpl.class.getSimpleName();

    private static final int MAX_REQUESTS = 2; // Max requests in _total_, not additional retries!

    private final HdeJiBXMarshaller jibxMarshaller;
    private final SOAPRequestBuilder soapRequestBuilder;
    private final HdeSoapClient hdeSoapClient;

    public SoapServiceImpl(@NonNull HdeJiBXMarshaller jibxMarshaller,
                           @NonNull SOAPRequestBuilder soapRequestBuilder,
                           @NonNull HdeSoapClient hdeSoapClient) {
        this.jibxMarshaller = jibxMarshaller;
        this.soapRequestBuilder = soapRequestBuilder;
        this.hdeSoapClient = hdeSoapClient;
    }

    @Override
    @SuppressWarnings({"EmptyCatchBlock", "ConstantConditions"})
    // Reason: Irrelevant if some error handling fails itself
    public <T, S> S performRequest(T request, Class<S> type, String soapAction, boolean retryOnFailure) {

        String soapRequest;
        S soapEnvelope;

        // Marshalling
        try {
            String marshalledAvailRequest = jibxMarshaller.marshalRequest(request).toString();
            soapRequest = soapRequestBuilder.buildSOAPRequest(marshalledAvailRequest);

            Timber.d("SOAP request: %s%n%s", soapAction, soapRequest);
        } catch (Exception e) {
            String detailMessage = String.format("%nMarshalling problem with %s", ObjectDumper.dump(request));
            Timber.e(detailMessage);

            throw new TechnicalException(TechnicalExceptionCode.MARSHALLING_EXCEPTION, detailMessage, e);
        }

        if (retryOnFailure) {
            soapEnvelope = performRetrying(type, soapRequest, soapAction);
        } else {
            soapEnvelope = performInternal(type, soapRequest, soapAction);
        }

        return soapEnvelope;
    }

    private <S> S performRetrying(Class<S> type, String soapRequest, String soapAction) {
        S soapEnvelope = null;

        for (int i = 0; i < MAX_REQUESTS; i++) {
            try {
                soapEnvelope = performInternal(type, soapRequest, soapAction);
            } catch (Exception e) {
                if (i < MAX_REQUESTS - 1) {
                    Timber.d("Request failed, will retry. soapAction: " + soapAction);
                    continue;
                }

                Timber.d("Request failed too often, giving up. soapAction: " + soapAction);
                throw e;
            }

            if (soapEnvelope != null) {
                break;
            }
        }

        return soapEnvelope;
    }

    private <S> S performInternal(Class<S> type, String soapRequest, String soapAction) {
        S soapEnvelope = null;
        HttpURLConnection urlConnection = null;

        // SOAP communication
        try {
            urlConnection = (HttpURLConnection) hdeSoapClient.performRequest(soapRequest, soapAction);
        } catch (Exception e) {
            String detailMessage = String.format("%nHTTP problem. %nSOAP request: %s", soapRequest);

            try {
                detailMessage = detailMessage + String.format("URL response message: %s", urlConnection.getResponseMessage());
            } catch (Exception exception) {
                // Irrelevant if getResponseMessage fails
            }

            Timber.e(detailMessage);


            if (urlConnection != null) {
                urlConnection.disconnect();
            }

            throw new RuntimeException(detailMessage, e);
        }

        // Unmarshalling

        try {
            soapEnvelope = jibxMarshaller.unmarshalResponse(urlConnection.getInputStream(), type);
        } catch (Exception e) {
            //noinspection EmptyCatchBlock Reason: Irrelevant if getResponseMessage fails
            try {
                Timber.e(String.format("URL response message: %s", urlConnection.getResponseMessage()));
            } catch (Exception exception) {
                // Irrelevant if getResponseMessage fails
            }

            String detailMessage = String.format("%nUnmarshalling problem. %nSOAP request: %s", soapRequest);
            Timber.e(detailMessage);

            throw new TechnicalException(TechnicalExceptionCode.UNMARSHALLING_EXCEPTION, detailMessage, e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return soapEnvelope;
    }
}
