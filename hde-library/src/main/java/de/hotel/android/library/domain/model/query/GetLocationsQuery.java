package de.hotel.android.library.domain.model.query;

import de.hotel.android.library.domain.model.data.Language;

public class GetLocationsQuery {
    private String destination;
    private String airportCode;
    private Language language;
    private String countryIsoA3;

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getAirportCode() {
        return airportCode;
    }

    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getCountryIsoA3() {
        return countryIsoA3;
    }

    public void setCountryIsoA3(String countryIsoA3) {
        this.countryIsoA3 = countryIsoA3;
    }
}
