package de.hotel.android.library.remoteaccess.soap;

import android.support.annotation.NonNull;
import android.util.Base64;

import java.io.IOException;
import java.net.URLConnection;

public class AuthorizationConnectionBuilder implements ConnectionBuilder {

    private static final String AUTHORIZATION = "Authorization";
    private static final String BASIC_PATTERN = "Basic %s";

    private final String username;
    private final String password;
    private final ConnectionBuilder connectionBuilder;

    public AuthorizationConnectionBuilder(@NonNull String username, @NonNull String password, @NonNull ConnectionBuilder connectionBuilder) {
        this.username = username;
        this.password = password;
        this.connectionBuilder = connectionBuilder;
    }

    @Override
    public URLConnection createUrlConnection(int requestBodyByteCount, String soapAction) throws IOException {
        URLConnection urlConnection = connectionBuilder.createUrlConnection(requestBodyByteCount, soapAction);
        setBasicAuthentication(urlConnection);

        return urlConnection;
    }

    private void setBasicAuthentication(URLConnection urlConnection) {
        byte[] auth = (username + ":" + password).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);

        urlConnection.setRequestProperty(AUTHORIZATION, String.format(BASIC_PATTERN, basic));
    }
}
