package de.hotel.android.library.domain.model;

public class HotelDescription {
    private String plainTextDescription;
    private String htmlDescription;

    public String getPlainTextDescription() {
        return plainTextDescription;
    }

    public void setPlainTextDescription(String plainTextDescription) {
        this.plainTextDescription = plainTextDescription;
    }

    public String getHtmlDescription() {
        return htmlDescription;
    }

    public void setHtmlDescription(String htmlDescription) {
        this.htmlDescription = htmlDescription;
    }
}
