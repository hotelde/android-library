package de.hotel.android.library.remoteaccess.querymapping;

import android.text.TextUtils;

import java.util.HashMap;
import java.util.Map;

import de.hotel.android.library.domain.model.query.CustomerRegistrationQuery;

public class CustomerRegistrationRequestMapperImpl {

    public Map<String, String> mapQueryParameters(CustomerRegistrationQuery query) {
        Map<String, String> queryMap = new HashMap<>();

        queryMap.put("PersonalData.FirstName", query.getFirstName());
        queryMap.put("PersonalData.LastName", query.getLastName());
        queryMap.put("PersonalData.Gender", "NotSet");
        queryMap.put("EmailData.EMailAddress", query.getEmail());
        queryMap.put("PasswordData.Password", query.getPassword());
        queryMap.put("IsSodaRequest", "true");
        queryMap.put("NewsletterSelected", "false");
        queryMap.put("SodaLanguage", query.getLocale().getLanguage().toLowerCase());

        if (!TextUtils.isEmpty(query.getCompanyName())) {
            queryMap.put("PersonalData.CompanyName", query.getCompanyName());
        }

        if (!TextUtils.isEmpty(query.getPhoneNumber())) {
            queryMap.put("PhoneNumber", query.getPhoneNumber());
        }

        return queryMap;
    }
}
