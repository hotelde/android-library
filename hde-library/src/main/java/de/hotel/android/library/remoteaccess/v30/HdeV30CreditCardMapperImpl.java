package de.hotel.android.library.remoteaccess.v30;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.model.data.CreditCard;
import de.hotel.android.library.domain.model.enums.CreditCardType;

public class HdeV30CreditCardMapperImpl implements HdeV30CreditCardMapper {
    private final CreditCardTypeMapper creditCardTypeMapper;

    public HdeV30CreditCardMapperImpl(@NonNull CreditCardTypeMapper creditCardTypeMapper) {
        this.creditCardTypeMapper = creditCardTypeMapper;
    }

    @Override
    public String mapToCreditCardCode(CreditCard creditCard) {
        @CreditCardType int creditCardType = creditCardTypeMapper.determineTypeByNumber(creditCard.getNumber());

        switch (creditCardType) {
            case CreditCardType.VISA:
                return "VI";
            case CreditCardType.MASTER_CARD:
                return "MC";
            case CreditCardType.AMERICAN_EXPRESS:
                return "AX";
            case CreditCardType.JCB:
                return "JC";
            case CreditCardType.DINERS_CLUB:
                return "DN";
            default:
                // TODO Change this to a more robust error handling, e.g. an "unknown" type
                // It's better to ignore unknown credit cards in the UI than crashing the app
                throw new RuntimeException("Cannot map this credit card type: " + creditCard.getCreditCardType());
        }
    }

    public  @CreditCardType int mapFromCreditCardCode(String cardCode) {
        switch (cardCode) {
            case "VI":
                return CreditCardType.VISA;
            case "MC":
                return CreditCardType.MASTER_CARD;
            case "AX":
                return CreditCardType.AMERICAN_EXPRESS;
            case "JC":
                return CreditCardType.JCB;
            case "DN":
                return CreditCardType.DINERS_CLUB;
            default:
                // TODO Change this to a more robust error handling, e.g. an "unknown" type
                // It's better to ignore unknown credit cards in the UI than crashing the app
                throw new RuntimeException("Cannot map this credit card type: " + cardCode);
        }
    }

}
