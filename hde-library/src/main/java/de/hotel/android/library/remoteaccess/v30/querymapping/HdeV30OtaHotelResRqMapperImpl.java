package de.hotel.android.library.remoteaccess.v30.querymapping;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.hrsgroup.remoteaccess.hde.v30.model.ota.CountryNameType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.CustomerType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.DateTimeSpanType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.EncryptionTokenType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.GuaranteeType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.GuestCountGroup;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.GuestCountType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.HotelReferenceGroup;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.HotelReservationsType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.OTAHotelResRQ;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.OTAPayloadStdAttributes;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.PaymentCardDateGroup;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.PaymentCardType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.PaymentFormType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.PersonNameType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.ProfileType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.ProfilesType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.RatePlanGroup;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.ResGlobalInfoType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.ResGuestRPHsType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.ResGuestType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.ResGuestsType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.RoomStayType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.RoomStaysType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.TelephoneInfoGroup;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.UniqueIDGroup;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.UniqueIDType;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import de.hotel.android.library.domain.model.data.CreditCard;
import de.hotel.android.library.domain.model.data.Customer;
import de.hotel.android.library.domain.model.query.HotelReservationQuery;
import de.hotel.android.library.remoteaccess.RemoteAccessTargetEnvironmentType;
import de.hotel.android.library.remoteaccess.v30.HdeV30CreditCardMapper;
import de.hotel.android.library.remoteaccess.v30.OtaConstants;

public class HdeV30OtaHotelResRqMapperImpl implements HdeV30OtaHotelResRqMapper {
    private static final String ADDRESS_TYPE_IRRELEVANT_BUT_NEEDED_FOR_HSBW = "1";
    private static final String GUARANTEE_TYPE_CREDIT_CARD = "5";
    private static final String PROFILE_TYPE_PRIVATE_CUSTOMER = "1";

    private final HdeV30OtaMapper hdeV30OtaMapper;
    private final HdeV30CreditCardMapper hdeV30CreditCardMapper;

    public HdeV30OtaHotelResRqMapperImpl(@NonNull HdeV30OtaMapper hdeV30OtaMapper, @NonNull HdeV30CreditCardMapper hdeV30CreditCardMapper) {
        this.hdeV30OtaMapper = hdeV30OtaMapper;
        this.hdeV30CreditCardMapper = hdeV30CreditCardMapper;
    }

    @Override
    public OTAHotelResRQ createOTAHotelResRequest(
            @NonNull HotelReservationQuery hotelReservationQuery,
            @RemoteAccessTargetEnvironmentType int remoteAccessTargetEnvironmentType) {
        OTAHotelResRQ otaHotelResRQ = new OTAHotelResRQ();

        // Standard attributes
        OTAPayloadStdAttributes otaPayloadStdAttributes = hdeV30OtaMapper.createOtaPayloadStdAttributes(hotelReservationQuery.getLocale().getLanguage());
        otaHotelResRQ.setOTAPayloadStdAttributes(otaPayloadStdAttributes);

        HotelReservationsType hotelReservationsType = new HotelReservationsType();
        List<HotelReservationsType.HotelReservation> hotelReservations = new ArrayList<>();
        HotelReservationsType.HotelReservation hotelReservation = new HotelReservationsType.HotelReservation();

        RoomStaysType roomStaysType = new RoomStaysType();
        ArrayList<RoomStaysType.RoomStay> roomStays = new ArrayList<>();
        RoomStaysType.RoomStay roomStay = new RoomStaysType.RoomStay();

        setUniqueId(hotelReservation);
        setRoomRate(hotelReservationQuery, roomStay);
        setGuestCount(roomStay);
        setTimeSpan(hotelReservationQuery, roomStay);
        setBasicPropertyInfo(hotelReservationQuery, roomStay);

        // When there are no traveling persons, the booking customer is the traveling person
        int numberOfTravelingPersons = hotelReservationQuery.getTravelingPersons().isEmpty() ? 1 : hotelReservationQuery.getTravelingPersons().size();
        setRoomStayResGuestRPHs(roomStay, numberOfTravelingPersons);

        setCreditCardAsGuarantee(hotelReservationQuery, roomStay);

        roomStays.add(roomStay);
        roomStaysType.setRoomStayList(roomStays);

        hotelReservation.setRoomStays(roomStaysType);

        // When there are no traveling persons, the booking customer is the traveling person
        if (hotelReservationQuery.getTravelingPersons().isEmpty()) {
            ArrayList<Customer> customers = new ArrayList<>();
            customers.add(hotelReservationQuery.getBookingPerson());
            setResGuests(customers, hotelReservation);
        } else {
            setResGuests(hotelReservationQuery.getTravelingPersons(), hotelReservation);
        }

        ResGlobalInfoType resGlobalInfo = new ResGlobalInfoType();

        // Booking customer profile
        resGlobalInfo.setProfiles(createProfileForBookingCustomer(hotelReservationQuery.getBookingPerson()));
        hotelReservation.setResGlobalInfo(resGlobalInfo);

        hotelReservations.add(hotelReservation);
        hotelReservationsType.setHotelReservationList(hotelReservations);
        otaHotelResRQ.setHotelReservations(hotelReservationsType);

        otaHotelResRQ.setPOS(hdeV30OtaMapper.createPosType());

        return otaHotelResRQ;
    }

    private void setCreditCardAsGuarantee(HotelReservationQuery hotelReservationQuery, RoomStaysType.RoomStay roomStay) {
        CreditCard creditCard = hotelReservationQuery.getCreditCard();
        if (creditCard == null) {
            return;
        }

        ArrayList<GuaranteeType> guaranteeList = new ArrayList<>();
        GuaranteeType guaranteeType = new GuaranteeType();
        GuaranteeType.GuaranteesAccepted guaranteesAccepted = new GuaranteeType.GuaranteesAccepted();
        ArrayList<GuaranteeType.GuaranteesAccepted.GuaranteeAccepted> guaranteeAcceptedList = new ArrayList<>();
        GuaranteeType.GuaranteesAccepted.GuaranteeAccepted guaranteeAccepted = new GuaranteeType.GuaranteesAccepted.GuaranteeAccepted();
        PaymentFormType.Sequence paymentFormType = new PaymentFormType.Sequence();
        PaymentCardType paymentCard = new PaymentCardType();
        paymentCard.setCardCode(hdeV30CreditCardMapper.mapToCreditCardCode(creditCard));
        paymentCard.setCardHolderName(creditCard.getName());
        EncryptionTokenType encryptionTokenType = new EncryptionTokenType();
        encryptionTokenType.setPlainText(creditCard.getNumber());
        PaymentCardDateGroup paymentCardDateGroup = new PaymentCardDateGroup();
        paymentCardDateGroup.setExpireDate(mapCreditCardDate(creditCard));
        paymentCard.setPaymentCardDateGroup(paymentCardDateGroup);
        paymentCard.setCardNumber(encryptionTokenType);

        paymentFormType.setPaymentCard(paymentCard);
        guaranteeAccepted.setSequence(paymentFormType);
        guaranteeAccepted.setGuaranteeTypeCode(GUARANTEE_TYPE_CREDIT_CARD);

        guaranteeAcceptedList.add(guaranteeAccepted);
        guaranteesAccepted.setGuaranteeAcceptedList(guaranteeAcceptedList);
        guaranteeType.setGuaranteesAccepted(guaranteesAccepted);
        guaranteeList.add(guaranteeType);
        roomStay.setGuaranteeList(guaranteeList);
    }

    private String mapCreditCardDate(CreditCard creditCard) {
        return String.format("%02d%s", creditCard.getMonth(), String.valueOf(creditCard.getYear()).substring(2, 4));
    }

    // TODO When we are able to book different rooms in one request
    // we should map the guests to the rooms, not just count the RPHs
    private void setRoomStayResGuestRPHs(RoomStaysType.RoomStay roomStay, int countOfTravelingPersons) {
        ResGuestRPHsType resGuestRPHs = new ResGuestRPHsType();
        ArrayList<String> strings = new ArrayList<>();
        for (int i = 0; i < countOfTravelingPersons; i++) {
            strings.add(String.valueOf(i));
        }
        resGuestRPHs.setStrings(strings);
        roomStay.setResGuestRPHs(resGuestRPHs);
    }

    /**
     * Traveling persons
     */
    private void setResGuests(List<Customer> customers, HotelReservationsType.HotelReservation hotelReservation) {
        ResGuestsType resGuests = new ResGuestsType();
        ArrayList<ResGuestType> resGuestList = new ArrayList<>();

        for (int i = 0; i < customers.size(); i++) {
            Customer travelingPerson = customers.get(i);
            ResGuestType resGuestType = new ResGuestType();

            resGuestType.setResGuestRPH(String.valueOf(i));
            ProfilesType profile = createProfileForTraveller(travelingPerson);
            resGuestType.setProfiles(profile);

            resGuestList.add(resGuestType);
        }

        resGuests.setResGuestList(resGuestList);
        hotelReservation.setResGuests(resGuests);
    }

    private ProfilesType createProfileForBookingCustomer(Customer customer) {
        ProfilesType customerProfile = createEmptyCustomerProfile();
        ProfileType profile = customerProfile.getProfileInfoList().get(0).getProfile();

        setCustomerUserID(customer, profile);

        CustomerType customerType = new CustomerType();
        setCustomerEmailAddress(customer, customerType);
        setCustomerName(customer, customerType);
        setCustomerPhone(customer, customerType);
        setCustomerAddress(customer, customerType);

        profile.setCustomer(customerType);
        return customerProfile;
    }

    private ProfilesType createProfileForTraveller(Customer customer) {
        ProfilesType customerProfile = createEmptyCustomerProfile();
        ProfileType profile = customerProfile.getProfileInfoList().get(0).getProfile();

        CustomerType customerType = new CustomerType();
        setCustomerName(customer, customerType);
        setCustomerEmailAddress(customer, customerType);

        profile.setCustomer(customerType);
        return customerProfile;
    }

    private ProfilesType createEmptyCustomerProfile() {
        ProfilesType profiles = new ProfilesType();
        ArrayList<ProfilesType.ProfileInfo> profileInfoList = new ArrayList<>();
        ProfilesType.ProfileInfo profileInfo = new ProfilesType.ProfileInfo();
        ProfileType profile = new ProfileType();
        profile.setProfileType(OtaConstants.PROFILE_TYPE_CUSTOMER);
        profileInfo.setProfile(profile);
        profileInfoList.add(profileInfo);
        profiles.setProfileInfoList(profileInfoList);

        return profiles;
    }

    private void setCustomerEmailAddress(Customer customer, CustomerType customerType) {
        ArrayList<CustomerType.Email> emailList = new ArrayList<>();
        CustomerType.Email email = new CustomerType.Email();
        email.setString(customer.getEmail());
        emailList.add(email);
        customerType.setEmailList(emailList);
    }

    private void setCustomerName(Customer customer, CustomerType customerType) {
        ArrayList<PersonNameType> personNameList = new ArrayList<>();
        PersonNameType personNameType = new PersonNameType();
        ArrayList<String> givenNameList = new ArrayList<>();
        givenNameList.add(customer.getFirstname());
        personNameType.setGivenNameList(givenNameList);
        personNameType.setSurname(customer.getLastname());
        personNameList.add(personNameType);
        customerType.setPersonNameList(personNameList);
    }

    private void setCustomerUserID(Customer customer, ProfileType profile) {
        ArrayList<ProfileType.UserID> userIDs = new ArrayList<>();
        ProfileType.UserID userID = new ProfileType.UserID();
        if (!TextUtils.isEmpty(customer.getPassword())) {
            userID.setPinNumber(customer.getPassword());
        }
        UniqueIDGroup uniqueIDGroup = new UniqueIDGroup();
        uniqueIDGroup.setID(customer.getEmail());
        uniqueIDGroup.setType(PROFILE_TYPE_PRIVATE_CUSTOMER);
        userID.setUniqueIDGroup(uniqueIDGroup);
        userIDs.add(userID);
        profile.setUserIDList(userIDs);
    }

    private void setCustomerPhone(Customer customer, CustomerType customerType) {
        ArrayList<CustomerType.Telephone> telephoneList = new ArrayList<>();
        CustomerType.Telephone telephone = new CustomerType.Telephone();

        if (customer.getPhone() != null) {
            TelephoneInfoGroup telephoneInfoGroup = new TelephoneInfoGroup();
            telephoneInfoGroup.setPhoneTechType(OtaConstants.PHONE_TECH_TYPE_VOICE);
            telephoneInfoGroup.setPhoneNumber(customer.getPhone());
            telephone.setTelephoneInfoGroup(telephoneInfoGroup);
            telephoneList.add(telephone);
            customerType.setTelephoneList(telephoneList);
        }
    }

    private void setCustomerAddress(Customer customer, CustomerType customerType) {
        if (customer.getAddress() == null) {
            return;
        }

        ArrayList<CustomerType.Address> addressList = new ArrayList<>();
        CustomerType.Address address = new CustomerType.Address();
        address.setType(ADDRESS_TYPE_IRRELEVANT_BUT_NEEDED_FOR_HSBW);
        ArrayList<String> addressLineList = new ArrayList<>();
        addressLineList.add(customer.getAddress().getStreet());
        address.setAddressLineList(addressLineList);
        address.setCityName(customer.getAddress().getCity());
        address.setPostalCode(customer.getAddress().getPostalCode());
        CountryNameType countryName = new CountryNameType();
        countryName.setString(""); // Needed for JiBX
        countryName.setCode(customer.getAddress().getCountryIsoA3());
        address.setCountryName(countryName);
        addressList.add(address);
        customerType.setAddressList(addressList);
    }

    private void setBasicPropertyInfo(HotelReservationQuery hotelReservationQuery, RoomStaysType.RoomStay roomStay) {
        RoomStayType.BasicPropertyInfo basicPropertyInfo = new RoomStayType.BasicPropertyInfo();
        HotelReferenceGroup hotelReferenceGroup = new HotelReferenceGroup();
        hotelReferenceGroup.setHotelCode(hotelReservationQuery.getHotelCode());
        basicPropertyInfo.setHotelReferenceGroup(hotelReferenceGroup);
        roomStay.setBasicPropertyInfo(basicPropertyInfo);
    }

    private void setTimeSpan(HotelReservationQuery hotelReservationQuery, RoomStaysType.RoomStay roomStay) {
        DateTimeSpanType timeSpan = hdeV30OtaMapper.createDateTimeSpanType(hotelReservationQuery.getFrom(), hotelReservationQuery.getTo());
        roomStay.setTimeSpan(timeSpan);
    }

    private void setRoomRate(HotelReservationQuery hotelReservationQuery, RoomStaysType.RoomStay roomStay) {
        RoomStayType.RoomRates roomRates = new RoomStayType.RoomRates();
        ArrayList<RoomStayType.RoomRates.RoomRate> roomRateList = new ArrayList<>();
        RoomStayType.RoomRates.RoomRate roomRate = new RoomStayType.RoomRates.RoomRate();
        roomRate.setInvBlockCode(hotelReservationQuery.getInvBlockCode());
        RatePlanGroup ratePlanGroup = new RatePlanGroup();
        ratePlanGroup.setRatePlanCode(hotelReservationQuery.getRatePlanCode());
        roomRate.setRatePlanGroup(ratePlanGroup);
        roomRate.setRoomTypeCode(hotelReservationQuery.getRoomTypeCode());
        roomRateList.add(roomRate);
        roomRates.setRoomRateList(roomRateList);
        roomStay.setRoomRates(roomRates);
    }

    private void setGuestCount(RoomStaysType.RoomStay roomStay) {
        GuestCountType guestCounts = new GuestCountType();
        ArrayList<GuestCountType.GuestCount> guestCountList = new ArrayList<>();
        GuestCountType.GuestCount guestCount = new GuestCountType.GuestCount();
        GuestCountGroup guestCountGroup = new GuestCountGroup();
        guestCountGroup.setAgeQualifyingCode(OtaConstants.OTA_AGE_QUALIFYING_CODE_ADULT);
        guestCountGroup.setCount(BigInteger.ONE);
        guestCount.setGuestCountGroup(guestCountGroup);
        guestCountList.add(guestCount);
        guestCounts.setGuestCountList(guestCountList);
        roomStay.setGuestCounts(guestCounts);
    }

    private void setUniqueId(HotelReservationsType.HotelReservation hotelReservation) {
        ArrayList<UniqueIDType> uniqueIDList = new ArrayList<>();
        UniqueIDType uniqueIDType = new UniqueIDType();
        UniqueIDGroup uniqueIDGroup = new UniqueIDGroup();
        uniqueIDGroup.setID(""); // This is needed because HSBW enforces the node to be there, though it must be empty!
        uniqueIDGroup.setType(OtaConstants.OTA_UNIQUE_ID_TYPE_HOTEL);
        uniqueIDType.setUniqueIDGroup(uniqueIDGroup);
        uniqueIDList.add(uniqueIDType);
        hotelReservation.setUniqueIDList(uniqueIDList);
    }
}
