package de.hotel.android.library.domain.model.enums;

import android.support.annotation.IntDef;

@IntDef({PictureCategory.UNKNOWN, PictureCategory.EXTERIOR, PictureCategory.SURROUNDING, PictureCategory.COMBINED, PictureCategory.WELLNESS,
PictureCategory.POOL, PictureCategory.GASTRONOMY, PictureCategory.ROOM, PictureCategory.LOGO, PictureCategory.MAP})
public @interface PictureCategory {
    int UNKNOWN = 0;
    int EXTERIOR = 1;
    int SURROUNDING = 2;
    int COMBINED = 3;
    int WELLNESS = 4;
    int POOL = 5;
    int GASTRONOMY = 6;
    int ROOM = 6;
    int LOGO = 7;
    int MAP = 8;
}
