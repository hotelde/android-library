package de.hotel.android.library.domain.model.data;

import android.os.Parcel;
import android.os.Parcelable;

import de.hotel.android.library.domain.model.enums.CreditCardType;

public class CreditCard implements Parcelable {
    public static final Parcelable.Creator<CreditCard> CREATOR = new Parcelable.Creator<CreditCard>() {
        @Override
        public CreditCard createFromParcel(Parcel parcel) {
            return new CreditCard(parcel);
        }

        @Override
        public CreditCard[] newArray(int i) {
            return new CreditCard[i];
        }
    };
    private static final int MONTH_MINIMUM = 1;
    private static final int MONTH_MAXIMUM = 12;
    private static final int YEAR_DIGITS = 4;
    private String number; // Credit cards can contain leading zeros
    private int month;
    private int year;
    private int cvc;
    private String name;
    private
    @CreditCardType
    int creditCardType;

    public CreditCard() {
    }

    public CreditCard(Parcel parcel) {
        number = parcel.readString();
        month = parcel.readInt();
        year = parcel.readInt();
        cvc = parcel.readInt();
        name = parcel.readString();
        //noinspection ResourceType
        creditCardType = parcel.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(number);
        parcel.writeInt(month);
        parcel.writeInt(year);
        parcel.writeInt(cvc);
        parcel.writeString(name);
        parcel.writeInt(creditCardType);
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public
    @CreditCardType
    int getCreditCardType() {
        return creditCardType;
    }

    public void setCreditCardType(@CreditCardType int creditCardType) {
        this.creditCardType = creditCardType;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        if (month < MONTH_MINIMUM || month > MONTH_MAXIMUM) {
            throw new RuntimeException("Month cannot be less than 1 or greater than 12");
        }
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        if (String.valueOf(year).length() != YEAR_DIGITS) {
            throw new RuntimeException("Not a valid year, must have 4 digits");
        }
        this.year = year;
    }

    public int getCvc() {
        return cvc;
    }

    public void setCvc(int cvc) {
        this.cvc = cvc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
