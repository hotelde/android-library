package de.hotel.android.library.domain.model.data;

import android.os.Parcel;
import android.os.Parcelable;

import de.hotel.android.library.domain.model.enums.LocationType;

public class Distance implements Parcelable {
    public static final Parcelable.Creator<Distance> CREATOR = new Parcelable.Creator<Distance>() {
        @Override
        public Distance createFromParcel(Parcel parcel) {
            return new Distance(parcel);
        }

        @Override
        public Distance[] newArray(int i) {
            return new Distance[i];
        }
    };
    private
    @LocationType
    int locationType;
    private float distanceInKm;

    public Distance() {
    }

    public Distance(Parcel parcel) {
        //noinspection ResourceType
        locationType = parcel.readInt();
        distanceInKm = parcel.readFloat();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(locationType);
        parcel.writeFloat(distanceInKm);
    }

    public int getLocationType() {
        return locationType;
    }

    public void setLocationType(int locationType) {
        this.locationType = locationType;
    }

    public float getDistanceInKm() {
        return distanceInKm;
    }

    public void setDistanceInKm(float distanceInKm) {
        this.distanceInKm = distanceInKm;
    }
}
