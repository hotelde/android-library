package de.hotel.android.library.domain.model.response;


import java.util.ArrayList;
import java.util.List;

public class RegistrationResponse {

    private Result Result;

    public Result getResult() {
        return Result;
    }

    public void setResult(Result result) {
        this.Result = result;
    }

    public static final String ERROR_CODE_CUSTOMER_ALREADY_EXISTS = "Customer_Error_Email_Exists";

    public class Result {

        private Integer CustomerNumber;
        private Boolean Corporate;
        private Boolean Newsletter;
        private List<String> ErrorCodes = new ArrayList<String>();

        public Integer getCustomerNumber() {
            return CustomerNumber;
        }

        public void setCustomerNumber(int customerNumber) {
            this.CustomerNumber = customerNumber;
        }

        public List<String> getErrorCodes() {
            return ErrorCodes;
        }

        public void setErrorCodes(List<String> ErrorCodes) {
            this.ErrorCodes = ErrorCodes;
        }

        public Boolean getCorporate() {
            return Corporate;
        }

        public void setCorporate(Boolean corporate) {
            Corporate = corporate;
        }

        public Boolean getNewsletter() {
            return Newsletter;
        }

        public void setNewsletter(Boolean newsletter) {
            Newsletter = newsletter;
        }
    }
}

